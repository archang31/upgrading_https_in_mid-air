#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import json
import re

chrome_list = json.load(open("chrome_list.json"))
ff_list = json.load(open("ff_pins.json"))

from publicsuffix import PublicSuffixList
psl = PublicSuffixList()
psl.get_public_suffix("www.example.com")

def parse(f):
  d = {}
  for x in f["entries"]:
    key = x.get("pins", None)
    if not key: continue
    d.setdefault(key, [])
    d[key].append(x['name'])
  return d

c = parse(chrome_list)
f = parse(ff_list)

def get_base_domain(s):
  return psl.get_public_suffix(s)

c.update(f)

def base_len(l):
  return len(set([get_base_domain(y) for y in l]))

certs = {
"google":(2,1,0),
"test":(0,0,1),
"tor":(2,1,3),
"twitterCom":(21,2,1),
"twitterCDN":(42,8,1),
"tor2web":(1,1,1),
"cryptoCat":(1,1,1),
"lavabit":(0,0,1),
"dropbox":(18,4,0),
"mozilla":(21,3,0),
"mozilla_fxa": (1,1,0),
"mozilla_services": (1,1,0),
"facebook": (3,2,1),
}

for k in sorted(c.keys()):
  if k == "test" or k=="mozilla_test": continue
  print "%s & %d & %d & %d & %d & %d\\\\" % (k.replace("_", "\_"), certs[k][0], certs[k][1], certs[k][2], len(c[k]), base_len(c[k]))
