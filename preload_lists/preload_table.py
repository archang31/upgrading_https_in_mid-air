#!/usr/bin/python
# -*- coding, UTF-8 -*-

import sys
import json
import re


labels = [
[(True, False, False), "Hxx",],
[(True, False, True), "HxI",],
[(False, True, False), "xPx",],
[(False, True, True), "xPI",],
[(True, True, False), "HPx",],
[(True, True, True), "HPI",],
]


for t, l in labels:
  print "\\checkmark" if t[0] else '--', '&',
  print "\\checkmark" if t[1] else '--', '&',
  print "\\checkmark" if t[2] else '--', '&',
  print "\\C%sTotal{} & \\C%sBase{} & \\CG%sTotal{} & \\CG%sBase{} & \\CNG%sTotal{} & \\CNG%sBase{} & \\FF%sTotal{} & \\FF%sBase{} \\\\" % (l,l,l,l,l,l,l,l)






 
