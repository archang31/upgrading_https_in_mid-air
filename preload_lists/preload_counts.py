#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys
import json
import re

from publicsuffix import PublicSuffixList
psl = PublicSuffixList()
psl.get_public_suffix("www.example.com")

chrome_list = json.load(open("chrome_list.json"))
chrome_ng_list = json.load(open("chrome_non_google_list.json"))
chrome_g_list = json.load(open("chrome_google_list.json"))
ff_pins = json.load(open("ff_pins.json"))
ff_hsts = {x[0]:x[1] for x in json.load(open("ff_hsts.json"))}

def parse(f):
  d = {}
  for x in f["entries"]:
    policy = (x.get("mode", "") == "force-https", "pins" in x, "include_subdomains" in x)
    d[x['name']] = policy
  return d

c = parse(chrome_list)
cg = parse(chrome_g_list)
cng = parse(chrome_ng_list)
ffp = parse(ff_pins)

ff = {}
for domain in ffp:
  if domain in ff_hsts:
    if ff_hsts[domain] != ffp[domain][2]:
      sys.stderr.write("inconsistent include_subdomains %s (HSTS: %s) (Pins: %s)" % (domain, str(ff_hsts[domain]), str(ffp[domain][2])) + "\n")
    ff[domain] = (True, True, ff_hsts[domain])
  else:
    ff[domain] = ffp[domain]

for domain in ff_hsts:
  if domain not in ff:
    ff[domain] = (True, False, ff_hsts[domain])

def get_base_domain(s):
  return psl.get_public_suffix(s)

labels = {
(True, False, False): "Hxx",
(True, False, True): "HxI",
(True, True, False): "HPx",
(True, True, True): "HPI",
(False, True, False): "xPx",
(False, True, True): "xPI",
}

def base_len(l):
  return len(set([get_base_domain(y) for y in l]))

def get_policies(l, p, base_domains = False):
  matches = [x for x, y in l.items() if (not p or y == p)]
  if base_domains:
    matches = list(set([get_base_domain(x) for x in matches]))
  return matches

sys.stderr.write(str(get_policies(cng, (False, True, False), base_domains = True)) + "\n")
sys.stderr.write(str(get_policies(cng, (False, True, True), base_domains = True)) + "\n")
sys.stderr.write(str(get_policies(cng, (True, True, False), base_domains = True)) + "\n")
sys.stderr.write(str(get_policies(cng , (True, True, True), base_domains = True)) + "\n")

for x in labels:
  print "\\newcommand{\\FF%sTotal}{%d}" % (labels[x], len(get_policies(ff, x)))
  print "\\newcommand{\\FF%sBase}{%d}" % (labels[x], len(get_policies(ff, x, True)))
  print "\\newcommand{\\C%sTotal}{%d}" % (labels[x], len(get_policies(c, x)))
  print "\\newcommand{\\C%sBase}{%d}" % (labels[x], len(get_policies(c, x, True)))
  print "\\newcommand{\\CNG%sTotal}{%d}" % (labels[x], len(get_policies(cng, x)))
  print "\\newcommand{\\CNG%sBase}{%d}" % (labels[x], len(get_policies(cng, x, True)))
  print "\\newcommand{\\CG%sTotal}{%d}" % (labels[x], len(get_policies(cg, x)))
  print "\\newcommand{\\CG%sBase}{%d}" % (labels[x], len(get_policies(cg, x, True)))

print "\\newcommand{\\FFTotal}{%d}" % len(get_policies(ff, None))
print "\\newcommand{\\FFBase}{%d}" % len(get_policies(ff, None, True))
print "\\newcommand{\\CTotal}{%d}" % len(get_policies(c, None))
print "\\newcommand{\\CBase}{%d}" % len(get_policies(c, None, True))
print "\\newcommand{\\CNGTotal}{%d}" % len(get_policies(cng, None))
print "\\newcommand{\\CNGBase}{%d}" % len(get_policies(cng, None, True))
print "\\newcommand{\\CGTotal}{%d}" % len(get_policies(cg, None))
print "\\newcommand{\\CGBase}{%d}" % len(get_policies(cg, None, True))
