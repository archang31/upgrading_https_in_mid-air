# Upgrading HTTPs In Mid-Air Case Study

[![unlicense](https://img.shields.io/badge/un-license-brightgreen.svg)](http://unlicense.org "The Unlicense") [![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme "RichardLitt/standard-readme")

> This repo is a system to test and identify issues with HSTS and HPKP in websites leveraging the [OpenWPM - the open web privacy measurement framework](https://github.com/mozilla/OpenWPM) developed with my advisor, [Joe Bonneau](http://jbonneau.com/)

This repo is a cleaned version of the system used to collect and analyze the data for my master's thesis, ["Upgrading HTTPS in Mid-Air: An Empirical Study of Strict Transport Security and Key Pinning"](https://mjkranch.com/docs/pubs/NDSS15_Kranch_Bonneau_UpgradingHTTPSMidair.pdf) along with my advisor [Joe Bonneau](http://jbonneau.com/). Please read the paper for more details on the results. Essentially, this system visits the top million websites, checks for HTTP Strict-Transport-Security (HSTS) or HTTPS Public-Key-Pinning (HPKP) based on headers and preloaded lists, and analyses those sites for various security issues including mixed content, vulnerable cookies, and malformed headers.

This is a "cleaned" repository of the system. For a dirty version more complete version, check out the [master branch](https://gitlab.com/archang31/upgrading_https_in_mid-air/tree/master).

## Table of Contents

- [The System](#the-system)
- [Install](#install)
- [Usage](#usage)
- [Contribute](#contribute)
- [License](#license)

## The System

This measurement study is essentially comprised of three separate systems - the collection of [static web content](https://www.maxcdn.com/one/visual-glossary/static-content/) via OpenWPM, the collection of [dynamic web content](https://www.omniconvert.com/what-is/dynamic-content/) via a custom Firefox Extension, and the analysis of both these resources to identify security issues via the custom scripts.

We tested the top 1 million websites as defined by Alexa at the time (2015). We also tested every website included in both Chrome's and FireFox's preloaded (HSTS and HKPK) lists including both HTTP and HTTPS as well as the base and www subdomains. The lists used in testing are in the [resources](./hsts_testing/resources) folder. Also, [Section III (Measurement Setup) of the paper](https://mjkranch.com/docs/pubs/NDSS15_Kranch_Bonneau_UpgradingHTTPSMidair.pdf) covers the system and setup in significantly more detail.

### Static Resource Collection

First, I used [the Open Web Privacy Measurement Framework (OpenWPM)](https://github.com/mozilla/OpenWPM) to automate the visiting of the top million website and for its collection database. As shown below, this system uses [Selenium](https://www.seleniumhq.org) and [MitM proxy](https://mitmproxy.org) to drive a Firefox browser. I made several modifications to this system, but the primary static resource extraction code (including all sub-frames) is in [link_commands.py](./openWPM_framework_automation/Commands/link_commands.py). The primary output of this system is a SQLite database with numerous tables of data including a record of each request, the headers sent and received from each request, and a table with every external static resource loaded during the request. The complete modified version of OpenWPM used for this project is in [openWPM_framework_automation](./openWPM_framework_automation).

### Dynamic Resource Collection

In addition to static content, we need to collect all dynamic content. This feature was not available via OpenWPM (it only worked within the traditional HTML DOM model). To collect dynamic content, I created a custom FireFox Extension that is in the [firefox_dynamic_content_extension](./firefox_dynamic_content_extension) folder. This extension extracts all dynamic content as it is loaded in the browser and leverages the `mozilla.org/network/socket-transport-service` to insert a record of each dynamic resource load into the OpenWPM created SQLite database.

### HSTS and HPKP Security Analysis

The crux of the research (and resulting paper) revolved around the analysis of these static and dynamic resources. All the code regarding the analysis is in [analysis_code](./analysis_code). Below is a breakdown of some of the files that parse or even pull additional data to assist in our testing of HSTS and HPKP issues.

__[resource_parser.py](./analysis_code/resource_parser.py)__
This file parses the resources collected from a previous OpenWPM crawl (static resources, dynamic resources or both). This file assumes there is a database.sqlite file already generated from this crawl. This file has two outputs as new tables in the input database:
  * **HSTS_Pinning_Results_Categories** - these are the general "errors" that are identified by the crawl (both name and count).
  * **HSTS_Pinning_Results_List** - this a list of every result (category). Each individual resource from the database might result in several entries in this list for the various errors is composes. Some results are also inclusive (so HSTSRefHTTP might also have a second entry HSTSSelfRefHTTP as a more specific error).

__[lists_helper.py](./analysis_code/lists_helper.py)__:
This is a helper class that handles the conversation of various input lists (the preloaded hsts.json, public suffix list, https everywhere list, top 500, top 10000, etc). Also has various helpers now to pull lists from the crawl.sqlite database.

__[resource_summary_maker.py](./analysis_code/resource_summary_maker.py)__:
This file takes that output of the previous library as an input and generates a summary of errors. This error summary is currently outputted in latex form to [publication\resources\crawl_data.tex](./publication\resources\crawl_data.tex).

__[alexa_rank_helper.py](./analysis_code/alexa_rank_helper.py)__:
This file looks up all the sites in the preloaded list in Alexa to determine their global site rank. All outputs are in [/analysis_code/resources](./analysis_code/resources). alexa_raw.txt is the raw data, alexa_base_domains.csv and alexa_base_wo_google.csv are what they say.

__[chrome_list_over_time.py](./analysis_code/chrome_list_over_time.py)__:
This file looks at the changes in the preloaded list over time. It downloads every version of the list and saves it in [/analysis_code/resources/sts](./analysis_code/resources/sts).

## Install

This branch is a fork of the OpenWPM Project. You must install OpenWPM to collect the data for the study. Visit https://github.com/mozilla/OpenWPM to download the must current version of OpenWPM. 

**NOTE**: OpenWPM seems to have split from using the MitM proxy in the past two years. You can simply use this legacy version as well. You can reference the legacy OpenWPM documentation in [./openWPM_documentation](openWPM_documentation), specifically "[Setting-up-OpenWPM](./openWPM_documentation/Setting-up-OpenWPM).

## Usage

To test OpenWPM, simply run [./openWPM_demo.py](openWPM_demo.py) with `python openWPM_demo`.

**NOTE:** You might need to change the ULIMIT for files if running in shell.  You will cause an error if set to a lower number (like 256) due to the number of sockets open at one time that are sending results to the SQLite database. [This document](https://www.tecmint.com/increase-set-open-file-limits-in-linux/) explains how to check and set your ulimit.

To crawl some sites and collect the various static and dynamic resources, run [resources_crawl.py](./resources_crawl.py). Read this file and the OpenWPM documentation for more information on the various input parameters, but an example call for this file is:
`python resources_crawl.py crawl_db.sqlite test_sites.txt -proxy true -headless true &`

Running this script creates a database *crawl_db.sqlite* in the OpenWPM main directory with all the crawl information. The sites to crawl is contained in *test_sites.txt*. The ExtractedLinks table in the *crawl_db.sqlite* file is where the crawler will save all resoureces extracted from the crawled sites.

To run the parser on these resources, simple run [analysis_code/resource_parser.py](./analysis_code/resource_parser.py) with `python resource_parser.py`.

The code for the pinning extension is in FireFoxExtension/pinutil_addon. Install the Firefox add-on sdk as instructed here: https://developer.mozilla.org/en-US/Add-ons/SDK/Tutorials/Installation . To use the add-on, simply do "cfx run" from the main directory. You can also simply drag and drop the pinutil_addon.xpi into firefox. The utility will create a resources.sqlite on your desktop that contains the information from each website.

## Contribute

> Contributors to this project are expected to adhere to our [Code of Conduct](docs/CODE_OF_CONDUCT.md "Code of Conduct").

I welcome [issues](docs/issue_template.md "Issue template"), but I prefer [pull requests](docs/pull_request_template.md "Pull request template")! See the [contribution guidelines](contributing.md "Contributing") for more information.

## License

This code is [set free](LICENSE).
