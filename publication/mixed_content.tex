%!TEX root =  main.tex
\section{Mixed content}
\label{sec:mixed_content}

Browsers isolate content using the \emph{same-origin policy}, where the origin is defined as the scheme, host, and port of the content's URL.
For example, the contents of a page loaded with the origin \texttt{example.com} should not be accessible to JavaScript code loaded by the origin \texttt{b.com}.
This is a core principle of browser security dating to the early development of Netscape 2.0~\cite{ruderman01} and formally specified in the years since~\cite{rfc6454}.
Because HTTP and HTTPS are distinct schemes, the same-origin policy means content delivered over HTTPS is isolated from any insecure HTTP content an attacker injects with the same host and port.
Therefore, an attacker cannot simply inject a frame with an origin of \texttt{http://example.com} into the browser to attempt to read data from \texttt{https://example.com}.

However, subresources such as scripts or stylesheets inherit the origin of the encapsulating document.
For example, if \texttt{example.com} loads a JavaScript library from \texttt{b.com}, the code has an origin of \texttt{example.com} regardless of the protocol used to load it and can read user data (such as cookies) or arbitrarily modify the page contents.
When an HTTPS page loads resources from an HTTP origin, this is referred to as \textit{mixed content}.
Mixed content is considered dangerous as the attacker can modify the resource delivered over HTTP and undermine both the confidentiality and integrity of the HTTPS page, significantly undermining the benefits of deploying HTTPS.
For this reason, Internet Explorer has long blocked most forms of mixed content by default, with Chrome in 2011 and Firefox in 2013 following suit~\cite{ristic14}, although the details vary and there is no standard.
Other browsers (such as Safari) allow mixed content with minimal warnings.

Not all mixed content is equally dangerous.
While terminology is not standardized, mixed content is broadly divided into \textit{active} content such as scripts, stylesheets, iframes, and Flash objects, which can completely modify the contents of the encapsulating page's DOM or exfiltrate data~\cite{vyas13}, and \textit{passive} or \textit{display} content such as images, audio, or video which can only modify a limited portion of the rendered page and cannot steal data.
All browsers allow passive mixed content by default (usually modifying the graphical HTTPS indicators as a warning).
The distinction between active and passive content is not standardized.
For example, XMLHttpRequests (Ajax) and WebSockets are considered passive content by Chrome and not blocked but are blocked by Firefox and IE.

\subsection{Pinning and mixed content}

Unfortunately, the mixed content problem repeats itself with pinned HTTPS (as it has for HTTPS with Extended Validation certificates~\cite{zusman09} and other versions of HTTPS with increased security).
If a website served over a pinned HTTPS connection includes active subresources served over traditional (non-pinned) HTTPS, then, just as with traditional mixed content, an attacker capable of manipulating the included resources can hijack the encapsulating page. 
In the case of non-pinned mixed content, manipulation requires a rogue certificate instead of simply modifying HTTP traffic.
It should be noted that this risk is not exactly analogous to traditional mixed content because an attacker's ability to produce a rogue certificate may vary by the target domain whereas the ability to modify HTTP traffic is assumed to be consistent regardless of domain.
Still, including non-pinned content substantially undermines the security benefits provided by pinning.

A further potential issue with pinned content is that subresources may be loaded over pinned HTTPS with a different pinned set.
This also represents a potential vulnerability, as an attacker may be able to obtain a rogue certificate satisfying the subresource's pin set but not the encapsulating page.
Thus, the \textit{effective pin set} of the encapsulating page is the union of the pin sets of all (active) subresources loaded by the page.
If any of the subresources are not pinned, security of the page is reduced to the ``implicit'' set of pins consisting of all trusted root CAs, negating the security benefits of pinning completely.

\subsubsection*{Empirical results}
Overall, from the homepages of \PinnedBaseDomains{} total base domains with a pinning policy, we observed a total of \PinnedRefHTTPSMixed{} non-pinned resources being included across \PinnedRefHTTPSMixedDomains{} domains.
Of these, \PinnedRefHTTPSActive{} resources at \PinnedRefHTTPSActiveDomains{} domains (\url{dropbox.com}, \url{twitter.com}, \url{doubleclick.net}, \url{crypto.cat}, and \url{torproject.org}) were active content.
As noted above, this effectively negates the security goals of pinning for these domains.

While only \PinnedRefHTTPSActiveDomains{} of \PinnedBaseDomains{} pinned base domains having active mixed-pinning content appears low at first glance, recall that \PinnedGoogleBaseDomains{} of the pinned domains are operated by Google.
Google has been diligent to avoid mixed content and has the advantage of using its own content-delivery and advertising networks.
The fact that \PinnedNotGoogleRefHTTPSActiveDomains{} of the other \PinnedNotGoogleBaseDomains{} suffered from fatal active-mixed content problems suggests this will be a serious problem as pinning is incrementally deployed, especially because these sites are on the cutting edge of security awareness.

\subsubsection*{Sources of mixed content}
\input{resources/mixed_table.tex}{}
A summary of the types of resources involved in mixed content errors is provided in Table~\ref{tab:mixed_content_types}.
The errors generally arise from including active web analytics or advertising resources. 
For example, \url{crypto.cat} loads 27 scripts on 4 pages (including \url{crypto.cat}) from \url{get.clicky.com}, a Web analytics site. 

Content delivery networks were another major source of errors.
At \url{dev.twitter.com} we observed 85 loads from various subdomains of the content-delivery network \url{akamai.net}. 
Dropbox was responsible for 921 of the mixed-content loads we observed, including loading scripts, stylesheets (CSS), and fonts (considered active content) from \url{cloudfront.net}, another content-delivery network.
They load these resources multiple times on essentially every page we visited within the domain. 

\begin{comment}
Tor's homepage (specifically its blog) provided an ironic example.
They include video (in the form of an iframe, considered active content) from YouTube, which is normally pinned, but used the proxy site \url{www.youtube-nocookie.com} which prevent users' cookies from being sent to YouTube.
Unfortunately by using this proxy site they lost the benefits of YouTube's pinning.
\end{comment}

We also observed interesting errors in ``widget'' iframes for pinned sites which we happened to observe embedded in other pages in our crawl.
For example, Twitter's embeddable gadget \url{twitter.com/settings/facebook/frame} loads (3 times) scripts from \url{connect.facebook.net}.
Similarly, we observed the advertising network DoubleClick loading an assortment of advertising scripts from various locations within an iframe embedded in other sites.
While this is meant to be included as an iframe at other sites, the non-pinned scripts it loads could still in some cases steal cookies and read user data.
In particular, all of DoubleClick's cookies and many of Twitter's are not marked \texttt{httponly} and can therefore be read by malicious scripts.
%The origin is always https://2309828.fls.doubleclick.net/activityi but the resource varies (4x https://s.xp1.ru4.com, 4x bizographics.com, 4x https://pixel.mathtag.com, 3x  https://secure.adnxs.com, and 2x https://s0.2mdn.net). 

\subsubsection*{Impact of subdomains}

A large number of these mixed content errors were due to resources loaded from subdomains of pinned domains without \texttt{includeSubDomains} set. Of the \PinnedNotGoogleBaseDomains{} pinned non-Google base domains, \PinnedRefWOSubMixedDomains{} domains had mixed content issues from loading a resource from a non-pinned subdomain of an otherwise pinned domain.
Overall, \PinnedRefWOSubMixedPercent\% of the unpinned active content loads were ``self-inflicted'' in that they were loaded from the same base domain.

Twitter had perhaps the most issues including loading scripts from \url{syndication.twitter.com}.
Although they did set a dynamic HSTS Header to protect this resource load from this non-preloaded subdomain, this doesn't fix the fact that the domain isn't pinned.
Tor also included content from numerous non-pinned subdomains.
Dropbox and CryptoCat both link to their blog and forum subdomain without an HSTS header, and dropbox.com loads images and other passive resources from photo-*.dropbox.com without HSTS being set.
The blog.x.com subdomain was the most frequent subdomain with this issue with two of the five domains introducing ``self-imposed'' mixed content on this subdomain. 
These findings suggest that confusion over the relationship between subdomains owned by the same entity is a major source of errors and that developers may be forgetting when \texttt{includeSubDomains} is in effect.

Some of the problems may also simply come from modern websites' complicated structure making it difficult to remember what is pinned and what isn't.
To this end we observed many cases of content included from non-pinned domains owned by the same domain in practice, though not strictly subdomains.
For example, Google loads images from *.ggpht.com on many of the major Google domains including play.google.com. 

\subsubsection*{Expanded pin set mixed content}
We observed \PinnedRefDifferentPin{} references to resources protected by a \textit{different} pin set from \PinnedRefDifferentPinDomains{} domains.
As discussed above, this expands the effective pin set to the union of the top-level page and all resources loaded.
Of these, \PinnedRefDifferentPinActive{} were loaded as active content by \PinnedRefDifferentPinActiveDomains{} domains: Twitter and Dropbox. 
%%NEED TO FIX. Manually switched. OLD: Twitter accounted for \DifferentPinsActiveTwitter{} of these resources referencing platform.twitter.com.
Twitter accounts for over 85\% of the expanded pin-set resources, primarily through self-expansion.
Since Twitter has two separately listed pin sets, it frequently increases its effective pin set size by loading content from the twitterCDN pin set (e.g. platform.twitter.com and api.twitter.com) on a twitterCom pin set domain.
Both Twitter and Dropbox also include a script from \url{ssl.google-analytics.com} in multiple places.
While this is a lower risk than including unpinned content, these findings support our expectation that mixed content handling will be more complicated for pinned content due to the multiple levels of potential risk..

\subsubsection*{Plain HTTP resources loaded by pinned domains}
We observed a further \PinnedRefHTTP{} references to resources over plain HTTP from \PinnedRefHTTPDomains{} pinned domains.
Only one domain, (\url{doubleclick.net}), made the mistake (observed 3 times) of including active content over HTTP by including a script from \url{http://bs.serving-sys.com/}.
Again, this script was only loaded in a \url{doubleclick.net} iframe we observed within another page.

These numbers serve as a useful baseline for comparison and suggest that errors due to mixed pinning, particularly active content, are more common than mixed HTTP content.
This suggests that this problem is not yet widely understood or appreciated, although it can completely undermine pinning.

\subsection{HSTS Mixed Content}

We also briefly consider the existence of mixed content between HSTS-protected HTTPS pages and non-HSTS resources loaded over HTTPS.
Unlike the case of pinning, this is not currently a significant security risk because resources referenced via a URL with the \texttt{https} scheme must be accessed over HTTPS, even if they are not protected by HSTS.

There is an edge case which is not clearly defined by the specification~\cite{rfc6797} related to error messages.
The HSTS standard requires hard failure with no warning if a connection to an HSTS domain has a certificate error but doesn't specify if warnings can be shown for non-HSTS resources loaded by the page.
This is likely a moot point, as modern browsers now typically block active content which would produce a certificate error even from non-HSTS pages.

Still, we found references to non-HSTS resources from HSTS pages were widespread, with \PreloadedRefHTTPSMixed{} references from \PreloadedRefHTTPSMixedDomains{} base domains, of which \PreloadedRefHTTPSActive{} from \PreloadedRefHTTPSActiveDomains{} domains were active content.
As with the pinned mixed content errors, the vast majority were ``self-inflicted'' in that they were resources loaded from a common base domain, accounting for \PreloadedSelfRefWOSubMixedPercent{}\% of all mixed content and \PreloadedSelfRefWOSubActivePercent{}\% of the active mixed content.
Resources from explicit subdomains were again a major source of mixed policy, with \PreloadedRefWOSubMixed{} references from \PreloadedRefWOSubMixedDomains{} base domains, of which \PreloadedRefWOSubActive{} were active content.

\begin{comment}

\mkcomment{After thinking about this more, we almost should be checking the redirected non HSTS sites for mixed content. HSTS (whether preloaded or not) prevents resources being loaded from HTTP. In addition, the browser also blocks this content so its a double whammy even if}

We also looked for traditional mixed content on the above preloaded HSTS sites.
Of the \HSTSBaseDomains{} preloaded base domain, we found \PreloadedSelfRefHTTPDomains{} domains referenced self served non-secure (traditional HTTP) content. In total, we found over \PreloadedSelfRefHTTP{} references to self served mixed content. 
Of these, \PreloadedSelfRefHTTPActiveDomains{} domains were loading self-hosted active content from a non-secure link. 
A great example of this type of issue is https://wiki.python.org. 
\mkcomment{explain how they are attempting to redirect, but the page still loads anyways, etc.}
In total, we found \mkcomment{need total domains} of the preloaded HSTS domains referenced HTTP content. 

In addition to traditional mixed content, we also looked for HSTS sites referencing regular HTTPS content, particularly in an active context. 
We argue that an HSTS site linking to active content of a traditional HTTPS site is lowering its overall security by exposing itself to the previously present MitM attack. 
If the secondary site if compromised, you will be serving compromised content presented as more secure HSTS content \mkcomment{this clearly needs to be cleaned up}. 
When checking for HSTS referencing traditional HTTPS, the biggest issue was sites self-downgrading themselves due to linking content from another subdomain without setting a valid included subdomain. 
For example, mobile.usaa.com is a preloaded HSTS site as well as www.usaa.com and usaa.com, but all three domains do not set ``includes\_subdomains'': true. 
All three USAA domains also set HSTS headers where max-age=31536000 but again do not set the optional value includes subdomains.
On just about every site within the USAA domain including the base www.usaa.com and mobile.usaa.com, the page loads several scripts from tms.usaa.com, s.usaa.com and content.usaa.com. 
Since USAA did not set any includes subdomains flags, these sites are not included in the preloaded HSTS umbrella; however, USAA does set a HSTS header independently on these subdomains preventing the MitM attack assuming the user as previously visited the domain with the past year. 
While USAA also sets HSTS headers for those listed non-preloaded domain, it fails to also set one for reviews.usaa.com - another frequently referenced subdomain.
On multiple pages including https://www.usaa.com/inet/pages/auto\_insurance\_main and /bank\_loan\_personal, USAA loads scripts like jquery.core.js and commentDisplay.js from reviews.usaa.com. 
 An attacker could MitM review.usaa.com, insert malicious code into any of the loaded scripts and completely by-pass the added security of both being preloaded HSTS and setting a valid HSTS header on the majority of USAA subdomains. 
 \mkcomment{I am pretty sure this is true, but I would like your confirmation}
Setting HSTS makes this attack more difficult than without any HSTS, but USAA could completely prevent a MitM attack by properly setting includes subdomains on the base usaa.com, and then ``opting out'' on a specific subdomain if necessary.

\end{comment}

