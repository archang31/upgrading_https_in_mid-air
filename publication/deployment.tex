%!TEX root =  main.tex
\section{Current deployment}
\label{sec:deployment}

In this section we provide an overview of current deployment of HSTS and pinning using crawling and inspection of the preload lists used by Chrome and Firefox.
Our statistics are all based on observations from November 2014.

\subsection{Preload implementations}

Chrome's preload list is canonically stored\footnote{This file is not shipped with the browser but is parsed at build-time into a machine-generated C++ file storing the policies} in a single JSON file\footnote{Technically, the file is not valid JSON because it includes C-style comments.} containing both pinning and HSTS policies.%\footnote{Chrome's list also includes one entry for \url{learn.doubleclick.net} which sets neither HSTS nor pins and is thus meaningless.} 
There is also a supporting file specifying named keys referenced in pinning policies.
Some are specified as complete certificates (although only the keys are germane for pinning) and other are specified as SHA-1 hashes of keys.

Until 2014, the criteria for inclusion in Chrome's list were not formally documented and inclusion required email communication with an individual engineer responsible for maintaining the list~\cite{langley10,langley11}.
In mid-2014, Google rolled out an automated web form\footnote{\url{https://hstspreload.appspot.com}} for requesting inclusion into Chrome's preload list. 
In addition to submitting this form, domains must redirect from HTTP to HTTPS and set an HSTS header with \texttt{includeSubDomains}, a \texttt{max-age} value of at least 10,886,400 (one year), and a special \texttt{preload} directive.
As seen in Figure \ref{fig:preload_growth}, this automated process has led to a significant increase in the numbers of preloaded entries.
However, the new requirements have not been retroactively enforced on old entities and new sites can be added at the discretion of Google without meeting these requirements (e.g. Facebook and Yahoo!\ were recently added but do not set \texttt{includeSubDomains}).
Additionally, while requests for inclusion can now be submitted automatically, they are still manually reviewed and take months to land in a shipping version of the browser and there still is not an automated method of submitting preloaded pinsets. 

%This token must be present in the to-be-added site's header and its only purpose is for automated addition to the Chrome preload list. 
During our crawl, we observed \PreloadedSTSPreload{} entries on the preload list which present the \texttt{preload} token, accounting for \TopSTSNotPreloadedSetsPreloadPercent\% of the list. 
Since this token was only recently introduced, this number is a clear indication of both the effect of Google's automated entry and the growth of the HSTS list.
We also observed \TopSTSNotPreloadedSetsPreload{} additional sites setting the \texttt{preload} token in our top million crawl that are not yet in the preloaded list.
Several of these sites (\TopSTSNotPreloadedSetsPreloadNotValidPercent\%) are presenting invalid headers so were most likely rejected by Google for inclusion in the list; however, the remaining sites do set valid HSTS header and must be awaiting manual confirmation.%, demonstrating that is continuing to grow (or possibly that a substantial portion of domains requesting inclusion are being rejected).

%In addition to checking for the \texttt{preload} token, Google also if enforcing certain parameters, some in addition to the specifications in RFC 6797. 
%Google is also enforcing that the site sets the \texttt{includeSubDomains} token, , and, if there is a redirect, that the redirect serves the header and not the originating site.  These parameters should be enforced by the RFC rather than by a third party entity,  but this decision by Google has caused less errors and lead to

Mozilla's implementation uses two separate files for preloaded HSTS and pinning policies.
The preloaded HSTS file is compiled as C code.
Currently, the list is a strict subset of Chrome's list based on those domains in Chrome's list which also set an HSTS header with a \texttt{max-age} of at least 18 weeks~\cite{keeler12}.
The \texttt{includeSubDomains} parameter must also be set in the header to be preloaded, so 45 domains are preloaded in Chrome with \texttt{includeSubDomains} set but preloaded in Firefox without it.
Because Mozilla's list is a strict subset of Chrome's list, we perform all testing for the remainder of the paper on the larger Chrome list.

Firefox also has a preloaded pin list which is implemented as a JSON file with an identical schema to Chrome's.
Currently, the only entries are 9 Mozilla-operated domains, 3 test domains, and \url{twitter.com}.
We perform testing only on Chrome's preloaded pinning lists.

\begin{figure}[t]
	\centering
	\includegraphics[trim=1cm 0.5cm 1cm 1cm,clip=true,width=\linewidth]{figures/chrome_list_over_time.pdf}
	\caption{Growth of preloaded HSTS in Google Chrome.}
	\label{fig:preload_growth}
\end{figure}

\subsection{Preloaded HSTS}

\input{resources/preload_list_stats.tex}

%Chrome's preload lists consists policies for a total of \PreloadedDomains{} fully-qualified domains from \PreloadedBaseDomains{}.
\begin{figure}[t]
	\centering
	\includegraphics[trim=1cm 0.5cm 1cm 1cm,clip=true,width=\linewidth]{figures/alexa_ranks_bar_chart.pdf}
	\caption{Histogram of Alexa Site Ranks~\cite{alexa} for domains with a preloaded HSTS policy in Google Chrome.}
	\label{fig:AlexaRank}
\end{figure}

Chrome's preload list, launched in 2010, currently consists of \CTotal{} domains from \CBase{} unique base domains as summarized in Table~\ref{tab:preload_statistics}.
Roughly a quarter of the current list represents Google-owned properties.
Aside from the Google domains, there are a large number of relatively small websites.
Figure~\ref{fig:AlexaRank} shows the distribution of preloaded sites' Alexa traffic rankings.
The median site rank is about 100,000 and the mean is 1.5M, with 294 base domains (29\%) too small to be ranked (and thus not included in our computed mean).
Additionally, only 7 of the top 100 and 19 of the top 1000 non-Google sites are included in the preloaded HSTS list, suggesting that uptake is primarily driven by sites' security interest and not by their size.
At least 15 sites on the list appear to be individual people's homepages.

Even with the relatively small scale and short history of preloaded HSTS, the list is surprisingly stale. 
Of the \CNGBase{} non-Google base domains with a preloaded policy, at least \PreloadedDomainNotAccessible{} domains return a 404 or other site-not-accessible errors.
We excluded from this count sites which by manual inspection appeared to be content-delivery networks with no home page by design.
These outdated preloaded entries include sites like the infamous \url{liberty.lavabit.com} and \url{sunshinepress.org} (a long outdated WikiLeaks alias).
We also counted \PreloadedRedirectsToHTTP{} sites redirect directly to a distinct plain-HTTP URL, and \PreloadedSTSRedirectsOutOfSTS{} preloaded HSTS domains redirecting to non-HSTS domain. 
For example, \url{https://www.evernote.com} is the only Evernote entry in the preload list and redirects to \url{https://evernote.com} (which does not set a dynamic HSTS header).\footnote{This has since been changed after notification of Evernote.}
These may represent stale data or sites may have a legitimate reason to have a changed branding, but the result is that these preloaded policies have little effect in practice.
Thus, of the \CNGBase{} non-Google base domains in the list, \PreloadedSTSStale{} (\PreloadedSTSStalePercent\%) represent policies that are no longer needed.

Stale entries were not limited to non-Google domains either: the Google-owned domain \url{urchin.com} is still in the list although it was discontinued in May 2012.
An additional \PreloadedRedirectsToHTTPGoogle{} Google-operated domains with a preloaded HSTS policy now redirect directly to HTTP including \url{ssl.google-analytics.com} and \url{dl.google.com}. 

While scalability of the preload list is often discussed in terms of the amount of storage required to ship the list and the cost of checking every browser request for a preloaded policy, these findings highlight that human management of the list is a bigger issue today.
All of the stale entries we observed predate the automated process for inclusion.
However, it isn't clear if in the future stale entries will be removed or how quickly.
 %\footnote{Google has since automated this process and this automation is discussed in section xx.}
%Could discuss public suffix list as a historical example.

\subsection{Preloaded pinning}

\input{resources/pinsets_table.tex}

A summary of the preloaded pinning policies is shown in Table~\ref{tab:preload_statistics}.
Outside of Google, which pins nearly all of its properties, pinning remains relatively rare with only \PinnedNotGoogleEntries{} entries from \PinnedNotGoogleBaseDomains{} unique base domains implementing pinning in Firefox or Chrome.

Table~\ref{tab:pin_sets} providers further details of the domains that utilize pinning and the size of various pin sets being used.
Only 4 for-profit companies (Dropbox, Facebook, Google, and Twitter) are actively utilizing pinning.
Of these, Twitter and Dropbox both pin to a large number of certificate authorities rather than end-entity keys.
Mozilla takes a similar approach.

By contrast, Facebook's pin set is relatively small with only 3 CA pins (2 being from DigiCert).
Google pins to just 2 CAs, although they are intermediate CAs operated by Google itself (Google Internet Authority).
Google is rare in controlling a CA as well as largely running its own data centers and content-delivery networks, making it much easier to pin to a smaller set of keys.

Even for domains with a large nominal set of certificate authorities pinned, most in fact come from a small number of organizations as noted in Table~\ref{tab:preload_statistics} under ``Distinct CAs.''
For example, Twitter's main pin set currently uses 21 certificates but only 2 organizations (DigiCert and Symantec, which owns VeriSign and GeoTrust).
The large number of pinned certificates is partially an artifact of historical fragmentation and consolidation in the CA market.

%The remaining pinned domains are all non-profit entities with much more limited pin sets.

\subsubsection*{End-entity pins}
Pinning to end-entity keys is rare.
Of all pinned domains, we observed only \PreloadedCertsEnd{} (at \url{tor2web.org}) of the \PreloadedCertsMatch{} keys which matched pinning policies (\PreloadedCertsEndPercent\%) in an end-entity certificate.
Lavabit was the only domain specifying a pinning policy consisting of only an end-entity-key, leaving no certificate authority able to undermine its security.
Of course, Lavabit suspended operation in August 2013, indicating that the preloaded pin list is also not completely up-to-date.

Thus, while pinning is designed to limit vulnerability to rogue CAs, it is almost exclusively being used to limit trust in CAs and not remove them completely.
Furthermore, 75\% of active pinsets (not including Lavabit or the test pinset) include a single key (DigiCertEVRoot), meaning that a single CA (DigiCert) can still issue certificates for most pinned domains.

\subsubsection*{Unused pins}
Overall, of the \PreloadedCerts{} different keys listed in Chrome's \texttt{strict\_transport\_security.certs} file, only \PreloadedCertsUsed{} are matched when crawling the entire preload list, meaning \PreloadedCertsNotUsedPercent{}\% are in the list solely as backup or contingency keys.
It is possible some of these keys are used for subdomains that we did not crawl or alternate servers which our crawler did not hit due to load-balancing.
However, this large number of unused pins suggests that pinning is challenging for some large websites to deploy as they rely on a huge number of different certificates for different parts of their operation and aren't able to specify a narrow policy.

\subsubsection*{Pinning without HSTS}
Most pinned domains also set HSTS, which is consistent with the belief that pinning is more complicated to deploy and HSTS defends against easier-to-execute attacks.
However, \PinnedGoogleNotHSTS{} Google domains which are pinned do not set HSTS.
While Google has not explained this policy, analysis shows 217 of the domains are country-specific variations of the main Google home-page (such as \url{google.sn}) as well as \url{google.com} itself.
For these pages, which implement Google's search engine, supporting non-HTTPS access has been deemed a policy requirement by Google for schools, libraries, and other locations which mandate that adult content is filtered~\cite{googleSafeSearch14}.
The remainder are mostly ad network domains (\url{doubleclick.net}, \url{googleadservices.com}) and content delivery network domains (\url{gstatic.com}, \url{googleusercontent.com}), but a few security-critical domains are not protected with HSTS, including \url{googleapis.com} and \url{android.com}.
This suggests that, in some cases, enabling pinning may actually be less difficult than HSTS due to the need to support legacy clients which cannot use HTTPS.

Twitter and Tor2web are the only other domains to employ pinning without HSTS for 8 domains.
In Twitter's case, these are mostly specific subdomains of \url{twitter.com} such as \url{api.twitter.com} and \url{mobile.twitter.com}, which suggests that support for legacy (non-HTTPS clients) is a significant motivation.

\subsection{Dynamic HSTS}

All sites with a preloaded HSTS policy should also be setting HSTS headers.
Otherwise no security is provided for HSTS-compliant browsers without a preload list. % to users using the Chrome browser as Firefox requires a header for inclusion in its preload list and other browsers (such as Safari, Opera) don't support preloaded HSTS at all.
Still, \PreloadedNoSTSHeader{} of the \PreloadedDomains{} preloaded HSTS domains (\PreloadedNoSTSHeaderPercent\%) \emph{do not} set HSTS headers.
Google domains account for about a sixth of these domains with \PreloadedNoSTSHeaderGoogle{} (\PreloadedNoSTSHeaderGooglePercent\% of Google HSTS entries) not setting an HSTS header.
It is unclear what Google's reasoning is for not setting HSTS headers for these domains, particularly as Google \textit{does} set an HSTS header on \PreloadedSTSHeaderGoogle{} domains including \url{accounts.google.com} and \url{plus.google.com}.
This represents a major security shortcoming as all of these domains are left vulnerable on non-Chrome browsers.

\subsubsection*{Additional dynamic HSTS domains}

We also crawled all of the Alexa top million websites looking for additional domains setting HSTS headers.
We observed only \TopSTSAttempted{} sites of the \TopDomains{} valid HTTP responses attempt an HSTS header and, of these dynamic HSTS sites,  \TopSTSMaxAgeZero{} are ``opting out'' by setting a \texttt{max-age=0}.

Even though only a small percentage (\TopSTSAttemptedPercent\%) are setting an HSTS header, \TopRedirectsToHTTPSWithoutSTSPercent\% of the top million sites \emph{should} be using HSTS.
We found \TopRedirectsToHTTPSWithoutSTS{} sites using HTTP 30x codes to directly redirect from non-secure HTTP to HTTPS without setting an HTTPS HSTS header, including many financial sites like \url{citibank.com} and \url{chase.com}, email providers like \url{gmail.com} and government sites like \url{healthcare.gov}.

Redirecting from HTTP to HTTPS is a strong indication that these sites are intending to only offer their services over a secure connection.
The fact that this remains so much more common than setting HSTS indicates developers may not be sufficiently aware of the new technology yet or may be nervous about causing some legacy clients to lose access.
It's possible this is an explicit policy choice and the redirects would not have happened if we had crawled with an outdated \texttt{user-agent} string.
However, by redirecting without setting a header these sites are exposing users to HTTPS stripping every time they connect.

\subsubsection*{Max-age values}
\begin{figure}[t]
	\centering
	\includegraphics[trim=1cm 0.5cm 1cm 1cm,clip=true,width=\linewidth]{figures/max_age_bar_chart.pdf}
	\caption{Histogram of \texttt{max-age} values for dynamic HSTS headers.}
	\label{fig:MaxAge}
\end{figure}

Of the \TopSTSAttempted{} attempted HSTS headers we observed, there was a wide range of values for the \texttt{max-age} found in practice, as shown in Figure~\ref{fig:MaxAge}.
The most common value  is 31,536,000 seconds (one year), set by \TopSTSMaxAgeAYear{} domains.
This is not surprising given that this value is what is most often used as the example in the HSTS specification\cite{rfc6797}. 
Outside of this standard, the values vary widely with \TopSTSMaxAgeADayOrLess{} headers set at 86,400 seconds (a day) or under including 15 sites that set the ridiculous \texttt{max-age} value of one second. 
Conversely,  \TopSTSMaxAgeOverAYear{} values over a year with the highest value found of 100,000,000,000 (over 3,000 years). 
The \texttt{max-age} values also vary widely amongst the major players with Paypal only setting a header with an age of 4 hours and Twitter, Vine and Etsy all setting \texttt{max-age} over 20 years.

Very short values, such as the \TopSTSMaxAgeADayOrLessPercent\% of domains setting a value of a day or less, arguably undermine the value of HSTS.
While they provide some protection against a passive eavesdropper in the case of developer mistakes (such as using the HTTP protocol for an internal link), they are dangerous against an active attacker as many users will be repeatedly making untrusted initial connections if they do not visit the site very frequently.

It is an open question if very large \texttt{max-age} values will cause problems in the future if a small number of clients cache very old policies.
Unlike the proposal for HPKP, there is no standard maximum for \texttt{max-age} and user agents really are meant to cache the policy for 80 years if instructed to do so, although most browser instances in practice will not be around for that long.

Finally, we saw \TopSTSMaxAgeZero{} sites setting a \texttt{max-age} of 0 including several big domains like LinkedIn and \url{t.co}, a Twitter content delivery domain.
Yahoo actually redirects from HTTP to HTTPS on every tested Yahoo subdomain but still specifically avoids using HSTS and intentionally sets a max age of 0. 
This is valid according to the specification~\cite{rfc6797}.
It's intended use is to force clients to forget their cached policy for the domain in case the domain wishes to revert to using HTTP traffic.
Of these, \TopSTSMaxAgeZeroRedirectsToHTTP{} redirect from HTTPS to HTTP, clearly using the specification as intended. 

\subsection{HSTS errors}
\label{sec:hsts_errors}

%Added hard numbers for revised PDF. Need to make below automated
%Of the sites setting dynamic HSTS, a number did so in erroneous ways
Of the \TopSTSAttempted{} sites setting dynamic HSTS,  \TopSTSNotValid{} did so in erroneous ways.
A summary of errors we observed is in Table~\ref{tab:error_statistics}.
It is striking that overall, of the non-preloaded sites attempting to set HSTS dynamically, nearly \TopSTSNotValidPercent\% had a major security problem which undermined the effectiveness of HSTS.
The rate of errors was significantly lower among sites with a preloaded HSTS policy.

\input{resources/dynamic_hsts_errors_table.tex}

\subsubsection*{HSTS sites failing to redirect}

The HSTS specification~\cite{rfc6797} states that HSTS domains \emph{should} redirect HTTP traffic to HTTPS.
However, of the \TopSTSAttempted{} sites attempting to set dynamic HSTS, \TopSTSDoesNotRedirect{} do not redirect.
In addition, \PreloadedSTSDoesNotRedirect{} preloaded HSTS sites do not redirect from HTTP to HTTPS.
In addition to violating the standard this represents a security hole as first-time visitors may never transition to HTTPS and therefore never learn the HSTS policy.

\subsubsection*{HSTS headers set over HTTP}
\label{sec:hsts_over_http}
Another sign of confusion about the technology is sites setting an HSTS header over HTTP.
Although the HSTS standard specifies that this has no effect and should not be done~\cite{rfc6797}, we observed \TopHTTPSetsSTS{} domains setting an HSTS header on the HTTP version of their sites.
For sites otherwise implementing HSTS properly, this is a harmless mistake; however, we found \TopHTTPSetsSTSOnly{} domains set HTTP HSTS headers without specifying a HTTPS header, a strong indication that the sites misunderstood the specification. \TopSTSDoesNotRedirectButSetsSTS{} of the total HTTP HSTS domains (including popular tech sites like \url{blockchain.info} and \url{getfirebug.com}) also failed to redirect to HTTPS, indicating they might not understand that HSTS does not achieve redirection on its own.
In addition, \PreloadedHTTPSetsSTS{} preloaded HSTS domains set HSTS headers over HTTP including the pinned site CryptoCat, but these sites all also set valid HTTPS HSTS headers.

These sites are clearly attempting to improve the security of their connection via HSTS but perhaps misunderstanding the nature of the technology.
The relatively high proportion of this error compared to sites successfully deploying HSTS (\TopHTTPSetsSTSPercent\%) is a clear sign of significant developer confusion.

\subsubsection*{Malformed HSTS}
We also found \TopSTSIncorrectlyFormatted{} sites setting malformed HSTS headers including notable sites like \url{paypal.com}\footnote{The error at PayPal was particularly interesting as a PayPal engineer was the lead author of the HSTS standard~\cite{rfc6797}.} and \url{www.gov.uk}. 
The most common mistake was including more than one value for \texttt{max-age}.
For example, www.gov.uk's header includes the key-value pair \texttt{strict-transport-security: max-age=31536000, max-age=31536000, max-age=31536000;}. 
In all cases of multiple \texttt{max-ages}, the \texttt{max-age} value was the same.
We confirmed Chrome and Firefox will tolerate this mistake without harm, but this technically violates the specification.
We also found 3 setting simply a value without the required key \texttt{max-age=}. For example,
\url{www.pringler.com} sets the header \texttt{strict-transport-security: 2678400}).
This results in the header being ignored by the browser.
We saw a further \TopSTSMaxAgeNegative{} sites setting a negative value for \texttt{max-age} which causes the header to be ignored.

\subsubsection*{HSTS redirection to HTTP}
We also observed \TopSTSRedirectsToHTTP{} sites which correctly set an HSTS header via HTTPS but then immediately redirected to HTTP (where several then set a meaningless HSTS header). For example, \url{https://www.blockchain.info} sets an HSTS header while redirecting to \url{http://blockchain.info}. HSTS-compliant browsers handle this error and instead to go to \url{https://blockchain.info} which responds successfully, but a non-HSTS-complaint browser would be redirected back to HTTP which is clearly not the site's intention. In addition, \PreloadedSTSRedirectsToHTTP{} preloaded HSTS domains use 30x redirects from the listed preloaded domain to HTTP, 5 of which redirect back to HTTP versions of the same base domain which is still protected by the preload list. The Minnesota state health insurance exchange website, \url{mnsure.org}, is both preloaded as HSTS and sets a dynamic HSTS header but still responds with a 301 code (moved permanently) to \url{http://www.mnsure.org/}.
HSTS-compliant browsers will follow this redirect but then still upgrade the connection to \url{https://www.mnsure.org/}.
%You end up at https://blockchain.info but I could not track down the exact path. https://www.blockchain.info->http://blockchain.info->https://blockchain.info??? This would be a great example to talk about if we knew exactly what was going on. 
%https://www.opensuse.org is an example of https site with valid HSTS redirecting to HTTP, but you end up at https://www.opensuse.org/en. What is going on? You can find a list of more examples in the sts\_data.tex

\subsection{Dynamic pinning (HPKP) deployment}
\label{sec:dynamic_pinning}

Dynamic pins, as specified by the HPKP protocol, are not yet implemented by any major browser.
Nevertheless, we observed many sites already setting valid HPKP headers indicating they would like to use pinning.

We found 18 domains attempting to set dynamic key pins (of which 7 also had a preloaded HSTS policy). %(\TopKEYPINAttempted{} from the top million and \PreloadedKEYPINAttempted{} from the preload list with 3 overlapping).
%Of the 18 total key pins, only \jbcomment{check this?} 12 (67\%) where valid key pins which were actually satisfied by the domain's currently served certificates.
We found a comparable rate of errors with dynamic key pins as with dynamic HSTS with only 12 of 18 (67\%) setting pins securely.
As with dynamic HSTS, short validity periods were a major issue: 5 set key-pin values of 10 minutes or less (1 at only 60 seconds) and 1 was incorrectly formatted without any \texttt{max-age}.
It appears most of the domains experimenting with the header are small security-focused sites, with none ranked in the Alexa top 10,000 most popular sites.

In addition, we found two new errors not present with HSTS.
\url{Amigogeek.net} dynamically pins to a hash that is not found with SHA-1 or SHA-256 in any of the certificates presented by that domain.
Segu-info.com.ar mislabels a SHA-1 hash as SHA-256. 
Both these issues would result in a standards-compliant user agent ignoring these policies.

Most of the key pins we observed were specified as SHA-256 hashes although the standard allows either SHA-1 or SHA-256.
This was somewhat surprising as SHA-1 hashes are smaller and therefore more efficient to transmit and all preloaded pins are specified as SHA-1 hashes.

Finally, we observed little use of the HPKP standard's error reporting features.
Only one domain (\url{www.mnot.net}) set a \texttt{Public-key-pins-report-only} header to detect (but not block) pinning errors and only one domain (\url{freenetproject.org}) specified the \texttt{report-uri} directive to receive error reports.

%TODO: look more at subdomains.

%Interestingly, Netflix also sets a max age of 0 but then redirects to https://www.netflix.com/?tcw=2. 

%\subsubsection{HTTPS Everywhere}
%Of course, this approach can't scale to support the entire web.
%Based on data observing during web crawls performed for Google search, over 15,000 domains are already serving HSTS headers, which would be unwieldy to maintain in the browser and check upon every connection request.\mkcomment{We only found 150 HSTS headers in the top 10000.....15,000 domains with HSTS????}\footnote{TODO how does this compare to HTTPS everywhere?}


