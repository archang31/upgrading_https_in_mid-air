%!TEX root = main.tex
\section{Overview of web security technologies}

The core protocols of the World Wide Web, namely HTTP and HTML, were not designed with security in mind.
As a result, a series of new technologies have been gradually tacked on to the basic web platform.
All of these are to some extent weakened by backwards-compatibility considerations.
In this section we provide an overview of relevant web security concepts that we will study in this paper.

\subsection{HTTPS and TLS/SSL}

HTTPS is the common name for ``HTTP over TLS''~\cite{rfc2818} which combines normal HTTP traffic with the TLS~\cite{rfc2246,rfc4346,rfc5246} (Transport Layer Security) protocol instead of basic (insecure) TCP/IP.
Most HTTPS implementations will also use the older SSL v3.0 (Secure Sockets Layer) protocol~\cite{rfc6101} for backwards compatibility reasons although it contains a number of cryptographic weaknesses.
TLS and SSL are often used interchangeably to describe secure transport; in this paper we will strictly refer to TLS with the understanding that our analysis applies equally to SSL v3.0.

The goals of TLS are confidentiality against eavesdroppers, integrity against manipulation by an active network adversary, and authenticity by identifying one or both parties with a certificate.
The main adversary in TLS is typically called a \emph{man in the middle} (MitM) or \emph{active network attacker}, a malicious proxy who can intercept, modify, block, or redirect all traffic.
Formally, this adversary is referred to as a Dolev-Yao attacker model~\cite{dolev83}.
We will not discuss cryptographic issues with TLS; Clark and van Oorschot provide a thorough survey~\cite{clark13}.

\subsubsection{Certificates and Certification Authorities}
The ultimate goal of HTTPS is to bind the communication channel to the legitimate server for a given web domain, and is achieved with the use of server certificates in TLS.\footnote{TLS also supports mutual authentication in which both client and server are identified with a long-term certificate.
However, in nearly all use on the web only the server is authenticated and the client authentication is left for higher-level protocols (typically passwords submitted over HTTPS).}
Names are bound at the domain level and are sometimes referred to as ``hostname,'' ``host,'' or ``fully-qualified domain name''.
In this paper, we'll always use ``domain'' to refer to a fully-qualified domain name.
We'll use the term ``base domain'' to refer to the highest-level non-public domain in a fully-qualified domain name, also sometimes referred to as ``public suffix plus 1'' (PS+1).\footnote{Detecting base domains is not as simple as finding the domain immediately below the top-level domain (TLD+1) due to the existence of \emph{public suffixes} of more than one domain, such as \texttt{.ca.us} or \texttt{.ac.uk}. We use Mozilla's public suffix list (\url{https://publicsuffix.org/}) as the canonical list.}
For example, for \texttt{www.example.com} the base domain is \texttt{example.com}.

HTTPS clients will check that the ``common name'' field in the server's presented certificate matches the domain for each HTTP request.
The exact rules are somewhat complicated~\cite{rfc2459} enabling the use of wildcards and the ``subject alternative name'' extension to allow certificates to match an arbitrary number of domains.

If name matching fails or a certificate is expired, malformed, or signed by an unknown or untrusted certificate authority, HTTPS clients typically show the user a warning.
Browser vendors have made the warnings more intrusive over time and click-through rates have declined significantly~\cite{sunshine09,akhawe13a} to below 50\% for Firefox.
This decrease is generally considered a positive development as the vast majority of HTTPS warning messages represent false positives due to server misconfigurations or expired certificates~\cite{akhawe13b,huang14}.

\subsection{Strict transport security}

Because a large portion of web sites only support insecure HTTP, user agents must support both HTTP and HTTPS.
Many domains serve traffic over both HTTP and HTTPS.
This enables an active network attacker to attempt to downgrade security to plain HTTP by intercepting redirects from HTTP to HTTPS or rewriting URLs contained in an HTTP page to change the protocol from HTTPS to HTTP.
Such an attack is called an \emph{SSL stripping} or \emph{HTTPS stripping} attack.
While the threat has long been known~\cite{ornaghi03}, it gained increased attention in 2009 when the popular \emph{sslstrip} software package was released to automate the attack~\cite{marlinspike09}.

Browsers use graphical indicators to tell the user whether a connection is made over HTTP or HTTPS.
Typically, this UI includes showing \texttt{https} in the browser's address bar and a padlock icon.
However, user studies have indicated that the vast majority of users (upwards of 90\%) do not notice if these security indicators are missing and are still willing to transmit sensitive data such as passwords or banking details~\cite{schechter07}.
Thus, it is insufficient to rely on users to detect if their connection to a normally-secure server has been downgraded to HTTP by an attacker.

%In addition to HTTPS stripping, user data may be put at risk simply by web developer errors.
%Many web sites, for example, load scripts or other active content over plain HTTP into pages served over HTTPS.
%These scripts can be modified by an attacker

To counter the threat of HTTPS stripping, Jackson and Barth proposed ``ForceHTTPS''~\cite{jackson08} to enable servers to request clients only communicate over HTTPS.
Their proposal was ultimately renamed ``HTTP strict transport security'' and standardized in RFC 6797~\cite{rfc6797}.\footnote{Jackson and Barth~\cite{jackson08} originally proposed that servers would request HSTS status by setting a special cookie value, but this was ultimately changed to be an HTTP header.}

\subsubsection{HSTS security model}
HSTS works as a binary (per domain) security policy.
Once set, the user agent must refuse to send any traffic to the domain over plain HTTP.
Any request which would otherwise be transmitted over HTTP (for example, if the user clicks on a link with the \texttt{http} scheme specified) will be upgraded from HTTP to HTTPS.

In addition to upgrading all traffic to HTTPS, the HSTS specification recommends two other changes.
First, any TLS error (including certificate errors) should result in a \textit{hard fail} with no opportunity for the user to ignore the error.
Second, it is recommended (non-normatively) that browsers disable the loading of insecure resources from an HSTS-enabled page; this policy has since been adopted by Chrome and Firefox for all HTTPS pages even in the absence of HSTS (see Section~\ref{sec:mixed_content}).

By default, HSTS is declared for a specific fully-qualified domain name, though there is an optional \texttt{includeSubDomains} directive which applies to all subdomains of the domain setting the policy.
For example, if \texttt{example.com} sets an HSTS policy with \texttt{includeSubDomains}, then all traffic to \texttt{example.com} as well as \texttt{a.example.com} and \texttt{b.a.example.com} must be over HTTPS only.\footnote{Note that \texttt{includeSubDomains} covers subdomains to arbitrary depth unlike wildcard certificates, which only apply to one level of subdomain~\cite{rfc2459}.}

Note that while HSTS requires a valid TLS connection, it places no restrictions on the set of acceptable certificates beyond what the user agent would normally enforce.
That is, HSTS simply requires \emph{any valid TLS connection} with a trusted certificate.
It is not designed as a defense against insecure HTTPS due to rogue certificates (see Section~\ref{sec:pinning_bg}), only against the absence of HTTPS completely.
%Its presence is \textbf{invisible to users}, requiring no user decisions for security.
%It is also \textbf{incrementally deployable}, with individual domains gaining protection unilaterally by opting in.
%HSTS-protected domains can easily perform \textbf{secure introduction} of users to other HSTS-protected domains by serving HTTPS links, with no vulnerability to a network attacker stripping the HSTS upgrade for the new domain.
%Secure introduction enables users to more easily change the parties they rely on for security (\textbf{trust agility}) and, critically, to more intuitively understand whom they are trusting when connecting to a new domain (\textbf{trust affordance}).

\subsubsection{HSTS headers}
The primary means for a server to establish HSTS is by setting the HTTP header~\cite{rfc6797} \texttt{Strict-Transport-Security}.
Compliant user agents will apply an HSTS policy to a domain once the header has been observed over an HTTPS connection with no errors.
Note that setting the header over plain HTTP has no effect although a number of sites do so anyway (see Section~\ref{sec:hsts_over_http}).

In addition to the optional \texttt{includeSubDomains} directive, an HSTS header must specify a \texttt{max-age} directive instructing the user agent on how long to cache the HSTS policy.
This value is specified in seconds and represents a commitment by the site to support HTTPS for at least that time into future.
It is possible to ``break'' this commitment by serving an HSTS header with \texttt{max-age=0} but this directive must be served over HTTPS.

%Because compliant web browsers\footnote{As of this writing, HSTS is supported by Chrome, Firefox, and Opera but not Internet Explorer or Safari.} won't communicate with a server over HTTP once an HSTS header has been observed, the protocol requires a firm commitment to support HTTPS for the specified time period.

For regularly-visited HSTS domains (at least once per \texttt{max-age} period), the policy will be continually updated and prevent HTTP traffic indefinitely.
This can be described as a \emph{continuity} policy.\footnote{HSTS can also be described as a ``trust-on-first-use'' (TOFU) scheme. We prefer the  term \emph{continuity} which does not imply permanent trust after first use.}
% can describe protocols like TACK which do not afford full trust on initial connection.
A known shortcoming of HSTS is that it can not protect initial connections or connections after extended inactivity or flushing of the browser's HSTS state.
HSTS policy caching may also be restricted by browser privacy concerns.
For example, policies learned during ``private browsing'' sessions should be discarded because they contain a record of visited domains.

HSTS is also vulnerable to an attacker capable of manipulating the browser's notion of time so that it thinks a policy has expired, for example by manipulating the NTP protocol~\cite{selvi14}.

\subsubsection{HSTS preloads}

To address the vulnerability of HTTPS stripping before the user agent has visited a domain and observed an HSTS header, Chrome and Firefox both now ship with a hard-coded list of domains receiving a preloaded HSTS policy.%\footnote{A small number of domains are designated to only receive HSTS protection if the client TLS library supports the Server Name Indication (SNI) extension~\cite{rfc6066}. Because most web clients now support SNI, in this paper we evaluate these preloaded policies similarly to the others.} 
This approach reduces security for preloaded domains to maintaining an authentic, up-to-date browser installation.

Preloaded domains receive an automatic HSTS policy from the browser and may optionally specify \texttt{includeSubDomains}.\footnote{The syntax between HSTS preloads and HSTS headers is unfortunately incompatible, with \texttt{includeSubDomains} specified in the former and \texttt{include\_subdomains} used in the latter along with other minor details we will omit for clarity.}
There is no per-domain \texttt{max-age} specification, however in Chrome's implementation, the entire preload list has an expiration date if the browser is not regularly updated.

\subsubsection{HTTPS Everywhere}
The EFF's HTTPS Everywhere browser extension\footnote{\url{https://www.eff.org/https-everywhere}} (available for Chrome and Firefox) provides similar protection for a much larger list (currently over 5,000 domains).
It has been available since 2011.
The HTTPS Everywhere extension relies on a large group of volunteers to curate a preload list in a distributed manner.
Because it is an optional (though popular) browser extension, HTTPS Everywhere is willing to tolerate occasional over-blocking errors in return for increased coverage compared to the more conservative preload lists.
Because HTTPS Everywhere is crowd-sourced, errors are due to the developers and not site operators themselves.
Hence we do not study it in this work.

\subsection{Key pinning}
\label{sec:pinning_bg}

HSTS is useful for forcing traffic to utilize HTTPS; however, it has no effect against an attacker able to fraudulently obtain a signed certificate for a victim's domain (often called a \textit{rogue certificate}) and use this certificate in a man-in-the-middle attack.
Because every trusted root in the browser can sign for any domain, an attacker will succeed if they are able to obtain a rogue certificate signed by \emph{any} trusted root (of which there are hundreds~\cite{ristic10,eckersley11,kasten13}).
This vulnerability has long been known and security researchers have obtained several rogue certificates by exploiting social engineering and other flaws in the certificate authority's process for validating domain ownership~\cite{clark13}.

However, in 2010 it was reported publicly for the first time that commercial software was available for sale to government agencies to utilize rogue certificates to intercept traffic en masse~\cite{soghoian12}.
This report raised the concern of governments using \emph{compelled certificates} obtained by legal procedures or extralegal pressure to perform network eavesdropping attacks.

In addition to the risk of government pressure, a number of high-profile CA compromises have been detected since 2011\footnote{It should be noted that most of these issues were detected due to Chrome's deployment of key pinning. It is possible a large number of CA compromises occurred before pinning was deployed but evaded detection.}~\cite{niemann13} including security breaches at Comodo and DigiNotar (which has since been removed as a trusted CA from all browsers) and improperly issued subordinate root certificates from TrustWave and T\"{u}rkTrust.
Collectively, these issues have demonstrated that simply requiring HTTPS via HSTS is not sufficient given the risk of rogue certificates.

\subsubsection{Pinning security model}
Key pinning specifies a limited set of public keys which a domain can use in establishing a TLS connection.
Specifically, a key pinning policy will specify a list of hashes (typically SHA-1 or SHA-256) each covering the complete Subject Public Key Info field of an X.509 certificate.
To satisfy a pinning policy, a TLS connection must use a certificate chain where at least one key appearing in the chain matches at least one entry in the pin set.
This enables site operators to pin their server's end-entity public key, the key of the server's preferred root CA, or the key of any intermediate CA.
Pinning makes obtaining rogue certificates much more difficult, as the rogue certificate must also match the pinning policy which should greatly reduce the number of CAs which are able to issue a usable rogue certificate.

In fact, the browsers' default policy can be viewed as ``pinning'' all domains with the set of all trusted root certificate authorities' keys.
Explicit pinning policies further reduce this set for specific domains.
Much like HSTS, pinning policies apply at the domain level but an optional \texttt{includeSubDomains} directive extends this protection to all subdomains.

The risk of misconfigured pinning policies is far greater than accidentally setting HSTS.
HSTS can be undone as long as the site operator can present any acceptable certificate, whereas if a site declares a pinning policy and then can't obtain a usable certificate chain satisfying the pins (for example, if it loses the associated private key to a pinned end-entity key), then the domain will effectively be ``bricked'' until the policy expires.
For this reason, pinning policies often require a site to specify at least two pins to mitigate this risk.

\subsubsection{Pinning preloads}

Chrome has deployed preloaded pinning policies since 2011, although only a handful of non-Google domains currently participate.
Firefox shipped preloaded pinning in 2014 with policies for a subset of Chrome's pinned domains plus several Mozilla domains.
Like with preloaded HSTS, preloaded pinning policies have no individual expiration date but the entire set expires if the browser is not frequently updated.
No other browsers have publicly announced plans to implement preloaded pinning.

\subsubsection{Pinning headers (HPKP)}

A draft RFC specifies HTTP Public Key Pinning (HPKP) by which sites may declare pinning policies via the \texttt{Public-Key-Pins} HTTP header~\cite{evans12}.
The syntax is very similar to HSTS, with an optional \texttt{includeSubDomains} directive and a mandatory \texttt{max-age} directive.
Pinning policies will only be accepted when declared via a valid TLS connection which itself satisfies the declared policy.

Unlike HSTS, the HPKP standard adds an additional \texttt{Public-Key-Pins-Report-Only} header.
When this policy is declared, instead of failing if pins aren't satisfied, the user agent will send a report to a designated address.
This is designed as a step towards adoption for domains unsure if they will cause clients to lose access.
Additionally, the standard recommends that user agents limit policies to 60 days of duration from the time they are declared, even if a longer \texttt{max-age} is specified, as a hedge against attackers attempting to brick a domain by declaring a pinning policy which the genuine owner can't satisfy.

Currently, no browsers support HPKP and the standard remains a draft.
Chrome and Firefox have both announced plans to support the standard when it is finalized.

%-Discussion about Public key pinning focusing on its limited deployment\\
%-Discuss that only chrome supports pinning with firefox intending for a future dynamic implementation\\
%Currently, there are \Pinsets pinsets from 6 companies(Google, Twitter(2), Tor(2), Lavabit, CryptoCat and Dropbox) as well as a test pinset. Even with the very limited test group active public key pinning, we found several errors that migiate some of the addition security features from pinning.

