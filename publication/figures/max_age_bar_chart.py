#!/usr/bin/python
# -*- coding: UTF-8 -*-


#!/usr/bin/env python
# a stacked bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot, rc, rcParams

#Parameters for LaTex output
rc('font',**{'family':'serif','serif':['Computer Modern']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}",]

def get_data(file_name):
    f = open(file_name, 'r')
    total_age_list = list()
    zero_count = 0
    hour_count = 0
    day_count = 0
    month_count = 0
    half_year_count = 0
    year_count = 0
    five_year_count = 0
    over_five_count = 0
    for line in f:
        age = int(line.strip())
        total_age_list.append(age)
        if age == 0:
            zero_count += 1
        elif age <= 3600:
            hour_count += 1
        elif age <= 86400:
            day_count += 1
        elif age <= 2592000:
            month_count += 1
        elif age <= 15768000:
            half_year_count += 1
        elif age <= 31536000:
            year_count += 1
        elif age <= 157680000:
            five_year_count += 1
        else:
            over_five_count += 1
    bins = [zero_count, hour_count, day_count, month_count, half_year_count,
               year_count, five_year_count, over_five_count]
    percents = list()
    for age in bins:
        percents.append(int(float(age)*float(100) / float(len(total_age_list))))
    return [bins, percents]

results = get_data('max_age_top_data.csv')
top_bin = results[0]
top_percent = results[1]
results = get_data('max_age_preloaded_data.csv')
preloaded_bin = results[0]
preloaded_percent = results[1]
results = get_data('max_age_combined_keypin_data.csv')
top_keypin_bin = results[0]
top_keypin_percent = results[1]
print(top_keypin_bin)
print(top_keypin_percent)
N = 8
#oldAgeBins  = (11, 4.0, 4, 3.5, 6,  45,  7,  3)
ind = np.arange(N)    # the x locations for the groups
ind = ind + 1
width = 0.25       # the width of the bars: can also be len(x) sequence


fig = plt.figure()
ax1 = fig.add_subplot(111)
p1 = ax1.bar(ind, top_percent, width, color='#ff7777', edgecolor='black')
p2 = ax1.bar(ind+width, preloaded_percent, width, color='#7777ff', edgecolor='black')
#p3 = ax1.bar(ind-width, top_keypin_percent, width, color='#77ff77', edgecolor='black')
x_axis_label = 'zero', '$\\le 1$\n hour', '$\\le 1$\n day', '$\\le 1$\n month', \
               '$\\le 6$\n months', '$\\le 1$\n year', '$\\le 5$\n years', '$>5$\n years'
ax1.set_xticks(ind+width/2.)
ax1.set_xticklabels(x_axis_label)
ax1.yaxis.grid(True, color='gray')
ax1.autoscale(tight=True)
ax1.set_yticks(np.arange(0, 65, 5))
ax1.set_xlabel('\\texttt{max-age} bin value')
ax1.set_ylabel('Percentage of total max age values')
ax1.legend((p1[0], p2[0]), ('All sites', 'Sites also in preload list'), loc='upper left')

pyplot.savefig("max_age_bar_chart.pdf")