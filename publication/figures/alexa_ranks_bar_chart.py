#!/usr/bin/env python
# a stacked bar plot with errorbars
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot, rc, rcParams

#Parameters for LaTex output
rc('font',**{'family':'serif','serif':['Computer Modern']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}",]

#UPDATED: 19 NOV 2014
#1	Count	Count
#10	3	5
#100	4	21
#1000	12	70
#10000	28	83
#100000	53	103
#1000000	101	140
#10000000	181	197
#100000000	83	91
#-	277	294

N = 9
ngMeans = (3,  4, 12, 28, 53, 101, 181, 83, 277)
gMeans  = (2, 17, 58, 55, 50,  29,  16,  8,  17)
#total  = (5, 21, 70, 83, 103,140, 197, 91, 294)
ind = np.arange(N)    # the x locations for the groups
width = 0.35       # the width of the bars: can also be len(x) sequence

fig = plt.figure()
ax1 = fig.add_subplot(111)
p1 = ax1.bar(ind, ngMeans,   width, color='#ff7777')
p2 = ax1.bar(ind, gMeans, width, color='#7777ff', bottom=ngMeans)

ax1.set_xlabel('Alexa rank')
ax1.set_ylabel('Number of domains')
x_axis_label = '$<10$', '$10$--$10^2$', '$10^2$--$10^3$', '$10^3$--$10^4$', '$10^4$--$10^5$', \
               '$10^5$--$10^6$', '$10^6$--$10^6$', '$>10^7$', 'unranked'
ax1.set_xticks(ind+width/2.)
ax1.set_xticklabels(x_axis_label)
ax1.set_yticks(np.arange(0, 300, 25))
ax1.yaxis.grid(True, color='gray')
ax1.legend((p1[0], p2[0]), ('Non-Google domains', 'Google domains'), loc='upper center')

pyplot.savefig("alexa_ranks_bar_chart.pdf")



