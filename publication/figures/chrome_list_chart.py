#!/usr/bin/python
# -*- coding: UTF-8 -*-


#!/usr/bin/env python
# a stacked bar plot with errorbars
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt
from matplotlib import pyplot, rc, rcParams, dates

#Parameters for LaTex output
rc('font',**{'family':'serif','serif':['Computer Modern']})
rc('text', usetex=True)
rcParams['text.latex.preamble']=[r"\usepackage{amsmath}",]

f = open('chrome_list_over_time_data.csv', 'r')
#('Date, Pinsets, Total Entries, Base Domains, Google Base Domains, Non-Google Base Domains\n')
total_list = list()
date_list = list()
pinsets = list()
total_entries = list()
base_domains = list()
google_base_domains = list()
non_google_base_domains = list()

for line in f:
    value_list = line.strip().split(',')
    #in the form 12-Apr-2014
    value_list[0] = datetime.strptime(value_list[0].strip(), '%d-%b-%Y')
    total_list.append(value_list)

#The items in the file are not sorted. This sorts them based on the first value(time)
for item in sorted(total_list):
    date_list.append(item[0])
    pinsets.append(item[1].strip())
    total_entries.append(item[2].strip())
    base_domains.append(item[3].strip())
    google_base_domains.append(item[4].strip())
    non_google_base_domains.append(item[5].strip())
date_list = dates.date2num(date_list)


fig = plt.figure()
ax1 = fig.add_subplot(111)
#ax1.set_title("Chrome Preloaded List Growth Over Time")
ax1.set_xlabel('Date')
ax1.set_ylabel('Entries')
ax1.plot_date(date_list, total_entries, c='r', label='Total entries', fmt="r-")
ax1.plot_date(date_list, base_domains, c='m', label='Base domains', fmt='r-')
ax1.plot_date(date_list, google_base_domains, c='g', label='Google base domains', fmt='r-')
ax1.plot_date(date_list, non_google_base_domains, c='y', label='Non-Google base domains', fmt='r-')
#ax1.grid(True)
ax2 = ax1.twinx()
ax2.plot_date(date_list, pinsets, c='b', label='Pinsets', fmt="r--")
ax2.set_ylabel('Pinsets')
ax2.set_ylim([0,12])
# ask matplotlib for the plotted objects and their labels
lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc='upper left')
fig.autofmt_xdate()
#plt.show()
pyplot.savefig("chrome_list_over_time.pdf")

