%!TEX root =  main.tex
\section{Related work}

\subsection{Empirical web security studies}

Our work fits nicely into a long and fruitful line of measurement studies of web security, covering a wide variety web security topics such as authentication cookies~\cite{fu01}, \texttt{http-only} cookies~\cite{zhou10}, password implementations~\cite{bonneau10b}, third-party script inclusions~\cite{yue09,nikiforakis12}, third-party trackers~\cite{mayer12,roesner12}, Flash cross-domain policies~\cite{venkataraman12}, and OpenID implementations~\cite{sun12,wang12}.
A classic problem is detecting cross-site scripting vulnerabilities which is practically its own sub-field of research~\cite{di04,jovanovic06,wassermann08,nunan12}.
A common model for this research is exploration and analysis of an emerging threat on the web, followed by measurement and crawling to detect its prevalence and character.
A desirable research outcome is for automated detection to be built-in to web application security scanners~\cite{kals06,curphey06,vieira09}.

Ultimately, browsers themselves have come to absorb some of this logic.
For example, Chrome and Firefox both now warn developers about basic mixed-content errors in their built-in developer consoles.
Several issues identified in our research are solid candidates for inclusion in future automated tools: cookies vulnerable to theft, pinning mixed content, \extendedversion{}{and} some types of erroneous or short-lived HSTS headers\extendedversion{, and insecure links to HSTS domains}{}.
%As a first step we plan to release our own analysis code as an open-source add-on for WPM and Selenium.

\subsection{Empirical studies of HTTPS and TLS}

A significant amount of research has also focused specifically on empirical errors with HTTPS and TLS, of which Clark and van Oorschot provide the definitive survey~\cite{clark13}.
Important studies of cryptographic vulnerabilities have included studies of key revocation after the Debian randomness bug~\cite{yilek09}, studies of factorable RSA keys due to shared prime factors~\cite{lenstra12,heninger12}, studies of elliptic curve deployment errors in TLS~\cite{bos13}, forged TLS certificates in the wild~\cite{huang14} and multiple studies of key sizes and cipher suites used in practice~\cite{ristic10,holz11,amann12}.
Our work is largely distinct from these in that we focus on two new aspects of HTTPS (pinning and strict transport security) which are vulnerabilities at the HTTPS (application) level rather than the TLS (cryptographic) level.

Perhaps the most similar study to ours was by Chen et al.~\cite{chen09} which focused on mixed-content and stripping vulnerabilities.
This study was performed prior to the advent of HSTS, pinning, and mixed content blocking.
Hence, that work can be viewed as a first-generation study compared to our second-generation study based on newly introduced security measures (although no doubt many of the original vulnerabilities are still present on the web today).

Other empirical studies of the TLS ecosystem have focused on certificate validation libraries~\cite{brubaker14}, non-browser TLS libraries~\cite{georgiev12}, the interaction of HTTPS with content-delivery networks~\cite{liang14}, and TLS implementations in Android apps~\cite{fahl12,fahl13}.
Again, these efforts all found widespread weaknesses.
A common theme is developers not correctly understanding the underlying technology and using them in an insecure manner.

\subsection{Other proposals for improving HTTPS}

We briefly overview several noteworthy proposals for further improving HTTPS.
These proposals are mainly aimed at limiting the risk of rogue certificates and hence could complement or supplant key pinning.
%HSTS is generally considered an acceptable solution to HTTPS stripping.
We exclude other web security proposals such as Content Security Policy (CSP)~\cite{stamm10}.

\subsubsection{DANE}

DNS-based Authentication of Named Entities (DANE)~\cite{rfc6698} is a proposal for including the equivalent of public key pins in DNS records, relying on DNSSEC~\cite{ateniese01} for security.
This has the advantage of avoiding the scalability concerns of preloaded pins and potentially being easier\footnote{It is an open question whether web developers are more comfortable configuring HTTP headers or DNS TXT records.} and more efficient\footnote{Unlike for HSTS which operates with relatively small headers, there is significant concern about the efficiency of setting pins in headers which may be hundreds of bytes long for complicated pin sets as seen in our study.} to configure than pins set in headers.
Unfortunately, DANE adoption is delayed pending widespread support for DNSSEC, which common browsers do not currently implement.

DANE does not currently contain support for declaring policies applicable to all subdomains.
Based on our study, we would strongly advise such support be added (and possibly turned on by default) to avoid the type of mixed content and cookie vulnerabilities we observed with pinning today.
\extendedversion{On the positive side, DANE solves the problem of weak links as user agents must resolve a new domain and therefore fetch its policy prior to the initial connection.}{}

\subsubsection{Out-of-chain key pinning}
An alternative to pinning to keys within a cite's certificate chain is to specify a separate self-managed public key which must sign all end-entity public keys, in addition to requiring a certificate chain leading to a trusted CA.
This avoids fully trusting any external CA while offering more flexibility than pinning to an enumerated set of end-entity public keys.
Conceptually, it is similar to pinning to a domain-owned key in a CA-signed, domain-bound intermediate certificate.\footnote{Domain-bound intermediates are possible using X.509's \texttt{nameConstraints} extension. However, this extension is not universally supported and non-supporting clients will reject such certificates. %Furthermore, CAs are hesitant to issue intermediate CA certificates.
}
TACK~\cite{marlinspike12} proposes distributing out-of-chain public keys using continuity,\footnote{Continuity in TACK is distinct from HSTS/HPKP in that clients only retain a TACK policy for as long into the future as the policy has been consistently observed in the past, subject to a maximum of 30 days.}
while Sovereign Keys~\cite{eckersley12} proposes using a public append-only log.

TACK explicitly does not contain support for extending policies to subdomains, instead recommending that implementers omit the domain parameter to limit cookies to one domain and/or set the \texttt{secure} flag to avoid cookie theft.
Of course, the \texttt{secure} flag is inadequate for defending against rogue certificates given that one cannot set a TACK policy for non-existent subdomains.
Sovereign Keys, by contrast, does support wildcards to extend support to subdomains.

\subsubsection{Public logging}
Due to the risk of improperly configured pinning policies causing websites to be inaccessible, some proposals aim simply to require that all valid certificates be publicly logged to ensure rogue certificates are detected after the fact.
Certificate Transparency~\cite{laurie13} (CT) is the most prominent of these efforts, recording all valid end-entity certificates in a publicly verifiable, append-only log.
%The current proposal aspires to log all certificates in use on the public Internet.
As currently proposed, clients will begin to reject certificates lacking proof of inclusion in the log after universal adoption\footnote{Even after universal adoption, clients must wait until all extant legacy certificates have expired to require CT proofs.} by all public CAs.\footnote{Private CAs, such as those used within an enterprise, are excluded.}
A somewhat-related proposal is Accountable Key Infrastructure~\cite{kim13}.

As proposed, Certificate Transparency would avoid most of the subtle issues identified in this work in that the burden on web developers is extremely low (the only requirement is to start using a CT-logged certificate prior to some future deadline).
Given the number of errors we observed in our study, this seems like a major design advantage.
However, if CT is not able to be adopted via the proposed ``flag day'' strategy, it may be necessary to distribute policies to browsers specifying which domains require CT protection.
This problem would be largely similar to distributing HSTS today. 
Thus, the lessons from our paper would apply to designing such a mechanism, which most likely would be implemented as an extra directive in HSTS itself.
%While not part of the current draft proposal, it's possible that as CT is incrementally deployed supporting domains will wish to assert a ``CT-required'' security policy through browser preloads or a continuity protocol.


%\subsection{Secure links}
%TODO
