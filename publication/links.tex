%!TEX root =  main.tex
\section{Insecure linking}
\label{sec:links}

HSTS domains are vulnerable to HTTPS stripping prior to the user making a successful initial connection if they don't have a policy preloaded in the browser.
Of course, many HSTS-compliant user agents don't have any preloaded policies.
Thus, it is critical that all references to HSTS domains should be made with URLs declaring \texttt{https} as the protocol, even if the domain has a preloaded policy in some browsers.

This is generally best practice for any site implementing HTTPS.
However, because setting an HSTS header is a strong signal that a domain should be reached via HTTPS only, we can consider any reference to an HSTS domain via an HTTP URL to be an error.
With URLs for page resources, this error results in mixed content as discussed in Section~\ref{sec:mixed_content} and can be blocked by browsers.
However, hyperlinks (\texttt{a} tags) with an HTTP URL cannot generally be blocked by the browser like mixed content because there will always be legitimate cases where sites must be linked to over HTTP only.

Getting the protocol correct on hyperlinks is essential for the end-to-end security model of HSTS with a mix of preloaded and header-declared policies.
If users initially browse mainly at large sites whose HSTS status can be preloaded (such as search engines, webmail providers and social media sites) and mostly navigate to non-preloaded HSTS domains by means of HTTPS links from these preloaded domains, they will be protected from HTTPS stripping throughout.
HTTP URLs, however, break the chain and offer an opportunity for a network attacker to block the HTTP to HTTPS redirect.
In this section we examine the prevalence of dangerous ``weak links'' to HSTS domains.

\subsection{Insecure links to HSTS domains}
\input{resources/links_table.tex}{}
%new results
%(u'python.org', 1638)
%(u'derevtsov.com', 1326)
%u'vortexhobbies.com', 1133)
%(u'twitter.com', 1050)
%(u'salserocafe.com', 762)
%(u'aclu.org', 573)
%(u'imouto.my', 475)
%(u'weblogzwolle.nl', 461)
%(u'eldietista.es', 425)
%(u'tinte24.de', 410)%
%(u'minikneet.com', 385)
%(u'tekshrek.com', 300)
%(u'codereview.appspot.com', 296)
%(u'toner24.co.uk', 268)
%(u'schachburg.de', 262)

In our depth-1 crawling of all home-pages found in the preloaded list, we observed \HTTPSRefHSTSViaHTTPAtag{} non-secure links from \HTTPSRefHSTSViaHTTPAtagDomains{} domains to preloaded HSTS sites.
For comparison, \HTTPSRefHSTSAtag{} links to preloaded HSTS sites were correctly served with the \texttt{https} protocol.
Thus, \HTTPSRefHSTSPercent\% of links are served incorrectly, each of which is a security hole.

Breaking the numbers down further though, of \HTTPSSelfRefHSTSAtag{} links between HSTS pages with a common base domain, \HTTPSSelfRefHSTSViaHTTPAtag{} were over HTTP.
For example, \url{https://play.google.com/store} links to \url{http://play.google.com} and \url{https://wiki.python.org} links to \url{http://www.python.org}  
%The \url{blog.x.com or www.x.com/blog self-linking back to http://www.x.com was also a common occurrence present on a third of the self-linked sites. 

Of the remaining \HTTPSRefHSTSExternalAtag{} links to HSTS sites which were cross-domain, \HTTPSRefHSTSExternalViaHTTPAtag{} were HTTP links.
%Editted below. Wording is still funky
These links are the most important, as they might lead a user to initially connect insecurely to an otherwise HSTS domain providing an opportunity for an attacker to intercept prior to the upgrade to HTTPS.
Because a substantial portion of them (\HTTPSRefHSTSExternalPercent\%) were HTTP, this suggests insecure linking is a widespread problem on the web.

A summary of the link targets for cross-domain links is shown in Table~\ref{tab:linked_domains} and mostly consisted of external websites linking to a few popular domains.
%We also found \HTTPSSelfRefPinnedViaHTTPAtag{} from \HTTPSSelfRefPinnedViaHTTPAtagDomains{} domains. 
We also observed persistent errors from national versions of the Google homepage (like \url{www.google.dj}) linking to maps., www. and translate.google.com subdomains.
%For example, https://google.co.uk links to http://google.co.uk, http://www.google.co.ul, http://maps.google.co.uk and http://translate.google.co.uk. 
Most Google pages also reference \url{support.google.com} in a non-secure manner.


\subsection{Insecure links to pinned domains}
Unlike the case for HSTS domains, there is currently no way to securely specify a link to pinned domains.
All links to pinned domains are inherently ``weak'' in that they rely on the browser having a preloaded pinning policies for protection.
Of course, there are no dynamically pinned domains yet, but they are expected to arrive soon which will make this a practical security concern.
Several proposals have been put forward for extending URL or HTML syntax to enable specifying key pins or other security policies in hyperlinks, such as YURLs~\cite{close04} or S-links~\cite{bonneau13}, although there are complex concerns about the interaction of these schemes with the same-origin policy~\cite{jackson08}.
The risks outlined in this section suggest that securely deploying dynamic pinning will require a secure linking scheme may need to be added to protect initial connections to pinned domains.

\begin{comment}
We also found \HTTPSRefPinnedViaHTTPAtag{} non-secure links to preloaded HSTS sites from \HTTPSRefPinnedViaHTTPAtagDomains{} domains. These links where primarily external websites linking to main pages of google subdomains like www.google.com or maps.google, to www.youtube.com, or to www.twitter.com. We also found \HTTPSSelfRefPinnedViaHTTPAtag{} from \HTTPSSelfRefPinnedViaHTTPAtagDomains{} domains. 
The vast majority of these are google search engine domains self linking to that own domain's maps, www. and translate subdomains. For example, https://google.co.uk links to http://google.co.uk, http://www.google.co.ul, http://maps.google.co.uk and http://translate.google.co.uk. 
The secure version of Google search engine sites also repeatedly reference support.google.com in a non-secure manner.
\end{comment}
