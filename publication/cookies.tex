%!TEX root =  main.tex
\section{Cookie theft}
\label{sec:cookies}

A long-standing problem with the web has been the inconsistency between the same-origin policy defined for most web content and the one defined for cookies~\cite{rfc2109,rfc2965,rfc6265}.
Per the original cookie specification~\cite{rfc2109}, cookies are isolated only by host and not by port or scheme.
This means cookies set by a domain via HTTPS will be submitted back to the same domain over HTTP~\cite{zalewski12}.
Because cookies often contain sensitive information, particularly session identifiers which serve as login credentials, this poses a major security problem.
Even if a domain \texttt{secure.com} only serves content over HTTPS, an active attacker may inject script into any page in the browser triggering an HTTP request to \texttt{http://secure.com/non-existent} and the outbound request will contain all of the users cookie's for the domain.

\subsection{\texttt{Secure} cookies}

To address this problem, the \texttt{secure} attribute for cookies was added in 2000 by RFC 2965~\cite{rfc2965}, the first update to the cookie specification.
This attribute specifies that cookies should only be sent over a ``secure'' connection.
While this was left undefined in the formal specification, all implementations have interpreted this to limit the cookie to being sent over HTTPS~\cite{rfc6265}.
A persistent issue with the \texttt{secure} attribute is that it protects read access but not write access.
HTTP pages are able to overwrite (or ``clobber'') cookies even if they were originally marked \texttt{secure}.\footnote{In fact, HTTP connections can set cookies and mark them as \texttt{secure}, in which case they won't be able to read them back over HTTP.}

\begin{comment}
A related cookie attribute is \texttt{http-only}, which prevents cookies from being read by JavaScript as a hedge against cross-site scripting attacks.
Similar to our findings, Zhou et al.~\cite{zhou10} studied \texttt{http-only} in 2010 and found uptake was very low despite a large number of websites seemingly having the ability to turn the header on.
We won't discuss this attribute further here as it doesn't have direct interactions with HTTPS.
\end{comment}

\subsection{Interaction of \texttt{secure} cookies and HSTS}

At first glance, it might appear that HSTS obviates the \texttt{secure} cookie attribute, because if a browser learns of an HSTS policy and will refuse to connect to a domain over plain HTTP at all it won't be unable to leak a secure cookie over HTTPS.
Unfortunately, there the different treatment of subdomains which means that cookies can still be leaked.

Cookies may include a \texttt{domain} attribute which specifies which domains the cookie should be transmitted to.
By default, this includes all subdomains of the specified domain, unlike HSTS which does not apply to subdomains by default.
Even more confusingly, the only way to limit a cookie to a single specific domain is to not specify a \texttt{domain} parameter at all, in which case the cookie should be limited to exactly the domain of the page that set it.
However, Internet Explorer violates the standard~\cite{rfc2965} in this case and scopes the cookie to all subdomains anyway~\cite{zalewski12}.

Thus, a well-intentioned website may expose cookies by setting HSTS but not the \texttt{secure} attribute if the HSTS policy does not specify \texttt{includeSubDomains} (which is the default) and the cookie is scoped to be accessible to subdomains (which occurs whenever any \texttt{domain} attribute is set).
For example, suppose \texttt{example.com}, a domain which successfully sets HSTS without \texttt{includeSubDomains}, sets a cookie \texttt{session\_id=$x$} with \texttt{domain=example.com} but does not set \texttt{secure}.
This cookie will now be transmitted over HTTP to any subdomain of \texttt{example.com}.
The browser won't connect over HTTP to \texttt{example.com} due to the HSTS policy, but will connect to \texttt{http://nonexistent.example.com} and leak the cookie value $x$ over plain HTTP.

An active attacker can inject a reference to \texttt{http://nonexistent.example.com} into any page in the browser, making this cookie effectively accessible to any network attacker despite the domain's efforts to enforce security via HSTS.
Thus, we consider this to be a bug as it very likely undermines the security policy the domain administrator is hoping to enforce.
HSTS does not serve as an effective replacement for \texttt{secure} cookies for this reason and it is advisable that HSTS sites generally mark all cookies as \texttt{secure} unless they are specifically needed by an HTTP subdomain.

\subsubsection*{Empirical results}
\label{sec:hsts_cookie_results}

\input{resources/hsts_cookie_stats.tex}
 
This vulnerability requires three conditions: an HSTS domain with a non-HSTS subdomain (a ``hole''), cookies scoped to that subdomain, and those cookies to not be marked with \texttt{secure}.
Table~\ref{tab:HSTSCookies} summarizes the number of domains vulnerable to the attack, broken down by these three conditions.

\begin{comment}
A large number of domains had a non-HSTS subdomain: \HSTSDomainsWithHole{} of \HSTSBaseDomains{} (\TopDynamicHSTSDomainsWithHolePercent{}) and \TopDynamicHSTSDomainsWithHole{} of \TopSTSValid{} (\TopDynamicHSTSDomainsWithHolePercent{}) base domains with a dynamic HSTS policy.
Of these, many set cookies which were scoped to these subdomains, 
We checked for cookies vulnerable to theft over HTTP in our crawl of all HSTS domains.

Overall we observed and domains setting a dynamic HSTS header.
We observed a cookies scoped to non-HSTS subdomains at \HSTSDomainsWithLeakableCookie{} of \HSTSBaseDomains{} base domains (\HSTSDomainsWithLeakableCookiePercent\%) with a preloaded HSTS policy and  \TopDynamicHSTSDomainsWithLeakableCookie{} of \TopSTSValid{} (\TopDynamicHSTSDomainsWithLeakableCookiePercent\%) base domains with a dynamic HSTS policy.

Of these, the vast majority \PreloadedCookiePercentNotSecure{}\% \TopDynamicHSTSDomainsWithLeakableCookiePercent\% & 
%\PreloadedCookieTotal{} 832

Cookies from \PreloadedCookieDomains{} Base Domains which were accessible to non-HSTS subdomains.
The vast majority of these, \PreloadedCookieNotSecure{} (\PreloadedCookiePercentNotSecure{}\%), did not set the \texttt{secure} attribute meaning they are vulnerable to theft by an active network attacker. 
\end{comment}

This issue was present on several important domains like Paypal, Lastpass, and USAA, and the cookies included numerous tracking and analytics cookies, user attributes cookies like county code and language, and unique session identification cookies like ``guest\_id,'' ``VISITORID'', and ``EndUserId''.
Stealing these cookies can be a violation of user's privacy and may be used to obtain a unique identifier for users browsing over HTTPS.
Encouragingly, however, all authentication cookies we were able to identify for these sites were marked as \texttt{secure} and hence could not be leaked over HTTP.
This suggests that the \texttt{secure} attribute is relatively well-understood by web developers.

\subsection{Interaction of cookies and pinning}
A similar issue exists for pinned domains whereby a cookie may leak to unprotected subdomains.
For example, if \texttt{example.com}, a pinned domain without \texttt{includeSubDomains}, sets a cookie \texttt{session\_id=$x$} with \texttt{domain=example.com}, the cookie will be transmitted over unpinned HTTPS to any subdomain of \texttt{example.com}.
Note that even setting the \texttt{secure} flag doesn't help here---this will only require the cookie to be sent over HTTPS but an attacker able to compromise HTTPS with a rogue certificate will still be able to observe the value of the cookie.

Because there is no equivalent attribute to \texttt{secure} which would require a cookie to be sent over a pinned connection, there is currently no good fix for this problem.
The only way to securely set cookies for a pinned domain is either to limit them to a specific domain (by not setting a \texttt{domain} parameter) or to specify \texttt{includeSubDomains} for the pinning policy.

\subsubsection*{Empirical results}\label{sec:pinned_cookie_results}

\input{resources/cookie_figure.tex}

We checked for cookies vulnerable to theft over non-pinned HTTPS from all pinned domains in the Chrome preload list.
We observed \PinnedCookieTotal{} cookies on \PinnedCookieDomains{} pinned domains which are accessible by non-pinned subdomains, as summarized in Table~\ref{tab:PinnedCookies}. 
%(twitter.com, crypto.cat, play.google.com,  www.gmail.com, gmail.com, googlemail.com)
As mentioned above, there is no equivalent of the \texttt{secure} attribute to limit cookies to transmission over a pinned connection, meaning all of these cookies are vulnerable.

Interestingly, the majority of these cookies are also in fact vulnerable to theft over plain HTTP as \PinnedCookieNotSecure{} of these cookies (\PinnedCookiePercentNotSecure{}\%) did not set the \texttt{secure} attribute.\footnote{Note that because the Chrome preload file requires only one line per domain for both pinning and HSTS policies, every domain with a non-\texttt{includeSubDomains} pinning policy also has a non-\texttt{includeSubDomains} HSTS policy (if any HSTS policy at all). 
%A patch is currently pending to allow the two policies to be specified at different granularities.
}
This suggests that even if an attribute existed to limit cookies to a pinned connection, the semantics of this problem are complex enough that developers may not always deploy it.

Unlike our results for HSTS domains, we crawled 4 of the pinned sites with login cookies and we did observe several authentication cookies vulnerable to theft.
Notably, authentication cookies\footnote{Identifying exactly which set of cookies is sufficient to hijack a user's session can be a difficult problem~\cite{zhou10} but we confirmed manually for Twitter and Facebook that the vulnerable cookies were sufficient to log in.} for both Twitter (with it's critical \texttt{auth\_token} cookie) and Facebook (with \texttt{c\_user} and \texttt{xs}) are both vulnerable. Both are scoped to \url{.twitter.com} and \url{.facebook.com}, respectively, meaning they are visible to all subdomains even though neither Twitter nor Facebook set \texttt{includeSubDomains} for their base domain.
Thus an attacker can steal valid authentication cookies for either website without triggering the pinning policy.

We responsibly disclosed this vulnerability to both sites.
Unfortunately, in both cases it is considered unfixable at the moment as neither site is capable of setting \texttt{includeSubDomains} for their preloaded HSTS policy.
A fix has been proposed which will allow these sites to specify \texttt{includeSubDomainsForPinning} in the preload file.

Dropbox, by contrast, sets pins for \url{dropbox.com} without \texttt{includeSubDomains} but scoped its login cookies to \url{.www.dropbox.com}, for which \texttt{includeSubDomains} is set, preventing the cookies from being vulnerable.

%Google and Dropbox did not have single session cookies but we confirmed that the set of cookies leaked by both Dropbox and Google were sufficient for authentication.
%Thus we found authentication credentials were vulnerable at all three pinned domains with a login system.
%We also confirmed that the set of cookies leaked from Dropbox.com cookies (a subset of the bjar, vlid, t, jar, l and lid cookies), and all the required Google login cookies (SAPISID, SID at a minimum plus and a small subset of other leakable cookies).

Google's case is considerably more complex but instructive.
While the majority of Google's pinning entries set \texttt{includeSubDomains} including \url{google.com} and thus would appear to avoid this error, until August 2014 \url{play.google.com} did not set \texttt{includeSubDomains}.\footnote{Chrome's preload list previously included the comment ``play.google.com doesn't have include\_subdomains because of crbug.com/327834;'' however, this link was never valid and it isn't clear what the original bug was.}
For subdomains \url{*.play.google.com}, the \url{play.google.com} this entry overrode the less specific \url{google.com} entry as per RFC 6797\cite{rfc6797}.
%For example, the Google cookie \texttt{NID} is set on many Google domains with the domain value \texttt{Domain=.google.com}, enabling access to this cookie for all google.com subdomains.
%In addition, the play.google.com HTTPS header does not include any HSTS values.
As a result, any subdomain of \url{play.google.com} like evil.play.google.com was not be bound by the Google pin set and an adversary with a rogue certificate for one of these domains would have access to all of cookies scoped for \url{*.google.com} there.
However, Google limits its ``master'' authentication cookies' scope to \url{accounts.google.com}, which cannot be accessed by \url{*.play.google.com}, but assigns per-subdomain authentication cookies as needed.
Thus this vulnerability was limited to login cookies giving access to \url{play.google.com} only.
Google was aware of this vulnerability when we initially disclosed it and has since fixed it by extending \texttt{includeSubDomains} to cover \url{play.google.com}.

\subsubsection*{Recommendation to browsers}
\label{sec:cookie_rec}
As a result of our findings with pinned cookies, we recommend that browser vendors extend the semantics of the \texttt{secure} attribute for cookies as follows: if a cookie is set by a domain with a pinning policy and marked \texttt{secure}, the cookie should only be transmitted over HTTPS connections which satisfy the pinning policy of the domain setting the cookie.
This is a relatively simple fix which would close the security holes we found without introducing any new syntax.
This is also a reasonable interpretation of the original specification for \texttt{secure}, which never limited the syntax to mean simply HTTPS.
Given that a large number of developers have successfully learned to mark important cookies as \texttt{secure}, it makes to extend this in a natural way as pinning and other HTTPS upgrades are deployed.

