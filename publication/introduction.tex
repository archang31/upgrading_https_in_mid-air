%!TEX root =  main.tex
\section{Introduction}

HTTPS~\cite{rfc2818}, which consists of layering HTTP traffic over the TLS/SSL encrypted transport protocols~\cite{rfc5246} to ensure confidentiality and integrity, is the dominant protocol used to secure web traffic. 
Though there have been many subtle cryptographic flaws in TLS itself (see~\cite{clark13} for an extensive survey), the most significant problem has been inconsistent and incomplete deployment of HTTPS.
Browsers must seamlessly support a mix of HTTP and HTTPS connections, enabling \emph{stripping attacks}~\cite{marlinspike09} whereby network attackers attempt to downgrade a victim's connection to insecure HTTP despite support for HTTPS at both the server and client.

%Two important features have been added to the web platform to counter the risk of HTTPS stripping.
%The first is the \texttt{secure} attribute for HTTP cookies, which prevents designated cookies from being transmitted over insecure (HTTP) connections.
The primary countermeasure to HTTPS stripping is \emph{strict transport security} (HSTS)~\cite{rfc6797} through which browsers learn that specific domains must only be accessed via HTTPS.
This policy may be specified dynamically by sites using an HTTP header or preloaded by browsers for popular domains.
While HSTS is conceptually simple, there are subtle interactions with other browser security features, namely the same-origin policy and protections for HTTP cookies.
This leads to a number of deployment errors which enable an attacker to steal sensitive data without compromising HTTPS itself.
%HSTS provides a model for scalable, end-to-end distribution of security policy: browser preload HSTS for popular domains, follow HTTPS links to reach new domains securely, and then cache HSTS policies for frequently-visited domains when HSTS headers are observed.

Beyond HTTPS stripping, there are growing concerns about weaknesses in the certificate authority (CA) system.
The public discovery of commercial software to use \emph{compelled certificates}~\cite{soghoian12}, as well as high-profile compromises of several trusted CAs, have spurred interest in defending against a new threat model in which the attacker may obtain a \emph{rogue certificate} for a target domain signed by a trusted CA.

While many protocols have been proposed to maintain security against an attacker with a rogue certificate for a target domain signed by a trusted CA~\cite{clark13}, the only defense deployed to date is \emph{public-key pinning} (or just \emph{key pinning}), by which a browser learns to only connect to specific domains over HTTPS if one of a designated set of keys is used.
This policy can be used to ``pin'' a domain to a whitelist of keys of which at least one must appear somewhere in the server's certificate chain.
This can be used to pin a domain to specific end-entity keys, certificate authorities, or a mix of both.
Key pinning is currently deployed only as a browser-preloaded policy with Chrome and Firefox, although support is planned for dynamically declared header-based pins~\cite{evans12}.

While both technologies are still in the early stages, there is significant enough deployment to draw some meaningful lessons about their use in practice.
In this work, we report on the first comprehensive survey of HSTS and key pinning.
We use OpenWPM~\cite{openwpm} to perform realistic crawling (see Section~\ref{sec:methodology}) of both the list of domains with preloaded security policies in Firefox and Chrome and the top million most highly-visited domains as provided by Alexa~\cite{alexa}, examining both sites' static code and dynamically generated traffic.
% , using OpenWPM~\cite{opemwpm} for realistic crawling (see Section~\ref{sec:methodology}) to examine both a sites' static code as well as to dynamically observe traffic initiated by each website as it is loaded.
%Our code will ultimately be released as an open-source tool for security scanning.

We catalog several common security bugs observed in practice (see Sections~\ref{sec:deployment}--\extendedversion{\ref{sec:links}}{\ref{sec:cookies}}).
A summary of these errors is provided in Table~\ref{tab:results_summary}.
To the best of our knowledge, we provide the first published evidence of these bugs' appearance in the wild.
Knowledge of these errors is useful for any administrator seeking to deploy HSTS and/or pinning securely.

In summarizing our findings (Section~\ref{sec:discussion}), we highlight several underlying causes of these errors.
In particular, we believe the specification of HSTS and pinning suffers from both insufficient flexibility and a lack of sensible defaults.
These lessons are timely given the considerable amount of ongoing research and development of new proposals for upgrading HTTPS security.

\input{resources/results_summary.tex}
