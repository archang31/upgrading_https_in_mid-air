import Queue
import threading
import socket
import struct
import cPickle

#TODO - Implement a cleaner shutdown for server socket
# see: https://stackoverflow.com/questions/1148062/python-socket-accept-blocks-prevents-app-from-quitting

class serversocket:
    """
    A server socket to recieve and process string messages
    from client sockets to a central queue
    """
    def __init__(self, verbose=False):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #0 picks a free port
        self.sock.bind(('localhost', 0))
        #self.sock.bind(('localhost', 53535))
        #increase # of threads
        self.sock.listen(50)  # queue a max of n connect requests
        self.verbose = verbose
        self.queue = Queue.Queue()
        if self.verbose:
            print "Server bound to: " + str(self.sock.getsockname())
    
    def start_accepting(self):
        """ Start the listener thread """
        thread = threading.Thread(target=self._accept, args=())
        thread.daemon = True  # stops from blocking shutdown
        thread.start()

    def _accept(self):
        """ Listen for connections and pass handling to a new thread """
        while True:
            (client, address) = self.sock.accept()
            thread = threading.Thread(target=self._handle_conn, args=(client, address))
            thread.daemon = True
            thread.start()
            
    def _handle_conn(self, client, address):
        """
        Recieve messages and pass to queue. Messages are prefixed with
        a 4-byte integer to specify the message length and 1-byte boolean
        to indicate pickling.
        """
        if self.verbose:
            print "Thread: " + str(threading.current_thread()) + " connected to: " + str(address)
        try:
            while True:
                msg = self.receive_msg(client, 5)
                #this if statement is all a hack to get my extension to talk to the crawl. I tried passing
                #the query as a string but could not get the database to except it. This interface will need
                #to be improved for proper extension support
                if msg == '*****':
                    msg = self.receive_msg(client, 5)
                    msglen = int(msg)
                    msg = self.receive_msg(client, msglen)
                    msg = msg.split('&~', 8)
                    #query = (("INSERT INTO Resource_Loads_Dynamic (crawl_url, window_url, content_type,\
                    #resource_url, request_origin, isInIframe, parser_results, \
                    #node_name) VALUES (?,?,?,?,?,?,?,?)",
                    #          (msg[0], msg[1], msg[2], msg[3], msg[4],
                    #          msg[5], msg[6], msg[7])))
                    query = (("INSERT INTO Resource_Loads_Dynamic (crawl_url, resource_url, content_type,"
                              "parser_results, isInIFrame, window_url, request_origin, node_name) "
                              "VALUES (?,?,?,?,?,?,?,?)",
                              (msg[0], msg[1], msg[2], msg[3],
                               msg[4], msg[5], msg[6], msg[7])))
                    self.queue.put(query)
                else:
                    msglen, is_pickled = struct.unpack('>I?', msg)
                    msg = self.receive_msg(client, msglen)
                    if is_pickled:
                        msg = cPickle.loads(msg)
                    self.queue.put(msg)
        except RuntimeError:
            if self.verbose:
                print "Client socket: " + str(address) + " closed"

    def receive_msg(self, client, msglen):
        msg = ''
        while len(msg) < msglen:
            chunk = client.recv(msglen-len(msg))
            if chunk == '':
                raise RuntimeError("socket connection broken")
            msg = msg + chunk
        return msg
    def close(self):
        self.sock.close()

class clientsocket:
    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    def connect(self, host, port):
        self.sock.connect((host, port))

    def send(self, msg):
        """
        Sends an arbitrary python object to the connected socket. Pickles if 
        not str, and prepends msg len (4-bytes) and pickle status (1-byte).
        """
        #if input not string, pickle
        if type(msg) is not str:
            msg = cPickle.dumps(msg)
            is_pickled = True
        else:
            is_pickled = False
        
        #prepend with message length
        msg = struct.pack('>I?', len(msg), is_pickled) + msg
        totalsent = 0
        while totalsent < len(msg):
            sent = self.sock.send(msg[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + sent
    
    def close(self):
        self.sock.close()

if __name__ == '__main__':
    import sys

    #Just for testing
    if sys.argv[1] == 's':
        sock = serversocket(verbose=True)
        sock.start_accepting()
    elif sys.argv[1] == 'c':
        sock = clientsocket()
