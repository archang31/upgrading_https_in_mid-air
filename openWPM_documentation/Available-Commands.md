There are a number of commands that can be sent to the TaskManager.

## get

Using `manager.get(URL)` will load `URL` in a tab. The cookies, flash_cookies, http_request, http_response, and localStorage tables will be modified in the database.

## browse

Using `manager.browse(URL, num_links)` will load `URL` in a tab as `manager.get()` would. Once loaded, the browser will visit `num_links`, returning back to the homepage after each link. All links selected will be from the same hostname as `URL`. This may require a longer timeout, especially when used with [bot detection mitigation](https://github.com/citp/OpenWPM/wiki/Advanced-Features/). 

**NOTE**: All records in the database will have `top_url` set to `URL`, even for resources requested during visits to sub-links on the page. For example, some `http requests` may be labeled as coming from `http://www.yahoo.com` when they are actually from `http://www.yahoo.com/news/` after a homepage link was clicked.

## dump_storage_vectors
Calling `manager.dump_storage_vectors(URL, start_time = X)` will read the supported local storage locations (currently the browser's cookies and flash storage) and save any changes to the state following time `X`. The following example is a typical usage of this function:
```python
strt_tm = time.time()
manager.get(URL)
manager.dump_storage_vectors(URL, strt_tm)
```

## dump_profile
Calling `manager.dump_profile(dump_folder, closer_webdriver=False)` saves the browsers local state as a gzipped tar into `dump_folder`. If `close_webdriver` is set to `True`, the manager will close the webdriver so all browser data syncs to disk before copying. You will likely want to use with command with `close_webdriver = True` when saving that state at the end of a crawl.

## extract_links

Calling `manager.extract_links` will extract the links from `<a>` tags on the current page and save them to the `links_found` table. This can be used to find all the links on a page navigated to using [get](#get).  

The`links_found` table has two columns, `found_on` and `location`. `location` is a link that was found on the page stored in the `found_on` column. The`links_found` table will only exist if `extract_links` was called.