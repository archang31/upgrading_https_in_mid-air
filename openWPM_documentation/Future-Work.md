[Go To Wiki Home](https://github.com/citp/OpenWPM/wiki)

## Overview

There are several features that are currently unsupported, but that we see as the highest priority additions.

## Short Term Goals

* Expanding the socket communication interface to enable a fully distributed infrastructure
* Leveraging tools such as iMacro to enable better login coverage
* Add support for distinguishing between hard and soft commands fails (i.e. crashes vs. timeouts during more complex commands)
* Browser optimizations
* Enable the enumeration of plugins in addition to extensions

## Long Term Goals

### Additional Browser Support
One of the main reasons for choosing selenium as our automation tool is the fact that it supports a multitude of browsers. However, much of the necessary code ends up browser specific (loading and saving browser profiles, changing a browser's proxy settings, using shortcuts to close and open a new tab, and so on). We would like to expand to PhantomJS and Chrome/Chromium.

### Distributed Infrastructure
In addition to socket based communication between all components of the system on one machine, scaling the infrastructure to enable deployment on remote machines is ideal. This would involve the development of additional components to serve as listeners/managers on remote machines.

### Logging
The infrastructure is in dire need of proper error logging. Many errors are printed to screen during a crawl, which can make uncommon bugs difficult to track down. All errors, commands, and settings should be logged on a per-browser basis.

### Machine learning for button detection, clicking and form filling
Regular expression based parsing has limited success and requires a lot of development time, debugging time, and upkeep. A simple classifier for detecting login buttons, forms, etc could be very useful and could make use of the simpler methods we currently have for training data.

### Action chains
Running sequences of command synchronously with many browsers may be slow due to the fact that all browsers must be finished with a single command before beginning a new one. Furthermore, end users may only care that sequences of commands are executed in synchronization, considering synchronization of individual commands to be a lesser concern. One solution to this problem would be support for action chains in which an end user can pass the platform sequences of commands to be treated as a single command for the purposes of synchronization.