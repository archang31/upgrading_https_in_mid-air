[Go To Wiki Home](https://github.com/citp/OpenWPM/wiki)

## Overview

Contained in `automation/BrowserManager.py`, Browser Managers provide a wrapper around the drivers used to automate full browser instances. In particular, we opted to use [Selenium](http://docs.seleniumhq.org/) to drive full browser instances as bot detection frameworks can more easily detect lightweight alternatives such as PhantomJS. 

Browser Managers receive commands from the Task Manager, which they then pass to the command executor (located in `automation/Commands/command_executor.py`), which receives a command tuple and convert it into web driver actions. Browser Managers also receive browser parameters which they use to instantiate the Selenium web driver using one of the browser initialization functions contained in `automation/DeployBrowsers`. 

The Browser class, contained in the same file, is the Task Manager's wrapper around Browser Managers, which allow it to cleanly kill and restart Browser Managers as necessary.

**Important Programming Note** The Browser Managers are designed to isolate the Task Manager from the underlying browser instances. As part of this approach, no data from the browsers should flow up to the Task Manager (beyond basic metadata such as the browsers' process IDs). For instance, if the Browser Manager is assigned the task of collecting and parsing XPath data, this parsing should be completed by Browser Managers and **not** passed up to the Task Manager for post-processing.

## Browser Information Logging

Throughout the course of a measurement, the Browser Managers' commands (along with timestamps and the status of the commands) are logged by the Task Manager, which contributes the the reproducibility of individual experiments. Depending on whether the `proxy` flag is enabled in the Task Manager, the Browser Managers also instantiate HTTP proxies, specifically [mitmproxy](http://mitproxy.org) instances, which records traffic-related data during the crawl. The data are sent to the Data Aggregator process, which provides stability in logging data despite the possibility of individual browser crashes.

This third and final module of the platform will be described in the next section:

[Data Aggregator](https://github.com/citp/OpenWPM/wiki/Data-Aggregator)