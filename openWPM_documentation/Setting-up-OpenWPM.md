[Go To Wiki Home](https://github.com/citp/OpenWPM/wiki)

## Virtual Machines

OpenWPM is developed and deployed almost exclusively on Ubuntu machines. As such, we provide two Ubuntu 14.04 64-bit VM options in addition to instructions for a manual install on your personal machine. Both VMs are version 0.1.1 of OpenWPM, so we recommend running `git pull origin master` to get a newer version if necessary.

### Amazon EC2 AMI
Image: [Public ami-2862a640](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#LaunchInstanceWizard:ami=ami-2862a640)

We recommend running on an m3.medium EC2 instance. Note that you will need an EC2 account and will be charged while using the VM if you decide to go this route. Be sure to terminate your instance of the VM when finished.

### VirtualBox 
Image: [OpenWPM-0.1.1.ova](http://www.cs.princeton.edu/~ste/openwpm/OpenWPM-0.1.1.ova)

Instructions for import are available [here](http://www.virtualbox.org/manual/ch01.html#ovf)

## Manual Installation and Dependencies

Currently, OpenWPM has primarily been written and tested on Ubuntu machines. The following sets of commands should enable it to run on a clean install of Ubuntu.

`sudo apt-get update`

`sudo apt-get install htop git firefox python-dev python-pip libxml2-dev libxslt-dev libffi-dev libssl-dev build-essential xvfb`

`sudo pip install pyvirtualdisplay beautifulsoup4 selenium netlib pyasn1 PyOPenSSL mitmproxy python-dateutil tld pyamf`

`git clone https://github.com/citp/OpenWPM/`

If you need Flash on Ubuntu 14.04 64-bit Amazon EC2 instances, run the following commands:

`sudo sh -c 'echo "deb http://archive.canonical.com/ubuntu/ trusty partner" >> /etc/apt/sources.list.d/canonical_partner.list'`

`sudo apt-get update`

`sudo apt-get install adobe-flashplugin`

## Running a Simple Measurement

The most basic application of the infrastructure is to visit some set of sites and save data, such as HTTP traffic and the cookies placed. The platform will output this information into a SQLite database.

After downloading the OpenWPM code, try running the following command in the main folder.

`python run_simple_crawl.py <db_location> <site_list>`

where `<db_location>` is the absolute path of the crawl database (which may not yet exist) and `<site_list>` is the absolute location of a file containing a list of sites, with one site on each line.

Additional platform option flags are contained within `run_simple_crawl.py` and will be displayed when running this program without any arguments. Essentially, this crawl file instantiates an instance of the platform and calls its page visiting function. More details of this high-level infrastructure are contained in the next section:

[Task Manager](https://github.com/citp/OpenWPM/wiki/Task-Manager)

All set up and ready to go? See a more in-depth demo here:

[Platform Demo](https://github.com/citp/OpenWPM/wiki/Platform-Demo)