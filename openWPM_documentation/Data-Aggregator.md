[Go To Wiki Home](https://github.com/citp/OpenWPM/wiki)

## Overview

The Data Aggregator, contained in `automation/DataAggregator/DataAggregator.py`, is a separate process of the platform that receives data from the proxies and other instrumentation and logs the data into a SQLite database. The most useful feature of the Data Aggregator is the fact that it is isolated from the other processes through a network socket interface (see `automation/SocketInterface.py`). 

So far the platform logs a wide variety of information into a single database. We hope that these databases will be expanded and standardized so different teams of researchers can run each other's scripts on their own datasets as a means of verifying results.

## Data Logged

The full schema for the platform's output is contained in `automation/schema.sql`. On a high-level, the current information logged (assuming turning on the proxy and enabling Flash) is as follows:

* **crawl metadata**: browser options, start time, competition time
* **HTTP requests**: referrer, headers, method, timestamp, top URL (i.e. currently-visited site)
* **HTTP responses**: referrer, headers, method, status, location (for redirects), timestamp, top URL
* **HTTP cookies (both through Proxy and cookie DB scan)**: domain, name, value, expiry, accessed time
* **Adobe Flash cookies**: page URL, domain, file name, key, content
* **localStorage**: scope, value
* **crawl history**: commands, command arguments, success statuses, timestamps

We have now covered the high-level view of the WPM platform. The next section will cover some of the more advanced features we incorporated based on the work of other researchers:

[Advanced Features](https://github.com/citp/OpenWPM/wiki/Advanced-Features)