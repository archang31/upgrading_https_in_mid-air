[Go To Wiki Home](https://github.com/citp/OpenWPM/wiki)

## Overview

In this section, we present three basic development demos for working on the OpenWPM platform. In the first, we show how to run a basic crawl from scratch. In the second, we show how to add a new command to the platform. In the third, we provide a small example of data analysis that can be performed on the platform

## Running a simple crawl

The script below causes three browsers to crawl over a set of sites in lockstep. The two platform-specific tasks are instantiating the OpenWPM platform and ordering it to visit the given sites. 

````python
from automation import TaskManager
import tempfile
import time
import os
import copy
import json

# The list of sites that we wish to crawl
sites = ['http://www.example.com',
         'https://princeton.edu',
         'https://citp.princeton.edu/']

# Creates a temporary directory, where we will save the crawl DB
db_loc  = tempfile.mkdtemp() + '/openwpm_demo.sqlite'

preferences = TaskManager.load_default_params()


browser_params = [copy.deepcopy(preferences) for i in xrange(0, 3)]

# Instantiates the measurement platform
# Launches two (non-headless) Firefox instances which log data using mitmproxy
# Commands time out by default after 60 seconds
manager = TaskManager.TaskManager(db_loc, browser_params, 3)

# Visits the sites with both browsers simultaneously, 5 seconds between visits
for site in sites:
    manager.get(site, index='**') # ** = synchronized browsers
    time.sleep(5)

# Shuts down the browsers and waits for the data to finish logging
manager.close()
````

Generally, measurement crawls should be able to be run using scripts with lengths on the order of the one above. Even within this short script, there are several different options that a user can change. For instance, changing the line

```manager.get(site, index='**')```

will cause the browsers to be driven in a different way. For instance, setting ```index``` to ```None``` will cause the browsers to visit the site in a first-come, first-serve manner.

More importantly, users can modify the individual browser parameter dictionaries individually so that, for instance, certain browsers can run headless while others do not. An **important subtlety** to this instantiation is, as above, if the browser parameters dictionaries are copied from a single dictionary, these copies should be deep copies. Otherwise, changing an element in one dictionary will change the values in all the dictionaries (which are just pointers to the original dictionary) - a highly undesirable behavior.

We performed the deep copy in the code above to enable the end-user to easily practice modifying specific browser parameters independently. However, since the above example simply copies the same browser preference dictionary three times, the Task Manager constructor could instead be written as 

`manager = TaskManager.TaskManager(db_loc, preferences, 3)`

We also provide a simple method to load the deep-copied dictionaries without having to read the json file directly. Simply run the following line to return an array of deep-copied dictionaries.

`browser_params = TaskManager.load_default_params(num_browsers = 1)`

## Adding a new command

The OpenWPM platform already contains support for limited anti-bot detection through means such as randomly jiggling the mouse. However, suppose that we want to add a top-level command to cause a browser jiggle the mouse some number of times. The following demo illustrates how to add in a command assuming the Browser Manager contains a wrapper around Selenium instances.

First, in ```automation/TaskManager.py```, we need to add a new high-level command to the end of the file as follows

````python
def jiggle_mouse(self, num_jiggles, index=None, overwrite_timeout=None):
    self.distribute_command(('JIGGLE_MOUSE', num_jiggles), index, overwrite_timeout)
````

For any new command, we need to include the optional ```index``` argument to define which browsers receive the command as well as the ```overwrite_timeout``` command to allow users to enable a command to execute with a timeout different than the default input into the TaskManager constructor.

In this case, ```num_jiggles``` is the number of times we want the mouse to move. Within the command itself, we pass the Browser Managers command tuples in which the 0th argument is an alias for the command (in this case ```'JIGGLE_MOUSE'```) and the other arguments are the arguments for the command.

We next need to actually define the mouse jiggling command. For now, let's add it as another command in the ```automation/Commands/browser_commands.py``` file but, depending on the circumstances, we may want to add it to another module in practice. The necessary code is below:

````python
from selenium.webdriver.common.action_chains import ActionChains

def jiggle_mouse(webdriver, number_wiggles):
    for i in xrange(0, number_wiggles):
        x = random.randrange(0, 500)
        y = random.randrange(0, 500)
        action = ActionChains(webdriver)
        action.move_by_offset(x, y)
        action.perform()
````

In this case, ```webdriver``` is the Selenium webdriver contained within the Browser Manager. Finally, we need want to add in a hook for this function to be called. To do so, we add in a new branch condition in ```automation/Commands/command_executor.py``` within the ```execute_command``` function. In particular, this function takes in a tuple in which the first argument will be the alias of the higher-level command (in this case ```'JIGGLE_MOUSE'```). The new condition will simply call our newly-defined function as follows:

```python
if command[0] == 'JIGGLE_MOUSE':
    browser_commands.jiggle_mouse(webdriver, command[1])
````


## Running a simple analysis

Suppose that we ran the platform over some set of sites while logged into several sites while using a particular email. During the crawl, we turned on the proxy option to log HTTP traffic. One possible threat is, perhaps due to sloppy coding, the first-party leaks the user's email as plaintext over HTTP traffic. Given an OpenWPM database, the following script logs the first-party sites on which such a leakage occurs.

````python
import sqlite3 as lite

# connect to the output database
wpm_db = "<absolute_path_to_db>"
conn = lite.connect(wpm_db)
cur = conn.cursor()

# dummy user email and set of first-party sites on which email is leaked
user_email = "alice.bob@insecure.com"
fp_sites = set()

# scans through the database, checking for first parties on which the email is leaked
for url, top_url in cur.execute('SELECT DISTINCT url, top_url FROM http_requests'):
    if user_email in url and url.startswith("http:"):
        fp_sites.add(top_url)

# outputs the results
print list(fp_sites)
````

The variety of data stored in OpenWPM databases (assuming all logging settings are on) allows the above script to easily be expanded into a larger study. For instance, one step would be to see which parties are the recipients of the email address. Do these recipients later place cookies containing the email? Besides the site on which the original email leak was made, on which other first parties do these recipients appear as a third party? All of these questions are answerable through OpenWPM database instances.

Interested on collaborating on OpenWPM? See the next section outlining a development roadmap for the project 

[Future Work](https://github.com/citp/OpenWPM/wiki/Future-Work)