[Go To Wiki Home](https://github.com/citp/OpenWPM/wiki)

A few advanced features currently supported by the infrastructure are:

* Bot detection mitigation when visiting sites via Javascript emulation
 * After loading default preferences, set `preferences['bot_mitigation'] = True`
* Fingerprint mitigation through random browser settings generation
 * After loading default preferences, set `preferences['random_attributes'] = True`
* Extensions to the Selenium webdriver parsing engine 
 * For use in writing parsing code, accessible [here](https://github.com/citp/OpenWPM/blob/master/automation/Commands/utils/webdriver_extensions.py).
* Extensions to XPath 1.0 Searching and BeautifulSoup4 XPath Parsing
 * For use in writing parsing code, accessible [here](https://github.com/citp/OpenWPM/blob/master/automation/Commands/utils/XPathUtil.py).
* (coming soon) Preliminary automated site logins

Finish the overview of the WPM platform with the demo below:

[Platform Demo](https://github.com/citp/OpenWPM/wiki/Platform-Demo)