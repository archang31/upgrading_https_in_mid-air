#!/usr/bin/python
# -*- coding: UTF-8 -*-

import lists_helper
import re
from subprocess import Popen, PIPE
import hashlib
import base64
import sqlite3
import os
import sys
import copy
from M2Crypto import X509


base_folder = os.path.dirname(os.path.realpath(__file__))
resource_folder = os.path.join(base_folder, 'resources/')
top_million_header_path = os.path.abspath(os.path.join(resource_folder, 'top_million_headers.sqlite'))

certs_folder = "/etc/ssl/certs/"
def enumerate_ca_cert_files():
  return [os.path.join(certs_folder,f) for f in os.listdir(certs_folder) if os.path.isfile(os.path.join(certs_folder,f)) ]

def sha1hash(s):
    m = hashlib.sha1()
    m.update(s)
    return base64.b64encode(m.digest())

def sha256hash(s):
    m = hashlib.sha256()
    m.update(s)
    return base64.b64encode(m.digest())

def parse_cert(c, server_name = None):
  parsed_cert = X509.load_cert_string(c, X509.FORMAT_PEM)
  cert_dict = dict()
  cert_dict['url'] = server_name
  cert_dict['base_cert'] = c
  cert_dict['common_name'] = parsed_cert.get_subject().CN
  cert_dict['organization'] = parsed_cert.get_subject().O
  cert_dict['subject_name'] = parsed_cert.get_subject().as_text()
  cert_dict['issuer_name'] = parsed_cert.get_issuer().as_text()
  is_ca = False
  try:
      e = parsed_cert.get_ext("basicConstraints").get_value()
      if e.find("CA:TRUE") != -1:
          is_ca = True
  except LookupError:
      pass

  cert_dict['is_ca'] = is_ca
  cert_dict['SHA_1'] = sha1hash(parsed_cert.get_pubkey().as_der())
  cert_dict['SHA_256'] = sha256hash(parsed_cert.get_pubkey().as_der())
  return cert_dict

def parse_ca_certs():

  ca_certs = {}

  for f in enumerate_ca_cert_files(): 
    c = parse_cert(open(f, "r").read())
    if c['issuer_name']:
      ca_certs[c['issuer_name']] = c

  print len(ca_certs), "root certs loaded"

  return ca_certs

def fetch_certs(server_name, ca_certs = None):
    cmd = "openssl s_client -showcerts -connect %s:443" % server_name
    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    p.stdin.write("\n")
    s = p.communicate()[0]
    certs = re.findall("-----BEGIN CERTIFICATE-----[^-]+-----END CERTIFICATE-----", s)
    m_text = "%d certificates returned from %s" % (len(certs), server_name)
    print m_text
    print "-" * len(m_text)
    certs_list = list()
    for c in certs:
        certs_list.append(parse_cert(c, server_name))
  
    #Append extra certs from the root store to the chain
    if ca_certs is not None:
      subjects = set([c["subject_name"] for c in certs_list])
      while True:
        added = False
        for c in certs_list:
          if c["issuer_name"] in ca_certs and ca_certs[c["issuer_name"]]["subject_name"] not in subjects:
            added = True
            subjects.add(ca_certs[c["issuer_name"]]["subject_name"])
            temp_cert = copy.deepcopy(ca_certs[c["issuer_name"]])
            temp_cert['url'] = server_name
            certs_list.append(temp_cert)
        if not added:
          break
      
    return certs_list


def delete_old_and_create_new_table(conn):
    conn.executescript('DROP TABLE IF EXISTS Pinning_Certificates;')
    conn.execute('CREATE TABLE IF NOT EXISTS Pinning_Certificates ('
                 'url, base_cert, SHA_1, SHA_256, common_name, organization, is_ca, subject_name, issuer_name)')
    conn.commit()


def add_to_database(certs_list, conn, delete_old):
    if delete_old:
        delete_old_and_create_new_table(conn)
    cur = conn.cursor()
    for certs in certs_list:
        for i in range(0, len(certs)):
            cert_dict = certs[i]
            cur.execute("INSERT INTO Pinning_Certificates (url, base_cert, SHA_1, SHA_256, common_name, "
                        "organization, is_ca, subject_name, issuer_name) VALUES (?,?,?,?,?,?,?,?,?)",
                        (cert_dict['url'], cert_dict['base_cert'], cert_dict['SHA_1'], cert_dict['SHA_256'],
                        cert_dict['common_name'], cert_dict['organization'], cert_dict['is_ca'],
                        cert_dict['subject_name'], cert_dict['issuer_name']))
    conn.commit()


def extract_preloaded_pinned_sites_certificates(ca_certs, delete_old=True):
    conn = sqlite3.connect(os.path.abspath(os.path.join(resource_folder, 'preloaded_pinned_headers.sqlite')))
    pinned_list = LH.pinned_list
    certs_list = list()
    for url in pinned_list:
        returned_list = fetch_certs(url, ca_certs)
        certs_list.append(returned_list)
    add_to_database(certs_list, conn, delete_old)


def extract_top_pinned_sites_certificates(ca_certs, delete_old=True):
    conn = sqlite3.connect(os.path.abspath(os.path.join(resource_folder, 'top_million_headers.sqlite')))
    c = conn.execute("SELECT url FROM HTTPS_Headers WHERE key_pin is not ?", (None,))
    top_pinned_list = c.fetchall()
    certs_list = list()
    for url in top_pinned_list:
        certs_list.append(fetch_certs(url[0], ca_certs))
    add_to_database(certs_list, conn, delete_old)


def extract_dynamic_pinned_sites_certificates_without_db(ca_certs, delete_old=True):
    conn = sqlite3.connect(os.path.abspath(os.path.join(resource_folder, 'dynamic_pinned_certificates.sqlite')))
    top_pinned_list = ['propublica.org', 'astrogyan.com', 'slo-tech.com', 'tmb.in', 'detectify.com', 'freenetproject.org',
                'usanpedro.edu.pe', 'steventress.com', 'www.propublica.org', 'www.astrogyan.com', 'www.slo-tech.com',
                'www.tmb.in', 'www.detectify.com', 'www.steventress.com', 'segu-info.com.ar', 'frederik-braun.com',
                'amigogeek.net', 'timtaubert.de']
    certs_list = list()
    for url in top_pinned_list:
        certs_list.append(fetch_certs(url, ca_certs))
    add_to_database(certs_list, conn, delete_old)


def convert_certs_to_hash():
    f = open(LH.certs_path, 'r')
    f2 = open(LH.certs_path + 'parsed', 'w')
    part_of_cert = False
    cert_string = ""
    name = ""
    for line in f:
        if part_of_cert:
            if line == '\n':
                parsed_cert = parse_cert(cert_string.strip())
                f2.write("sha1/" + parsed_cert['SHA_1'] + '\n')
                f2.write(name)
                f2.write("sha2/" + parsed_cert['SHA_256'] + '\n')
                part_of_cert = False
            else:
                cert_string += line
        else:
            if line[0] == '-':
                part_of_cert = True
                cert_string = line
            elif line != '\n':
                name = line
                f2.write(line)

#Debug mode
if len(sys.argv) > 1:
    hostname = sys.argv[1]
    index = 0
    ca_certs = parse_ca_certs()
    #print sorted(ca_certs.keys())
    for c in fetch_certs(hostname, ca_certs):
        index += 1
        print "Cert %d" %index
        for k, v in  c.items(): 
            if k == "base_cert": 
              continue
            print k, v
        print ""
    sys.exit(0)

if __name__ == "__main__":
    LH = lists_helper.ListHelper()
    LH.get_preloaded_list()
    #convert_certs_to_hash()
    ca_certs = parse_ca_certs()
    extract_top_pinned_sites_certificates(ca_certs)
    extract_preloaded_pinned_sites_certificates(ca_certs)
    extract_dynamic_pinned_sites_certificates_without_db(ca_certs)
