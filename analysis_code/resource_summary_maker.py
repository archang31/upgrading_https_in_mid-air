import sqlite3
from urlparse import urlparse
import lists_helper
import os
import operator
import pdb

LH = lists_helper.ListHelper()
public_suffix_list = {}

#File location stuff
database_path = '../crawl_db.sqlite'
results_folder = LH.results_folder
header_results = os.path.join(results_folder, 'header_results.csv')
sum_results = {}
latex_file = []
errors = {}
other_list = {}
dif_pin_twitter = 0
mixed_file = open("links.csv", 'w')

def content_active(content_type):
    if content_type == "SCRIPT" or content_type == "STYLESHEET " or content_type == "LINK" \
            or content_type == "XMLHTTPREQUEST" or content_type == "OBJECT" \
            or content_type == "SUBDOCUMENT" or content_type == 'WEBSOCKET' \
            or content_type == "FONT" or content_type == "DOCUMENT" or "stylesheet" in content_type.lower():
        return True
    else:
        return False


def content_passive(content_type):
    if content_type == 'MEDIA' or content_type == "IMAGE" or "icon" in content_type.lower() \
            or 'apple' in content_type.lower() or 'image' in content_type.lower():
        return True
    else:
        return False


def handle_type(type_dict, content_type, domain):
    #based on https://developer.mozilla.org/en-US/docs/Security/MixedContent
    if content_active(content_type):
        type_dict['mixed'] += 1
        type_dict['active'] += 1
        type_dict['active_domains'][domain] = True
        type_dict['mixed_domains'][domain] = True
        if content_type == "SUBDOCUMENT":
            type_dict['iframe'] += 1
            type_dict['iframe_domains'][domain] = True
    elif content_type == 'A TAG':
        type_dict['a tag'] += 1
        if type_dict['atag_domains'].get(domain, False):
            type_dict['atag_domains'][domain] += 1
        else:
            type_dict['atag_domains'][domain] = 1
    elif content_type == 'MEDIA' or content_type == "IMAGE" or "icon" in content_type.lower() \
            or 'apple' in content_type.lower() or 'image' in content_type.lower():
        type_dict['mixed'] += 1
        type_dict['mixed_domains'][domain] = True
        #This is PASSIVE: SHOULD be IMG, AUDIO, VIDEO, OBJECT
        type_dict['passive'] += 1
        type_dict['passive_domains'][domain] = True
    else:
        type_dict['other'] += 1
        if not other_list.get(content_type, False):
            other_list[content_type] = True


def initialize_type(new_dict):
    new_dict['mixed'] = 0
    new_dict['active'] = 0
    new_dict['passive'] = 0
    new_dict['a tag'] = 0
    new_dict['iframe'] = 0
    new_dict['other'] = 0
    new_dict['active_domains'] = {}
    new_dict['mixed_domains'] = {}
    new_dict['passive_domains'] = {}
    new_dict['atag_domains'] = {}
    new_dict['iframe_domains'] = {}


def set_example(parser_results, crawl_url, rowid, resource_url,
                content_type, IFrame, window_url, results_type, origin_url):
    if 'Redirect' in parser_results:
        temp_string = "Row: " + str(rowid) + ", Crawl: " + crawl_url.scheme + "://" + crawl_url.netloc + \
                      crawl_url.path + ", Window: " + window_url.scheme + "://" \
                      + window_url.netloc + ", Obj: " + content_type + ", IFRAME: " + IFrame + results_type
    elif IFrame == "True":
        temp_string = "Row: " + str(rowid) + ", Origin: " + origin_url.scheme + "://" + origin_url.netloc + \
                      origin_url.path + ", Resource: " + resource_url.scheme + "://" \
                      + resource_url.netloc + ", Obj: " + content_type + ", IFRAME: " + IFrame + results_type
    else:
        temp_string = "Row: " + str(rowid) + ", Window: " + window_url.scheme + "://" + window_url.netloc + \
                      window_url.path + ", Resource: " + resource_url.scheme + "://" \
                      + resource_url.netloc + ", Obj: " + content_type + ", IFRAME: " + IFrame + results_type
    return temp_string


def initialize_dict(parser_results, crawl_url, rowid, resource_url, content_type, IFrame,
                    window_url, results_type, origin_url, control_domain):
    new_dict = {}
    if results_type:
        initialize_type(new_dict)
    new_dict['count'] = 1
    new_dict['example'] = set_example(parser_results, crawl_url, rowid, resource_url,
                                      content_type, IFrame, window_url, results_type, origin_url)
    handle_type(new_dict, results_type, control_domain)
    return new_dict


def handle_row(parser_results, rowid, crawl_url, resource_url, content_type, isInIFrame, window_url, origin_url):
    results_type = content_type
    #key point here is that I only care about the control_domain_url. So I used the window URL if not an iframe,
    # and I used the original url if in an iframe. The "crawl_url" is simply the address the crawl set the browser to
    # but has no specific bearing on the actual site visited or resource accessed.
    if isInIFrame == 'True':
        IFrame = 'True'
        control_domain = LH.domain(origin_url.netloc)
        control_domain_url = origin_url
        
    else:
        IFrame = 'False'
        control_domain = LH.domain(window_url.netloc)
        control_domain_url = window_url
    #control_domain = LH.domain(crawl_url.netloc)
    google_test = True
    #google_test = 'google' not in crawl_url.netloc
    if google_test:
        if control_domain is None:
            print("control_domain is NONE")
            control_domain = crawl_url.netloc + ": TLD"
        if sum_results.get(parser_results, False):
            sum_results[parser_results]['count'] += 1
            handle_type(sum_results[parser_results], results_type, control_domain)
            if sum_results[parser_results].get(control_domain, False):
                sum_results[parser_results][control_domain]['count'] += 1
                handle_type(sum_results[parser_results][control_domain], results_type, control_domain)
                if parser_results == "HTTPSRefHSTSExternalViaHTTP":
                    if content_type == 'A TAG':
                        string = LH.domain(resource_url.netloc) + ", " + resource_url.netloc + ", " + control_domain
                        mixed_file.write(string)
                        mixed_file.write('\n')
                if content_active(content_type):
                    sum_results[parser_results][control_domain]['example'] = \
                        set_example(parser_results, crawl_url, rowid, resource_url,
                                    content_type, IFrame, window_url, results_type, origin_url)
                    #if parser_results == "PinnedRefDifferentPin":
                    #    if resource_url.netloc == "platform.twitter.com":
                    #        global dif_pin_twitter
                    #        dif_pin_twitter += 1
                    #print(sum_results[parser_results][control_domain]['example'])
                if sum_results[parser_results][control_domain].get(control_domain_url.netloc, False):
                    sum_results[parser_results][control_domain][control_domain_url.netloc]['count'] += 1
                    handle_type(sum_results[parser_results][control_domain][control_domain_url.netloc], 
                                results_type, control_domain)
                else:
                    sum_results[parser_results][control_domain][control_domain_url.netloc] = \
                        initialize_dict(parser_results, crawl_url, rowid, resource_url, content_type, IFrame, 
                                        window_url, results_type, origin_url, control_domain)
                    sum_results[parser_results][control_domain]['unique subdomains'] += 1

            else:
                sum_results[parser_results]['unique domains'] += 1
                sum_results[parser_results][control_domain] = \
                    initialize_dict(parser_results, crawl_url, rowid, resource_url, content_type, IFrame, 
                                    window_url, results_type, origin_url, control_domain)
                sum_results[parser_results][control_domain][control_domain_url.netloc] = \
                    initialize_dict(parser_results, crawl_url, rowid,resource_url, content_type, IFrame, 
                                    window_url, results_type, origin_url, control_domain)
                sum_results[parser_results][control_domain]['unique subdomains'] = 1
        else:
            sum_results[parser_results] = {}
            sum_results[parser_results]['count'] = 1
            sum_results[parser_results]['unique domains'] = 1
            initialize_type(sum_results[parser_results])
            sum_results[parser_results][control_domain] = \
                initialize_dict(parser_results, crawl_url, rowid,resource_url, content_type, IFrame, 
                                window_url, results_type, origin_url, control_domain)
            sum_results[parser_results][control_domain][control_domain_url.netloc] = \
                initialize_dict(parser_results, crawl_url, rowid, resource_url, content_type, IFrame, 
                                window_url, results_type, origin_url, control_domain)
            sum_results[parser_results][control_domain]['unique subdomains'] = 1


def write_results_to_file(file_name):
    f = open(file_name, 'w')
    for error, domain_list in sorted(sum_results.iteritems(), reverse=True):
        #this if avoids 'count', 'example' and other attributes
        if type(domain_list) is dict:
            output = error + ", Unique Base Domains:" + str(sum_results[error]['unique domains'])
            output += ", Count: " + str(sum_results[error]['count']) + '\n'
            if sum_results[error].get('active'):
                output += "Error by Type: Active " + str(sum_results[error]['active']) + ", IFRAME "
                output += str(sum_results[error]['iframe']) + ", A TAG " + str(
                    sum_results[error]['a tag']) + ", PASSIVE "
                output += str(sum_results[error]['passive']) + ", OTHER " + str(sum_results[error]['other']) + '\n'
            print(output),
            f.write(output)
            for domain, url_list in sorted(domain_list.iteritems()):
                if type(url_list) is dict:
                    output = "\t" + domain + ", Unique Subdomains = :" + \
                             str(sum_results[error][domain]['unique subdomains'])
                    output += ", Count: " + str(sum_results[error][domain]['count'])
                    output += ", Example {" + str(sum_results[error][domain]['example']) + "\n"
                    print(output),
                    print('\t\tSubs: '),
                    for item in url_list:
                        if item != "count" and item != "example" and item != "unique subdomains" and \
                                        item != "active" and item != "passive" and item != "other" and item != "iframe":
                            print(item),
                    print('\n'),
                    #f.write(output)
        print("\n"),
        f.write("\n")
    f.close()


def latex(command, command_text):
    if type(command_text) is int:
        output = "\\newcommand{\\" + str(command) + '}{' + str("{:,d}".format(command_text)) + '}\n'
    else:
        output = "\\newcommand{\\" + str(command) + '}{' + str(command_text) + '}\n'
    latex_file.write(output)


def comment(text):
    try:
        latex_file.write("%" + text + '\n')
    except UnicodeEncodeError:
        latex_file.write("%" + text.encode('utf-8') + '\n')


def percent(num1, num2):
    return format(((float(num1) / float(num2)) * 100), '.1f')


def make_tex_file():
    global latex_file
    latex_file = open('../publication/resources/crawl_data.tex', 'w')
    LH.get_preloaded_list()
    latex("Pinsets", LH.pinsets)
    latex("Entries", LH.entries)
    latex("BaseDomains", LH.base_domains)
    latex("PinnedEntries", LH.entries_pinned)
    latex("PinnedGoogleEntries", LH.entries_pinned_google)
    latex("PinnedGoogleNotHSTS", LH.entries_pinned_google_not_hsts)
    latex("PinnedNotGoogleEntries", LH.entries_pinned - LH.entries_pinned_google)
    latex("PinnedBaseDomains", LH.pinned_base_domains)
    latex("PinnedGoogleBaseDomains", LH.pinned_google_base_domains)
    latex("PinnedNotGoogleBaseDomains", LH.pinned_base_domains - LH.pinned_google_base_domains)
    latex("PinnedHSTSEntries", LH.entries_pinned_hsts)
    latex("PinnedHSTSBaseDomains", LH.pinned_hsts_base_domains)
    latex("HSTSEntries", LH.entries_hsts)
    latex("HSTSGoogleEntries", LH.entries_hsts_google)
    latex("HSTSNotGoogleEntries", LH.entries_hsts - LH.entries_hsts_google)
    latex("HSTSBaseDomains", LH.hsts_base_domains)
    if sum_results.get('PreloadedSelfRefWOSub', False):
        latex("PreloadedSelfRefWOSubMixedPercent",
              format((float(sum_results['PreloadedSelfRefWOSub']['mixed']) /
                      float(sum_results['PreloadedRefWOSub']['mixed']) * 100), '.2f'))
        latex("PreloadedSelfRefWOSubActivePercent",
              format((float(sum_results['PreloadedSelfRefWOSub']['active']) /
                      float(sum_results['PreloadedRefWOSub']['active']) * 100), '.2f'))
    if sum_results.get('PinnedRefWOSub', False):
        latex("PinnedRefWOSubPercent",
              format((float(sum_results['PinnedRefWOSub']['count']) /
                      float(sum_results['PinnedRefHTTPS']['count']) * 100), '.1f'))
        latex("PinnedRefWOSubMixedPercent",
              format((float(sum_results['PinnedSelfRefWOSub']['mixed']) /
                      float(sum_results['PinnedRefWOSub']['mixed']) * 100), '.2f'))
        latex("PinnedRefWOSubActivePercent",
              format((float(sum_results['PinnedSelfRefWOSub']['active']) /
                      float(sum_results['PinnedRefWOSub']['active']) * 100), '.2f'))
    latex("PreloadedRefPreloadedPercent", percent(sum_results['HTTPSRefHSTSViaHTTP']['a tag'],
                                                  sum_results['PreloadedRefPreloaded']['a tag']))
    if sum_results.get('PinnedRefHTTPS', False):
        latex("PinnedRefHTTPSPassivePercent", percent(len(sum_results['PinnedRefHTTPS']['passive_domains']),
                                                      LH.pinned_base_domains))
        latex("PinnedRefHTTPSActivePercent", percent(len(sum_results['PinnedRefHTTPS']['active_domains']),
                                                     LH.pinned_base_domains))
        latex("PinnedNotGoogleRefHTTPSPassivePercent",
              percent(len(sum_results['PinnedNotGoogleRefHTTPS']['passive_domains']),
                      LH.pinned_base_domains - LH.pinned_google_base_domains))
        latex("PinnedNotGoogleRefHTTPSActivePercent",
              percent(len(sum_results['PinnedNotGoogleRefHTTPS']['active_domains']),
                      LH.pinned_base_domains - LH.pinned_google_base_domains))

    if sum_results.get('HTTPSRefHSTSExternalViaHTTP', False):
        total_external = sum_results['HTTPSRefHSTSExternalViaHTTP']['a tag'] + \
                         sum_results['HTTPSRefHSTSExternal']['a tag']
        latex('HTTPSRefHSTSExternalDomainPercent',
              percent(len(sum_results['HTTPSRefHSTSExternalViaHTTP']['atag_domains']),
                      len(sum_results['HTTPSRefHSTSExternal']['atag_domains'])))
        latex('HTTPSRefHSTSExternalPercent',
              percent(sum_results['HTTPSRefHSTSExternalViaHTTP']['a tag'], total_external))
        total_atag = sum_results['HTTPSRefHSTSViaHTTP']['a tag'] + \
                     sum_results['HTTPSRefHSTS']['a tag']
        latex('HTTPSRefHSTSExternalTotal', total_external)
        latex('HTTPSRefHSTSPercent',
              percent(sum_results['HTTPSRefHSTSViaHTTP']['a tag'], total_atag))
        latex('HTTPSRefHSTSTotal', total_atag)
    latex("DifferentPinsActiveTwitter", dif_pin_twitter)
    latex_file.write('\n')
    for error, domain_list in sorted(sum_results.iteritems()):
        #this if avoids 'count', 'example' and other attributes
        if type(domain_list) is dict:
            #eb is error base
            eb = error
            #latex(eb + 'Text', error[3:])
            comment("*************************************************************************************************"
                    "*************************************************************************************************"
                    "*************************************************************************************************"
                    "*************************************************************************************************")
            latex(eb, sum_results[error]['count'])
            latex(eb + 'Domains', sum_results[error]['unique domains'])
            if error.startswith("Pinned") or error.startswith("Preloaded") \
                    or "Via" in error or "HTTPSRefHSTS" in error or error == "HTTPSSelfRefHSTS":
                latex(eb + "Active", sum_results[error]['active'])
                latex(eb + "ActiveDomains", len(sum_results[error]['active_domains']))
                comment(str(sorted(sum_results[error]['active_domains'])))
                latex(eb + "Mixed", sum_results[error]['mixed'])
                latex(eb + "MixedDomains", len(sum_results[error]['mixed_domains']))
                comment(str(sorted(sum_results[error]['mixed_domains'])))
                latex(eb + "Passive", sum_results[error]['passive'])
                latex(eb + "PassiveDomains", len(sum_results[error]['passive_domains']))
                comment(str(sorted(sum_results[error]['passive_domains'])))
                latex(eb + "IFrame", sum_results[error]['iframe'])
                latex(eb + "IFrameDomains", len(sum_results[error]['iframe_domains']))
                comment(str(sorted(sum_results[error]['iframe_domains'])))
                latex(eb + "Atag", sum_results[error]['a tag'])
                latex(eb + "AtagDomains", len(sum_results[error]['atag_domains']))
                latex(eb + "NotAtag", sum_results[error]['count'] - sum_results[error]['a tag'])
                #latex(eb+"Other", sum_results[error]['other'])
                for domain, url_list in sorted(domain_list.iteritems()):
                    if type(url_list) is dict and sum_results[error][domain].get('unique subdomains', False):
                        #    and sum_results[error]['unique domains'] < 40:
                        output = domain + ", Unique Subdomains = :" + \
                                 str(sum_results[error][domain]['unique subdomains'])
                        output += ", Count: " + str(sum_results[error][domain]['count'])
                        try:
                            output += ", Example {" + sum_results[error][domain]['example']
                        except UnicodeEncodeError:
                            output += ", Example {" + sum_results[error][domain]['example'].encode('utf-8')
                        comment(output)
                        subdomain_list = ""
                        for item in url_list:
                            if item != "count" and item != "example" and item != "unique subdomains" and \
                                            item != "active" and item != "passive" and item != "other" and \
                                            item != "iframe" and item != "a tag" and item != "atag_domains" and \
                                            item != "active_domains" and item != 'iframe_domains' and item != 'mixed' and \
                                            item != "mixed_domains" and item != "passive_domains":
                                subdomain_list += item + ", "
                        comment(subdomain_list)
        latex_file.write('\n')
    latex_file.close()


def make_http_links_figure():
    error_domains = sum_results['HTTPSRefHSTSViaHTTP']['atag_domains']
    print(error_domains)
    sorted_error = sorted(error_domains.items(), key=operator.itemgetter(1), reverse=True)
    for domain in sorted_error:
        print(domain[0], domain[1])
    print('\n\n\n')
    error_domains = sum_results['HTTPSRefHSTSExternalViaHTTP']['atag_domains']
    print(error_domains)
    sorted_error = sorted(error_domains.items(), key=operator.itemgetter(1), reverse=True)
    for domain in sorted_error:
        print(domain[0], domain[1])


def handle_errors():
    for error_name, error_dict in sorted(errors.iteritems()):
        #error_dict has list and 'text' which is the
        for row in error_dict['list']:
            #row[0] = rowId, row[1] = crawl_url, row[2] = resource url, row[3] = content_type (Script, image,etc.)
            #row[4] = iFrame ('True' if in IFrame), row[5] = window url (whats in the tab), row[6] = origin
            if row[2]:
                #handle_row(parser_results, rowid, crawl_url, resource_url, content_type, isInIFrame, window_url)
                handle_row(error_name, row[7][0] + str(row[0]), urlparse(row[1]),
                           urlparse(row[2]), row[3], row[4], urlparse(row[5]), urlparse(row[6]))


def make_summary():
    #First pull all the Result Categories by type from the database
    conn = sqlite3.connect(database_path)
    c = conn.execute('SELECT name FROM HSTS_Pinning_Results_Categories')
    category_data = c.fetchall()
    for category in category_data:
        #Now handle a since category from our list and pull all the entries for that result
        print(category[0])
        c2 = conn.execute("SELECT error_type, original_row, crawl_url, resource_url, content_type, "
                          "isInIFrame, window_url, request_origin FROM HSTS_Pinning_Results_List "
                          "WHERE error_type = ?", (category[0],))
        category_results_list = c2.fetchall()

        for e in category_results_list:
            #handle_row(parser_results, rowid, crawl_url, resource_url, content_type, isInIFrame, window_url, origin_url):
            handle_row(e[0], e[1], urlparse(e[2]), urlparse(e[3]), e[4], e[5], urlparse(e[6]), urlparse(e[7]))

def make_summary():
    #First pull all the Result Categories by type from the database
    conn = sqlite3.connect(database_path)
    c = conn.execute('SELECT name FROM HSTS_Pinning_Results_Categories')
    category_data = c.fetchall()
    for category in category_data:
        #Now handle a since category from our list and pull all the entries for that result
        print(category[0])
        c2 = conn.execute("SELECT error_type, original_row, crawl_url, resource_url, content_type, "
                          "isInIFrame, window_url, request_origin FROM HSTS_Pinning_Results_List "
                          "WHERE error_type = ?", (category[0],))
        category_results_list = c2.fetchall()

        for e in category_results_list:
            #handle_row(parser_results, rowid, crawl_url, resource_url, content_type, isInIFrame, window_url, origin_url):
            handle_row(e[0], e[1], urlparse(e[2]), urlparse(e[3]), e[4], e[5], urlparse(e[6]), urlparse(e[7]))


def output_mixed_results():
    #First pull all the Result Categories by type from the database
    conn = sqlite3.connect(database_path)
    print("getting results")
    c2 = conn.execute("SELECT error_type, original_row, crawl_url, resource_url, content_type, "
                      "isInIFrame, window_url, request_origin FROM HSTS_Pinning_Results_List "
                      "WHERE error_type = ?", ('PinnedRefHTTPS',))
    category_results_list = c2.fetchall()
    print("Done getting results")
    passive_type_list = dict()
    active_type_list = dict()
    passive_count = 0
    active_count = 0
    for e in category_results_list:
        content_type = e[4]
        if content_active(content_type):
            if active_type_list.get(content_type):
                active_type_list[content_type] += 1
            else:
                active_type_list[content_type] = 1
            active_count += 1
        elif content_passive(content_type):
            if passive_type_list.get(content_type):
                passive_type_list[content_type] += 1
            else:
                passive_type_list[content_type] = 1
            passive_count += 1
    print(passive_type_list)
    print(passive_count)
    print('\n')
    print(active_type_list)
    print(active_count)
    print('\n')

if __name__ == "__main__":
    make_summary()
    #output_mixed_results()
    make_tex_file()
    #make_http_links_figure()
    #pdb.set_trace()
