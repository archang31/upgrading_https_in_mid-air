import Cookie
import lists_helper
import sqlite3
import re


def initialize_cookie():
    c = dict()
    c['path'] = ''
    c['domain'] = ''
    c['expires'] = ''
    c['secure'] = False
    c['httponly'] = False
    return c


def my_cookie_parser(cookie_string):
    cookie = cookie_string.replace(",", ";").split(';')
    cookie_list = dict()
    cookie_dict = initialize_cookie()
    for cookie_attribute in cookie:
        if '=' in cookie_attribute:
            key = cookie_attribute.split('=', 1)[0].lower().replace(" ", "")
            value = cookie_attribute.split('=', 1)[1]
            if key == 'expires' or key == 'domain' or key == 'version' or key == 'comment' \
                    or key == "max-age" or key == 'path':
                cookie_dict[key] = value.replace(" ", "")
            else:
                if cookie_dict.get('name'):
                    #must be an old cookie. Append to cookie_list and create new cookie_dict
                    cookie_list[cookie_dict['name']] = cookie_dict
                    cookie_dict = initialize_cookie()
                cookie_dict['name'] = cookie_attribute.split('=', 1)[0].strip()
                cookie_dict['value'] = value
        else:
            if cookie_attribute.lower() == 'httponly' or cookie_attribute.lower() == 'secure':
                cookie_dict[cookie_attribute.lower()] = True
    if cookie_dict.get('name'):
        cookie_list[cookie_dict['name']] = cookie_dict
    return cookie_list


def add_cookies_to_db(cookie_list, db_path):
    conn = sqlite3.connect(db_path)
    conn.executescript('DROP TABLE IF EXISTS cookies;')
    conn.execute('CREATE TABLE IF NOT EXISTS cookies ('
                 'top_url, name, value, isSecure, isHttpOnly, referrer, domain, expiry)')
    cur = conn.cursor()
    for cd in cookie_list:
        cur.execute("INSERT INTO cookies (top_url, name, value, isSecure, isHttpOnly, referrer, domain, expiry) "
                    "VALUES (?,?,?,?,?,?,?,?)", (cd['top_url'], cd['name'], cd['value'], cd['secure'],
                                                 cd['httponly'], cd['path'], cd['domain'], cd['expires']))
    conn.commit()
    conn.close()


def parse_cookie_from_header(header_list):
    # this takes a HTTP header that is parsed into a dictionary
    # and the parses just the cookies (the 'set-cookie' headers)
    cookie_list = list()
    for url, headers in header_list.iteritems():
        #if they ever set a cookie
        if headers['set-cookie']:
            try:
                c = Cookie.BaseCookie(headers['set-cookie'].encode('UTF-8'))
                #so the above line should handle everything, but it frusteratingly does not. this cookie
                #parser can not handle httponly or secure so I had to do my own which mostly works
                my_cookie_dict = my_cookie_parser(headers['set-cookie'])
                #update to cookie expires time if present because my cookie expires time always messed up
                for cookie_name, morsel in c.iteritems():
                    if my_cookie_dict.get(cookie_name) and morsel.get('expires'):
                        my_cookie_dict['expires'] = morsel['expires']
                #now convert to list and append url as where it came from
                for cookie_name, cookie_dict in my_cookie_dict.iteritems():
                    cookie_dict['top_url'] = url
                    cookie_list.append(cookie_dict)
            except:
                #These are some weird formatted cookies
                continue

    return cookie_list

LH = lists_helper.ListHelper()
print(LH.top_million_header_path)
master_cookie_list = parse_cookie_from_header(LH.get_top_million_https_headers())
add_cookies_to_db(master_cookie_list, LH.top_million_header_path)