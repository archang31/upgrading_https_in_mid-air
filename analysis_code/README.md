HSTS Readme:
---------------------------------------

These are all files to assist with the HSTS testing. The main part of this folder is various python files use to parse or do pull additional data to assist in our testing of HSTS and pinning.


hsts_resource_parser.py
--------------------------------------- 
This file parses the resources collected from a previous OpenWPM crawl (static resources, dynamic resources or both). This file assumes there is a database.sqlite file already generated from this crawl. This file has two outputs as new tables in the input database:

   HSTS_Pinning_Results_Categories - these are the general "errors" that are identified by the crawl(name/count
		
   HSTS_Pinning_Results_List - this a list of every result (category). Each individual resource from the database might result in several entries in this list for the various errors is composes. Some results are also inclusive (so HSTSRefHTTP might also have a second entry HSTSSelfRefHTTP as a more specific error).

lists_helper.py
--------------------------------------- 
This is a helper class that handles the conversation of various input lists (the preloaded hsts.json, public suffix list, https everywhere list, top 500, top 10000, etc. Also has various helpers now to pull lists from the crawl.sqlite database.


hsts_summary_maker.py
--------------------------------------- 
This file takes that output of the previous library as an input and generates a summary of errors. This error summary is currently outputted in latex form to publication\resources\crawl_data.tex

alexa_rank_helper.py
--------------------------------------- 
This file looks up all the sites in the preloaded list in Alexa to determine their global site rank. All outputs are in /resources. alexa_raw.txt is the raw data, alexa_base_domains.csv and alexa_base_wo_google.csv are what they say.

chrome_list_over_time.py
---------------------------------------
looks at the changes in the preloaded list over time. Actually downloads every version of the list and saves in resources/sts .  