import lists_helper
from urlparse import urlparse
import sqlite3
import os
import copy

top = dict()
preloaded = dict()

LH = lists_helper.ListHelper()
preloaded_sites_list = LH.get_preloaded_list()
hsts_list = LH.hsts_list

latex_file = open('../publication/resources/header_data.tex', 'w')
base_folder = os.path.dirname(os.path.realpath(__file__))
resource_folder = os.path.join(base_folder, 'resources/')


def is_redirect(status_code):
    if status_code:
        if status_code == 301 or status_code == 302 or status_code == 303 or status_code == 307:
            return True
    return False


def add_error(error, url, pdict, bad_error=True):
    #print(error, url)
    #if it does not exist, add to error list
    if not pdict['errors'].get(error, False):
        pdict['errors'][error] = {}
    #append the url
    pdict['errors'][error][url] = True
    #set no_error value to false now
    #if pdict['parsed_sts'].get(url, False):
    if bad_error:
        pdict['parsed_sts'][url]['no_error'] = False

def set_count_no_error(pdict):
    pdict['sts_no_error'] = 0
    pdict['preload_token'] = 0
    for url, sts_dict in pdict['parsed_sts'].iteritems():
        if sts_dict['no_error']:
            pdict['sts_no_error'] += 1
        if sts_dict['sts'].get('preload') and sts_dict['sts']['preload']:
            pdict['preload_token'] += 1

def latex_sts_errors(pdict):
    pre = pdict['string']
    latex(pre + 'Domains', len(pdict['https_raw']))
    latex(pre + 'STSAttempted', len(pdict['parsed_sts']))
    latex(pre + 'STSAttemptedPercent', percent(len(pdict['parsed_sts']), len(pdict['https_raw'])))
    latex(pre + 'STSValid', pdict['sts_no_error'])
    latex(pre + 'STSValidPercent', percent(pdict['sts_no_error'], len(pdict['parsed_sts'])))
    not_valid = len(pdict['parsed_sts']) - pdict['sts_no_error']
    latex(pre + 'STSNotValid', not_valid)
    latex(pre + 'STSNotValidPercent', percent(not_valid, len(pdict['parsed_sts'])))
    latex(pre + 'KEYPINAttempted', len(pdict['parsed_key_pins']))
    total_without_valid_sts = len(pdict['https_raw']) - pdict['sts_no_error']
    latex(pre + 'WithoutSTS', total_without_valid_sts)
    latex(pre + 'WithoutSTSPercent', percent(total_without_valid_sts, len(pdict['https_raw'])))
    latex(pre + 'STSPreload', pdict['preload_token'])
    comment("")
    not_preloaded_sets_preloaded_invalid = 0
    for error, url_list in sorted(pdict['errors'].iteritems()):
        if 'KEYPIN' in error:
            base = len(pdict['parsed_key_pins'])
        else:
            base = len(pdict['parsed_sts'])
        latex(pre + error, len(pdict['errors'][error]))
        if error == 'NoSTSHeaderGoogle':
            latex(pre + error + "Percent", percent(len(pdict['errors'][error]), LH.entries_hsts_google))
        elif error == 'RedirectsToHTTPSWithoutSTS':
            latex(pre + error + "Percent", percent(len(pdict['errors'][error]), len(pdict['https_raw'])))
        elif error == 'STSNotPreloadedSetsPreload':
            latex(pre + error + "Percent", percent(len(pdict['errors'][error]), base))
            not_preloaded_sets_preloaded_invalid = 0
            for url in url_list:
                if not pdict['parsed_sts'][url]['no_error']:
                    not_preloaded_sets_preloaded_invalid += 1
            latex(pre + error + "NotValid", not_preloaded_sets_preloaded_invalid)
            latex(pre + error + "NotValidPercent",
                  percent(not_preloaded_sets_preloaded_invalid, len(pdict['errors'][error])))
        else:
            latex(pre + error + "Percent", percent(len(pdict['errors'][error]), base))
        if len(pdict['errors'][error]) < 150:
            #list subdomains
            base_domains_list = "Domains: "
            for url, value in sorted(url_list.iteritems()):
                base_domains_list += url + ", "
            comment(base_domains_list)
        comment('')


def print_sts_errors(pdict):
    for error, value in sorted(pdict['errors'].iteritems()):
        print(error, len(pdict['errors'][error]))
        #if error.startswith('STS'):
        #    for url, set_value in pdict['errors'][error].iteritems():
        #        print('\t'),
        #        print(url, pdict['https_raw'][url]['sts'])
        #if error.startswith('KEY-PIN'):
        #    for url, set_value in pdict['errors'][error].iteritems():
        #        print('\t'),
        #        print(url, pdict['https_raw'][url]['pin'])
    print("STS sites with no error:", pdict['sts_no_error'])
    print("STS total entrys:", len(pdict['parsed_sts']))


def output_max_age(pdict):
    sts_age_file = open('../publication/figures/max_age_' + pdict['string'].lower() + '_sts_data.csv', 'w')
    for url, sts_dict in pdict['parsed_sts'].iteritems():
        if sts_dict.get('sts'):
            if sts_dict['sts']['age'] is not None:
                sts_age_file.write(str(sts_dict['sts']['age']) + "\n")
    sts_age_file.close()
    keypin_age_file = open('../publication/figures/max_age_' + pdict['string'].lower() + '_keypin_data.csv', 'w')
    for url, key_dict in pdict['parsed_key_pins'].iteritems():
        if key_dict['age'] is not None:
            keypin_age_file.write(str(key_dict['age']) + "\n")
    keypin_age_file.close()


def latex(command, command_text):
    if type(command_text) is int:
        output = "\\newcommand{\\" + str(command) + '}{' + str("{:,d}".format(command_text)) + '}\n'
    else:
        output = "\\newcommand{\\" + str(command) + '}{' + str(command_text) + '}\n'
    latex_file.write(output)


def comment(text):
    latex_file.write("%" + text + '\n')


def percent(num1, num2):
    return format(((float(num1) / float(num2)) * 100), '.1f')


def test_preloaded_headers():
    #test if every preloaded sets sts
    for domain, list_value in hsts_list.iteritems():
        if not preloaded['parsed_sts'].get(domain):
            #Preloaded STS site does not set STS header (not including invalid addresses)
            add_error('NoSTSHeader', domain, preloaded, False)
            if list_value['pins'] == 'google':
                add_error('NoSTSHeaderGoogle', domain, preloaded, False)
            else:
                add_error('NoSTSHeaderNotGoogle', domain, preloaded, False)
        else:
            if list_value['pins'] == 'google':
                add_error('STSHeaderGoogle', domain, preloaded, False)
            else:
                add_error('STSHeaderNotGoogle', domain, preloaded, False)
        #Check if redirects to HTTP
        if preloaded['https_raw'][domain]['location'] is not None:
            parsed_location = urlparse(preloaded['https_raw'][domain]['location'])
            if parsed_location.scheme == 'http':
                add_error("RedirectsToHTTP", domain, preloaded, False)
                if list_value['pins'] == 'google':
                    add_error("RedirectsToHTTPGoogle", domain, preloaded, False)
    stale = len(preloaded['errors']['STSRedirectsOutOfSTS']) + len(preloaded['errors']['STSRedirectsToHTTP']) + \
            len(preloaded['errors']['DomainNotContentNotAccessible']) + len(preloaded['errors']['RedirectsToHTTP'])
    latex(preloaded['string'] + 'STSStale', stale)
    latex(preloaded['string'] + 'STSStalePercent', percent(stale, len(preloaded['https_raw'])))


def test_http_redirects(pdict):
    for url, sts_dict in pdict['parsed_sts'].iteritems():
        if sts_dict['sts']['age'] != 0 and sts_dict['sts']['age'] is not None:
            if pdict['http_raw'].get(url):
                if not is_redirect(pdict['http_raw'][url]['status_code']):
                    #HTTP site of dynamic STS site with valid max age does not redirect
                    add_error('STSDoesNotRedirect', url, pdict)
                    if pdict['http_raw'][url].get('sts'):
                        add_error('STSDoesNotRedirectButSetsSTS', url, pdict)
                else:
                    if pdict['http_raw'][url]['location'] is not None:
                        parsed_location = urlparse(pdict['http_raw'][url]['location'])
                        if parsed_location.netloc is not "" and parsed_location.scheme != "https":
                            #techinically redirects but not to a HTTPS site
                            add_error('STSDoesNotRedirect', url, pdict)
                            if pdict['http_raw'][url].get('sts'):
                                add_error('STSDoesNotRedirectButSetsSTS', url, pdict)
        if pdict['https_raw'][url]['location'] is not None:
                parsed_location = urlparse(pdict['https_raw'][url]['location'])
                searchable_url = parsed_location.netloc
                if searchable_url is not None and searchable_url is not "":
                    if parsed_location.scheme == 'http':
                        #add_error("STSRedirectsOutOfSTS", url, pdict)
                        add_error("STSRedirectsToHTTP", url, pdict)
                    #now check if just redirects to another HTTPS not STS site
                    elif not hsts_list.get(searchable_url) and not pdict['parsed_sts'].get(searchable_url):
                        if top['https_raw'].get(searchable_url) and not top['parsed_sts'].get(searchable_url):
                            add_error("STSRedirectsOutOfSTS", url, pdict)


def parse_header_value(header, url, pdict, is_sts):
    header_dict = dict()
    header_dict['subdomains'] = False
    if is_sts:
        header_dict['preload'] = False
        error_string = 'STS'
        sts_error = True
    else:
        header_dict['keypins'] = []
        header_dict['report-uri'] = []
        header_dict['no_error'] = True
        error_string = 'KEYPIN'
        sts_error = False
    split = header.strip().split(";")
    for i in range(0, len(split)):
        split_value = split[i].strip().lower()
        if split_value[0:8] == 'max-age=':
            if header_dict.get('age'):
            #an error if an age set before
                add_error(error_string + 'IncorrectlyFormatted', url, pdict, sts_error)
            else:
                age_value = split_value[8:]
                try:
                    header_dict['age'] = int(age_value)
                    if header_dict['age'] == 0:
                        add_error(error_string + 'MaxAgeZero', url, pdict, sts_error)
                        if pdict['https_raw'][url]['location']:
                            parsed_location = urlparse(pdict['https_raw'][url]['location'])
                            if parsed_location.scheme == 'http':
                                add_error(error_string + "MaxAgeZeroRedirectsToHTTP", url, pdict, False)
                    elif header_dict['age'] <= 86400:
                        add_error(error_string + 'MaxAgeADayOrLess', url, pdict, sts_error)
                        if not is_sts:
                            header_dict['no_error'] = False
                        if 60 >= header_dict['age'] > 0:
                            add_error(error_string + 'MaxAgeAMinute', url, pdict, sts_error)
                        if header_dict['age'] < 0:
                            add_error(error_string + 'MaxAgeNegative', url, pdict, sts_error)
                    elif header_dict['age'] == 31536000:
                        add_error(error_string + 'MaxAgeAYear', url, pdict, False)
                    elif header_dict['age'] > 31536000:
                        add_error(error_string + 'MaxAgeOverAYear', url, pdict, False)
                except ValueError:
                    add_error(error_string + 'IncorrectlyFormatted', url, pdict, sts_error)
                    if not is_sts:
                        header_dict['no_error'] = False
                    if not header_dict.get('age'):
                        header_dict['age'] = None
        elif split_value == 'includesubdomains':
            header_dict['subdomains'] = True
        elif is_sts:
            if split_value == 'preload':
                header_dict['preload'] = True
                if not preloaded_sites_list.get(url):
                    add_error(error_string + 'NotPreloadedSetsPreload', url, pdict, False)
            elif split_value != "":
                add_error(error_string + 'IncorrectlyFormatted', url, pdict, sts_error)
        else:
            extra_split = split_value.split("=", 1)
            if extra_split[0] == 'pin-sha256' or extra_split[0] == 'pin-sha1':
                header_dict['keypins'].append([extra_split[0], extra_split[1]])
            elif extra_split[0] == 'report-uri':
                header_dict['report-uri'].append([extra_split[0], extra_split[1]])
                add_error(error_string + 'ReportUri', url, pdict, False)
            elif split_value != "":
                add_error(error_string + 'IncorrectlyFormatted', url, pdict, sts_error)
                header_dict['no_error'] = False
            #no else - these just "" (empty strings)
    if header_dict.get('age', False) is False:
        header_dict['age'] = None
        add_error(error_string + 'IncorrectlyFormatted', url, pdict, sts_error)
        if not is_sts:
            header_dict['no_error'] = False
    return header_dict


def initialize_sts_entry(url, pdict, url_raw_header):
    pdict['parsed_sts'][url] = {}
    pdict['parsed_sts'][url]['no_error'] = True
    pdict['parsed_sts'][url]['location'] = url_raw_header['location']
    pdict['parsed_sts'][url]['status_code'] = url_raw_header['status_code']
    pdict['parsed_sts'][url]['sts'] = parse_header_value(url_raw_header['sts'], url, pdict, True)


def http_redirect_to_https_but_does_not_set_https_sts_header_test(pdict, url):
    # These are extra tests for sites that do not set STS header but could/should
    # These tests need the top x http headers as well
    if pdict['http_raw'].get(url) and is_redirect(pdict['http_raw'][url]['status_code']):
        if pdict['http_raw'][url]['location']:
            parsed_location = urlparse(pdict['http_raw'][url]['location'])
            if parsed_location.scheme == 'https':
                #HTTP site redirects to HTTPS but does not set STS
                add_error('RedirectsToHTTPSWithoutSTS', url, pdict, False)
                if LH.domain(url) in url:
                    #HTTP self redirects to HTTPS (same base) but does not set STS
                    add_error('SelfRedirectsToHTTPSWithoutSTS', url, pdict, False)


def output_combined_key_pins():
    combined_key_pins = copy.deepcopy(top['parsed_key_pins'])
    for url, key_dict in preloaded['parsed_key_pins'].iteritems():
        if not combined_key_pins.get(url):
            combined_key_pins[url] = key_dict
    latex('CombinedKEYPINAttempted', len(combined_key_pins))


def test_for_http_sts_and_key_pin_headers(pdict):
    for url, headers in sorted(pdict['http_raw'].iteritems()):
        if headers['sts']:
            if not pdict['parsed_sts'].get(url):
                #add to the parsed_sts list so included in total sts count for errors
                pdict['parsed_sts'][url] = dict()
                pdict['parsed_sts'][url]['sts'] = dict()
                pdict['parsed_sts'][url]['sts']['age'] = None
                #HTTP site sets sts header
                add_error('HTTPSetsSTS', url, pdict, False)
                add_error('HTTPSetsSTSOnly', url, pdict)
            else:
                add_error('HTTPSetsSTS', url, pdict, False)
        if headers['pin']:
            if not pdict['parsed_key_pins'].get(url):
                pdict['parsed_key_pins'][url] = dict()
                pdict['parsed_key_pins'][url]['age'] = None
                #HTTP site sets key_pin header
                add_error('HTTPSetsKEYPIN', url, pdict, False)


def handle_https_raw_headers(pdict):
    #pdict is the passed in dictionary, either top dictionary or preloaded dictionary
    for url, headers in pdict['https_raw'].iteritems():
        #if not accessible, ignore
        if pdict['https_raw'][url]['status_code'] is None \
                or pdict['https_raw'][url]['status_code'] >= 400:
            #Preloaded STS listed domain is not an accessible website
            add_error('DomainNotAccessible', url, pdict, False)
            if pdict['is_preloaded']:
                if not preloaded_sites_list[url]['pins']:
                    add_error('DomainNotContentNotAccessible', url, pdict, False)
        if headers['sts']:
            initialize_sts_entry(url, pdict, pdict['https_raw'][url])
            #test is redirects to HTTP
        if headers['pin']:
            #print(url, headers['pin'])
            pdict['parsed_key_pins'][url] = parse_header_value(headers['pin'], url, pdict, False)
        if headers['public-key-pins-report-only']:
            add_error('PublicKeyPinReportOnly', url, pdict, False)
        if not pdict['is_preloaded']:
            http_redirect_to_https_but_does_not_set_https_sts_header_test(pdict, url)
    print ("sts count is", len(pdict['parsed_sts']))


def get_key_pins(key_pin_path):
    #so each header database (top_million_headers and preloaded_pinned_headers) has all the certificate
    #information for their pinned sites. the issue is preloaded_pinned_headers is missing the information
    #for the dynamic sts sites. Since they are so few, I hardcode these into get_key_hashes.py and they
    #are stored in dynamic_pinned_certifcates
    key_pins = dict()
    conn = sqlite3.connect(key_pin_path)
    sql_statement = "SELECT url, base_cert, SHA_1, SHA_256, common_name, organization, is_ca FROM Pinning_Certificates"
    c = conn.execute(sql_statement)
    data = c.fetchall()
    for row in data:
        if not key_pins.get(row[0]):
            key_pins[row[0]] = list()
        cert_dict = dict()
        cert_dict['url'] = row[0]
        cert_dict['base_cert'] = row[1]
        cert_dict['pin-sha1'] = row[2]
        cert_dict['pin-sha256'] = row[3]
        cert_dict['common_name'] = row[4]
        cert_dict['organization'] = row[5]
        cert_dict['is_ca'] = row[6]
        key_pins[row[0]].append(cert_dict)
    return key_pins


def test_preloaded_key_pins():
    key_pin_path = os.path.abspath(os.path.join(resource_folder, 'preloaded_pinned_headers.sqlite'))
    key_pins = get_key_pins(key_pin_path)
    pinned_dict = LH.pinned_list
    hash_dict = LH.get_hash_dict()
    results = dict()
    cert_pairs = list()
    no_match = list()
    total_ca = 0
    total_end = 0
    for url, cert_dict_list in sorted(key_pins.iteritems()):
        ca_certs = 0
        end_certs = 0
        match = False
        for cert_dict in cert_dict_list:
            if hash_dict.get(cert_dict['pin-sha1']):
                match = True
                hash_dict[cert_dict['pin-sha1']]['match'] = True
                if cert_dict['is_ca']:
                    ca_certs += 1
                else:
                    comment("End entity key is:" + url)
                    end_certs += 1
                if not results.get(pinned_dict[url]['pins']):
                    results[pinned_dict[url]['pins']] = dict()
                if not results[pinned_dict[url]['pins']].get(hash_dict[cert_dict['pin-sha1']]['name']):
                    results[pinned_dict[url]['pins']][hash_dict[cert_dict['pin-sha1']]['name']] = 1
                else:
                    results[pinned_dict[url]['pins']][hash_dict[cert_dict['pin-sha1']]['name']] += 1
                if pinned_dict[url]['pins'] != 'google':
                    cert_pairs.append([url, hash_dict[cert_dict['pin-sha1']]])
                #print(pinned_dict[url]['pins'], hash_dict[cert_dict['pin-sha1']])
            elif hash_dict.get(cert_dict['pin-sha256']):
                print("SHA 256 hash present")
            #elif pinned_dict[url]['pins'] != 'google':
            #    print(url, cert_dict['pin-sha1'])
        if not match:
            no_match.append(url)
        total_ca += ca_certs
        total_end += end_certs
    total_match = total_ca + total_end
    latex(preloaded['string'] + 'CertsMatch', total_match)
    latex(preloaded['string'] + 'CertsCa', total_ca)
    latex(preloaded['string'] + 'CertsCaPercent', percent(total_ca, total_match))
    latex(preloaded['string'] + 'CertsEnd', total_end)
    latex(preloaded['string'] + 'CertsEndPercent', percent(total_end, total_match))
    certs_used = 0
    certs_not_used = 0
    for my_hash, value in hash_dict.iteritems():
        if value['match']:
            certs_used += 1
        else:
            certs_not_used += 1
    latex(preloaded['string'] + 'Certs', len(hash_dict))
    latex(preloaded['string'] + 'CertsUsed', certs_used)
    latex(preloaded['string'] + 'CertsUsedPercent', percent(certs_used, len(hash_dict)))
    latex(preloaded['string'] + 'CertsNotUsed', certs_not_used)
    latex(preloaded['string'] + 'CertsNotUsedPercent', percent(certs_not_used, len(hash_dict)))
    #print('Results are: ', results)
    #print('\n')
    #print('Sites without a match:', no_match)
    #print('\n')
    #print(cert_pairs)


def test_dynamic_key_pins(pdict):
    key_pin_path = os.path.abspath(os.path.join(resource_folder, 'dynamic_pinned_certificates.sqlite'))
    key_pins = get_key_pins(key_pin_path)
    total_ca = 0
    total_end = 0
    for url, pin_dict in pdict['parsed_key_pins'].iteritems():
        match = False
        ca_certs = 0
        end_certs = 0
        for hash_type, dynamic_pin_value in pin_dict['keypins']:
            for cert_dict in key_pins[url]:
                if cert_dict[hash_type].lower() in dynamic_pin_value:
                    match = True
                    if cert_dict['is_ca']:
                        ca_certs += 1
                    else:
                        end_certs += 1
                #print(cert_dict[hash_type].lower(), dynamic_pin_value, match)
        if match:
            add_error("KEYPINMatch", url, pdict, False)
        else:
            add_error("KEYPINNoMatch", url, pdict, False)
            pin_dict['no_error'] = False
        if pin_dict['no_error']:
            add_error("KEYPINValid", url, pdict, False)
        else:
            add_error("KEYPINNotValid", url, pdict, False)
        total_ca += ca_certs
        total_end += end_certs
    total_match = total_ca + total_end
    latex(pdict['string'] + 'DynamicCertsMatch', total_match)
    latex(pdict['string'] + 'DynamicCertsCa', total_ca)
    latex(pdict['string'] + 'DynamicCertsCaPercent', percent(total_ca, total_match))
    latex(pdict['string'] + 'DynamicCertsEnd', total_end)
    latex(pdict['string'] + 'DynamicCertsEndPercent', percent(total_end, total_match))


def create_header_dictionary(passed_dict, https_raw, http_raw, is_preloaded, pre_string):
    passed_dict['https_raw'] = https_raw
    passed_dict['http_raw'] = http_raw
    passed_dict['parsed_sts'] = {}
    passed_dict['parsed_key_pins'] = {}
    passed_dict['errors'] = {}
    passed_dict['is_preloaded'] = is_preloaded
    passed_dict['string'] = pre_string


def output_dynamic_sts_sites(pdict):
    f = open(os.path.join(resource_folder, pdict['string'].lower() + '_dynamic_hsts_wosub_sites.txt'), 'w')
    for url, value in sorted(pdict['parsed_sts'].iteritems()):
        if value['no_error']:
            if not value['sts']['subdomains']:
                f.write(url + '\n')
    f.close()


def execute_sts_test(pdict, https_list, http_list, is_preloaded, pre_string):
    #initialize dictionary to test top sites
    create_header_dictionary(pdict, https_list, http_list, is_preloaded, pre_string)
    print ("https total responses are ", len(pdict['https_raw']))
    print ("http total responses are ", len(pdict['http_raw']))
    handle_https_raw_headers(pdict)
    test_http_redirects(pdict)
    test_for_http_sts_and_key_pin_headers(pdict)
    print ("sts count after http is", len(pdict['parsed_sts']))
    if is_preloaded:
        test_preloaded_headers()
        test_preloaded_key_pins()
    test_dynamic_key_pins(pdict)
    set_count_no_error(pdict)
    #print_sts_errors(pdict)
    output_max_age(pdict)
    latex_sts_errors(pdict)
    output_dynamic_sts_sites(pdict)


execute_sts_test(top, LH.get_top_million_https_headers(), LH.get_top_million_http_headers(), False, "Top")
execute_sts_test(preloaded, LH.get_preloaded_https_headers(), LH.get_preloaded_http_headers(), True, "Preloaded")
output_combined_key_pins()
latex_file.close()