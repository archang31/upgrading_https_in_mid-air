import lists_helper
import sqlite3
import httplib
import os
import json
from httplib import HTTPResponse
from StringIO import StringIO

LH = lists_helper.ListHelper()
top_10000 = LH.get_top_10000()
preloaded_list = LH.get_preloaded_list()
#Do not actually want preloaded_list, just HSTS list but need run to initialize
hsts_list = LH.hsts_list
pinning_list = LH.pinned_list

#Lists folder location
base_folder = os.path.dirname(os.path.realpath(__file__))
resource_folder = os.path.join(base_folder, 'resources/')
top10000_header_path = os.path.abspath(os.path.join(resource_folder, 'top_10000_headers.sqlite'))
hsts_header_path = os.path.abspath(os.path.join(resource_folder, 'preloaded_hsts_headers.sqlite'))
pinned_header_path = os.path.abspath(os.path.join(resource_folder, 'preloaded_pinned_headers.sqlite'))
top_million_header_path = os.path.abspath(os.path.join(resource_folder, 'top_million_headers.sqlite'))


def get_sts_headers_from_list(site_list, output_path, use_https=None, add_www=None):
    conn = sqlite3.connect(output_path)
    create_tables_if_not_exist(conn)
    count = 1

    for domain, value in sorted(site_list.iteritems()):
        print(count),
        if add_www:
            domain = "www." + domain
        initial_header = {"Host": domain,
                          "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:30.0) Gecko/20100101 Firefox/30.0",
                          "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                          "Accept-Language": "en-US,en;q=0.5",
                          "Accept-Encoding": "gzip, deflate",
                          "Connection": "close"}
        print("Getting header for "),
        print(domain),
        try:
            if use_https:
                connection = httplib.HTTPSConnection(domain, timeout=20)
            else:
                connection = httplib.HTTPConnection(domain, timeout=20)
            connection.request("GET", "", headers=initial_header)
            res = connection.getresponse()
            results = res.status, res.reason
            print(results)
            add_to_database(conn, res, use_https, domain)

        except BaseException as reason:
            print(reason)
            if use_https:
                conn.execute("INSERT INTO HTTPS_Headers(url, https, error) VALUES (?,?,?)",
                             (domain, use_https, str(reason)))
            else:
                conn.execute("INSERT INTO HTTP_Headers(url, https, error) VALUES (?,?,?)",
                             (domain, use_https, str(reason)))

        count += 1
        #if count > 4:
        #    break
    conn.commit()


def create_tables_if_not_exist(conn):
    conn.executescript('DROP TABLE IF EXISTS HTTP_Headers;')
    conn.executescript('DROP TABLE IF EXISTS HTTPS_Headers;')
    conn.execute('CREATE TABLE IF NOT EXISTS HTTP_Headers ('
                 'url, https, status_code, status_text, sts, key_pin, key_pin_report_only, '
                 'location, cookies, full_header, error)')
    conn.execute('CREATE TABLE IF NOT EXISTS HTTPS_Headers ('
                 'url, https, status_code, status_text, sts, key_pin, key_pin_report_only,'
                 'location, cookies, full_header, error)')
    conn.commit()


def add_to_database(conn, res, use_https, domain):
    header = dict(res.getheaders())
    if use_https:
        conn.execute("INSERT INTO HTTPS_Headers(url, https, status_code, status_text, sts, key_pin,"
                     "key_pin_report_only, location, cookies, full_header) VALUES (?,?,?,?,?,?,?,?,?,?)",
                     (domain, use_https, res.status, res.reason, header.get('strict-transport-security'),
                     header.get('public-key-pins'), header.get('public-key-pins-report-only'),
                     header.get('location'), header.get('set-cookie'), str(res.getheaders())))
    else:
        conn.execute("INSERT INTO HTTP_Headers(url, https, status_code, status_text, sts, key_pin, "
                     "key_pin_report_only, location, cookies, full_header) VALUES (?,?,?,?,?,?,?,?,?,?)",
                     (domain, use_https, res.status, res.reason, header.get('strict-transport-security'),
                     header.get('public-key-pins'), header.get('public-key-pins-report-only'),
                     header.get('location'), header.get('set-cookie'), str(res.getheaders())))


def combine_header_db(from_path, to_path):
    conn = sqlite3.connect(to_path)
    conn.execute('CREATE TABLE IF NOT EXISTS HTTPS_Headers ('
                 'url, https, status_code, status_text, sts, location, cookies, full_header, error)')
    conn.commit()
    conn2 = sqlite3.connect(from_path)
    c2 = conn2.execute('SELECT url, https, status_code, status_text, sts, location, '
                       'cookies, full_header, error FROM HTTP_Headers')
    data2 = c2.fetchall()
    print("starting first")
    for row in data2:
        conn.execute("INSERT INTO HTTPS_Headers(url, https, status_code, status_text, sts, "
                     "location, cookies, full_header) VALUES (?,?,?,?,?,?,?,?)",
                     (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7]))
    conn.commit()


def get_top_headers(site_list, output_path):
    get_sts_headers_from_list(site_list, output_path, False, False)
    get_sts_headers_from_list(site_list, output_path, False, True)
    get_sts_headers_from_list(site_list, output_path, True, False)
    get_sts_headers_from_list(site_list, output_path, True, True)


def get_preloaded_headers(site_list, output_path):
    #only need base not www
    get_sts_headers_from_list(site_list, output_path, False, False)
    get_sts_headers_from_list(site_list, output_path, True, False)


class FakeSocket():
    def __init__(self, response_str):
        self._file = StringIO(response_str)

    def makefile(self, *args, **kwargs):
        return self._file


def insert_top_million_into_db(conn, lines, use_https):
    if use_https:
        index = 4
    else:
        index = 3
    for i in range(len(lines) - 1):
        my_json = json.loads(lines[i])
        if len(my_json['log']) == index:
            source = FakeSocket(my_json['log'][index-1]['data']['response'])
            try:
                res = HTTPResponse(source)
                res.begin()
                add_to_database(conn, res, use_https, my_json['domain'])

            except BaseException as reason:
                continue
                #print(reason, my_json['domain'])


def move_top_million_addresses(conn, path, https):
    print("starting")
    with open(path) as json_file:
        lines = json_file.readlines()
    insert_top_million_into_db(conn, lines, https)
    conn.commit()


    #{log: [{data:, type:, error:},
    #       {data: {server_hello:, server_key_exchange:, server_certificates:, server_finished:},
    #               type:, error}],
    # timestamp:, host:, domain:, port:}
    #{u'log': [{u'data': None, u'type': u'connect', u'error': None},
    #          {u'data':
    #               {u'server_hello':
    #                    {u'compression_method': 0, u'next_protocol_negotiation': False, u'random': u'64VL5AjpfLjXM6sAcxY3aSTD3ZQC8LVSW5xJCX2HaPY=', u'session_id': u'Sr+QBiwmvBCDvKu6Ce2aCA52kdANROrYM5gp48PXkEs=', u'cipher_suite': 49169, u'version': 771, u'ocsp_stapling': False, u'ticket_supported': False, u'next_protocols': None, u'heartbeat_supported': True}
    #                , u'server_key_exchange':
    #                    {u'key': u'AwAXQQRLibk8RpOh7Op+Iv70CIR6wiLSi6FeZB9Zj99kQ86Nupy4aSUteTeMM+TxTts0aME8Ce2QTMm4e4cIN8+NVTxcBAEBAEauqK1ZLPBYGvazqnV2SS3Q2864+tNFEBJAJ5vmDS9jkZHFabdv1jmo57rv4FqscE7r733WjXPRDYLDLtcejP5QJa0NRjO/eiHDxCvpo8ZAkT3YmEvyugqm4rojNgInyMIEFidL7XggoUVt+YQK2R4BnrRQyXSAli0YmDomY8hz5NtAOlgwH8HoXNfaOROxkSwVHKV0s3klfexRKv1mfCXRMBF9gI1rNuOgsgoNlznMnjvrD00vvOWLgZSW9cRnfwQS3OdbQnqjJZFf03P5i25EpzvhRZmaAIj8CeZ8VLzXILBv7xD6BuS1F+Bzd+6UCrse2YXlzfuhKOHdZ7aee3c='},
    #                , u'server_certificates':
    #                    {u'validation_error': None, u'alt_names': [u'*.torcache.net', u'torcache.net'], u'common_name': u'*.torcache.net', u'is_valid': True, u'certificates': [u'MIIFUzCCBDugAwIBAgIQVklhj6vg53fbvwMCQPRqajANBgkqhkiG9w0BAQsFADCBkDELMAkGA1UEBhMCR0IxGzAZBgNVBAgTEkdyZWF0ZXIgTWFuY2hlc3RlcjEQMA4GA1UEBxMHU2FsZm9yZDEaMBgGA1UEChMRQ09NT0RPIENBIExpbWl0ZWQxNjA0BgNVBAMTLUNPTU9ETyBSU0EgRG9tYWluIFZhbGlkYXRpb24gU2VjdXJlIFNlcnZlciBDQTAeFw0xNDA1MDkwMDAwMDBaFw0xNTA1MTAyMzU5NTlaMFwxITAfBgNVBAsTGERvbWFpbiBDb250cm9sIFZhbGlkYXRlZDEeMBwGA1UECxMVRXNzZW50aWFsU1NMIFdpbGRjYXJkMRcwFQYDVQQDFA4qLnRvcmNhY2hlLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMJadnpUe4uToqYYTQ39bnagp9OYa+QOtUwox+lEJq8lrHaTBtLCRvgAHvs837KU84d3hEFp+5D8IqY92BtaTgyQi3QGUpOqq2lbQDr11Lz0iFei5GIe7kREVr6xeXkrxuOipAQOXhBrq5zU1JM3XuOlJYJPc76isubAJjF6BQugXWYUyM3pnJwL7BzitrVGDPlpDzrc9vIFJJrKopRSWwGIIpPUwbb8VYJmzoSUtG/hwYtDGa8t40ELCg5aA9eZP/39BepTcbuWBwoIbEiTXw+JlNGdGcrFep3rDlEtSHtLssXbO51hUIr8lxzS0A4MdNMm//VXWHtr4i886DgrMMcCAwEAAaOCAdowggHWMB8GA1UdIwQYMBaAFJCvajqUWgvYkOoSVnPfQ7Q6KNrnMB0GA1UdDgQWBBQswpkC92afJQ65GrdeV5cfSCrQNDAOBgNVHQ8BAf8EBAMCBaAwDAYDVR0TAQH/BAIwADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwUAYDVR0gBEkwRzA7BgwrBgEEAbIxAQIBAwQwKzApBggrBgEFBQcCARYdaHR0cHM6Ly9zZWN1cmUuY29tb2RvLm5ldC9DUFMwCAYGZ4EMAQIBMFQGA1UdHwRNMEswSaBHoEWGQ2h0dHA6Ly9jcmwuY29tb2RvY2EuY29tL0NPTU9ET1JTQURvbWFpblZhbGlkYXRpb25TZWN1cmVTZXJ2ZXJDQS5jcmwwgYUGCCsGAQUFBwEBBHkwdzBPBggrBgEFBQcwAoZDaHR0cDovL2NydC5jb21vZG9jYS5jb20vQ09NT0RPUlNBRG9tYWluVmFsaWRhdGlvblNlY3VyZVNlcnZlckNBLmNydDAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuY29tb2RvY2EuY29tMCcGA1UdEQQgMB6CDioudG9yY2FjaGUubmV0ggx0b3JjYWNoZS5uZXQwDQYJKoZIhvcNAQELBQADggEBADP2Hb9h7zCBUohqlRlWASTGLqKIQJpYp+ddDKMH+TJmm/fCIJJDz8BS5UrAMiUr/gSC7w8riOQqKRWQ01hwY4c/ALMcP6DRq91gp1huLN+d0k0IbHCG0SvOjY584b+jSVfzaU3iBZE/KKE+UQyfKYQzdi6WexDFbbrirkVxxSoVscO02FKho/ABVMy95NB5XC414HL1FQn/v48ZKqY5MQSbSNGnTmuVIGs3cAqs/E1B7s8e517l+n8xlJ9GKciWB1Tjij4Peoyg7fv4IqmERPkQMXcHNdR7FW1SxifKz6jOy88jDLvdM3ydS19p1TAsH/8TUdGps6oT/cQTGFNMxnM='], u'issuer': u'COMODO RSA Domain Validation Secure Server CA'},
    #                , u'server_finished': {u'verify_data': u'Zke7/IKlNLL0RTmy'}
    #                },
    #           u'type': u'tls_handshake', u'error': None},
    #          {u'data': {u'sent': u'GET / HTTP/1.1\r\nHost: torcache.net\r\n\r\n'},
    #           u'type': u'write', u'error': None},
    #          {u'data': {u'response': u'HTTP/1.1 200 OK\r\nServer: nginx/1.5.13\r\nContent-Type: text/html; charset=UTF-8\r\nTransfer-Encoding: chunked\r\nConnection: keep-alive\r\nX-Powered-By: PHP/5.4.11--pl2-gentoo\r\nCache-Control: no-cache\r\nDate: Sun, 02 Nov 2014 23:42:57 GMT\r\nSet-Cookie: session_payload=36e8a0795c685da931eefb3b35760845a7bd4d19%2B4PpeCZCvrzjdNY%2F55PBus0CZncLMqRScX8gmELuapg5MVEKTzeQg88qROKgHA7%2FhFR50dxiTlCnI4JXizexiBfEFLk4tOaGjUPk%2F6dJFvZSKQqH6sAOrW6XnqTvRZ5owCyfr0aC9x76QxtuaxeLiZOOhi3x6vIpzesypTbAu17FJ5fXtOYNER4P8d3lJqcyZel3JCqsZRJxax28bxR6fBNpZFK5of5FdkyBIWWQq8jxF8NlfZ%2BHI3DRmxdxi796miXcHpphIdGRMCNVMshGcx%2FswjeGJ51%2BJK6ooviOlzxNhWxCyuRtwC31EO%2BdsV4mzK%2BprB%2B%2BfjTAWgD2bedGLHA%3D%3D; expires=Mon, 08-Feb-2016 22:42:57 GMT; path=/; httponly\r\nSet-Cookie: torcache_session=b3843af7f1eff9c94cfd724738da1b0f2e8c1276%2B7bdz0z5T9VrJDu1OFnBKwrOijG1k92ueHw8gD7Ju; expires=Mon, 08-Feb-2016 22:42:57 GMT; path=/; httponly\r\n\r\nfa6\r\n<!DOCTYPE html>\n<html lang="en">\n<head>\n\t<meta http-equiv="content-type" content="text/html; charset=utf-8" />\n\t<title>Torcache - Torrent Cache</title>\n\t<link href="https://torcache.net/bundles/bootstrapper/css/bootstrap.min.css" media="all" type="text/css" rel="stylesheet">\n<link href="https://torcache.net/bundles/bootstrapper/css/nav-fix.css" media="all" type="text/css" rel="stylesheet">\n<link href="https://torcache.net/bundles/bootstrapper/css/bootstrap-responsive.min.css" media="all" type="text/css" rel="stylesheet">\n\t<script src="https://torcache.net/bundles/bootstrapper/js/jquery-1.8.0.min.js"></script>\n<script src="https://torcache.net/bundles/bootstrapper/js/bootstrap.min.js"></script>\n\t<link href="https://torcache.net/css/main.css" media="all" type="text/css" rel="stylesheet">\n\t</head>\n<body>\n\t<div class="navbar navbar-fixed-top"><div class="navbar-inner">\n\t\t<div class="container-fluid">\n\t\t\t<a class="brand" href="/">Torcache</a>\n\t\t\t<div class="nav-collapse">\n\t\t\t\t<ul class="nav">\n\t\t\t\t\t\t\t\t\t<li class="active"><a href="/"><i class="icon-home icon-white"></i> Home</a></li>\n\t\t\t\t\t<li><a href="/api"><i class="icon-wrench icon-white"></i> API</a></li>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>\n\t\t\t</div><!--/.nav-collapse -->\n\t\t</div>\n\t</div></div>\n\t<div class="container-fluid">\n\t\t<div class="row-fluid">\n\t\t\t<h1>  </h1>\n\t\t\t<h2>Torcache is a free service for caching torrent files online.</h2>\n\nYou can not search or list torrent files that are stored here, you can only access them if you already know the info_hash value of the torrent you want to download.\n<br />The way to access torrents is simple, you just use the url https://torcache.net//torrent/<i>INFO_HASH_IN_HEX</i>.torrent (note HEX values A-F must be in uppercase)</p>\n<p><b>Example URL:</b><br /> <a href="/torrent/640FE84C613C17F663551D218689A64E8AEBEABE.torrent">https://torcache.net/torrent/640FE84C613C17F663551D218689A64E8AEBEABE.torrent</a> (Slackware 12.2 DVD ISO)</p>\n<p>The torrent files are saved to disk in gzip format, that means you have to use a browser that understands the gzip transfer encoding.</p>\n\n<div class="leaderboard">\n\t<h2>Send .torrent to cache:</h2>\n\t<form enctype="multipart/form-data" class="form-horizontal" method="POST" action="https://torcache.net/api/upload" accept-charset="UTF-8">\t    <div class="control-group">\n\t\t    <input type="file" name="torrent"></input>\n            <button class="btn btn-primary btn" type="submit">Cache!</button>        </div>\n    </form>\t<p>Just choose the torrent file you want to add to the cache and click "Cache!".\n</div>\n\n<h2>Removal</h2>\n<p>We store torrents forever.</p>\n<h2>Legal</h2>\n<ul>\n<li>We <b>DO NOT</b> have or track any information about what type of content the torrents point to.</li>\n<li>We <b>DO NOT</b> have any type of search or listing system.</li>\n<li>We <b>DO NOT</b> run our own trackers. </li>\n<li>The original filename of the torrent is <b>NOT</b> saved.</li>\n<li>We <b>DO NOT</b> log any IP adresses of uploaders.</li>\n<li>You can <b>ONLY</b> download torrents if you <b>ALREADY KNOW</b> the INFO_HASH of the torrent file.</li>\n<li>Torrent files are cached on disk in gzip format making it extremely time consuming to search for any data contained within the torrent files.</li>\n</ul>\n\n<h2>I want automation / an API!</h2>\n<p>Here\'s a list of our <a href="/api">APIs</a>.</p>\n\n\t\t</div>\n\t</div> <!-- /container -->\n<div id="_47ec71d7b7be19d934c4ddbefc0aa90c"></div>\n<script type="text/javascript">\nvar _scq = _scq || [];\n_scq.push([\'setDebug\', false]);\n_scq.push([\'setHost\', \'s.torcache.net\']);\n_scq.push([\'setAccount\', \'_d0b70e33563de9bece3faa2ee3558e1e\']);\n_scq.push([\'addSlot\', \'_47ec71d7b7be19d934c4ddbefc0aa90c\']);\n_scq.push([\'load\']);\n(function() {\n   \xa0 \xa0var sc = document.createElement(\'script\'); sc.type = \'text/javascript\'; sc.async = true;\n    \xa0 \xa0sc.src = document.location.protocol + \'//s.torcache.net/sc.js\';\n    \xa0 \xa0var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(sc, s);\n})();\n</script>\n</body>\n</html>\r\n0\r\n\r\n'}
    #              , u'type': u'read', u'error': None}],
    #u'timestamp': u'2014-11-02T18:42:54.337973306-05:00', u'host': u'109.163.226.148', u'domain': u'jz123.cn', u'port': 443}



def move_top_million():
    conn = sqlite3.connect(top_million_header_path)
    #create_tables_if_not_exist(conn)
    #move_top_million_addresses(conn, os.path.abspath(os.path.join(resource_folder, 'top_million_http_domain.json')), False)
    #move_top_million_addresses(conn, os.path.abspath(os.path.join(resource_folder, 'top_million_http_www.json')), False)
    #move_top_million_addresses(conn, os.path.abspath(os.path.join(resource_folder, 'top_million_https_domain.json')), True)
    #move_top_million_addresses(conn, os.path.abspath(os.path.join(resource_folder, 'top_million_https_www.json')), True)
    conn.close()

#get_top_headers(top_10000, top10000_header_path)
#get_preloaded_headers(hsts_list, hsts_header_path)
#get_preloaded_headers(pinning_list, pinned_header_path)
move_top_million()
