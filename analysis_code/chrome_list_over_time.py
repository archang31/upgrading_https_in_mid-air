__author__ = 'mkranch'
import requests
import base64
import lists_helper
import os
import time
from bs4 import BeautifulSoup

#folders
base_folder = os.path.dirname(os.path.realpath(__file__))
resources_folder = os.path.join(base_folder, 'resources/')
sts_folder = os.path.join(base_folder, 'resources/sts/')


def get_single_json_file(link_address, file_name):
    #create a new request, go get the main master page, turn master into a soup
    session = requests.session()
    address = link_address + file_name + '?format=TEXT'
    #grab the new page (will be in base64 code)
    print(address)
    new_req = session.get(address)
    new_soup = BeautifulSoup(new_req.content)
    text64 = str(new_soup)
    #decode from base64 to utf-8, then decode from utf-8
    decoded = base64.b64decode(text64)
    page_string = decoded.decode("utf-8")
    f = open('resources/' + file_name, 'w')
    #write the entire json, close file
    f.write(page_string)
    f.close()


def get_sts_json_files(link_address, address_end, getNext):
    #create a new request, go get the main master page, turn master into a soup
    session = requests.session()
    master_req = session.get(link_address)
    master_soup = BeautifulSoup(master_req.content)
    #We also want time so create a time array and append each time title to the array using i as counter (later)
    time = []
    for link in master_soup.find_all('span', {"class": "time"}):
        time.append(link.get('title'))

    #Now look at all a tags in the master_soup
    for link in master_soup.find_all('a'):
        link_string = str(link.get('href'))
        #if starts with '/chromium/src/net/+/' must be a
        print(link_string)
        if link_string[0:20] == '/chromium/src/net/+/':
            #Create the actual address of the individual link. Key point is to add ?format=TEXT at the end
            address = 'https://chromium.googlesource.com' + link_string + address_end + \
                      '/transport_security_state_static.json?format=TEXT'
            #grab the new page (will be in base64 code)
            print(address)
            new_req = session.get(address)
            new_soup = BeautifulSoup(new_req.content)
            text64 = str(new_soup)
            #decode from base64 to utf-8, then decode from utf-8
            decoded = base64.b64decode(text64)
            page_string = decoded.decode("utf-8")
            #open file. Write the time string to the first line, increment time_string counter,
            time_comment = '// ' + time.pop(0) + '\n'
            time_string = time_comment[3:].strip().split(' ')
            formatted_time = time_string[4] + '-' + time_string[1] + '-' + time_string[2]
            f = open('resources/sts/' + formatted_time + '-' + link_string[20:27] + '.txt', 'w')
            f.write(time_comment)
            #write the entire json, close file
            f.write(page_string)
            f.close()

        #this looks for next (need to also use booleen getNext so do not grab "previous")
        elif link_string.startswith('/chromium/src/net/+log/master/http/transport_security_state_static.json?s=')\
                and getNext:
            #This must be the "next" link, so run again
            print("getting next")
            get_sts_json_files('https://chromium.googlesource.com' + link_string, '/http', False)


def parse_sts_json_files():
    #Lists folder location
    file_list = os.listdir(sts_folder)
    master_file = open('../publication/figures/chrome_list_over_time_data.csv', 'w')
    #master_file.write('Date, Pinsets, Total Entries, Base Domains, Google Base Domains, Non-Google Base Domains\n')
    for file in sorted(file_list):
        if file[0] is not '.':
            f = open(sts_folder + file, 'r')
            time_string = f.readline()
            #strip off the comment (first 3 lines) as well as the newline
            time_string = time_string[3:].strip()
            #unit_time = time.strptime(time_string)
            time_string = time_string.split(' ')
            formated_time = str(time_string[2]) + '-' + str(time_string[1]) + '-' + str(time_string[4])
            #print(unit_time.tm_year, unit_time.tm_mon, unit_time.tm_mday)
            f.close()
            #LH helper function (for parsing json)
            LH = lists_helper.ListHelper()
            parsed_json = LH.get_preloaded_list(sts_folder + file)
            master_file.write(formated_time + ', '
                              + str(LH.pinsets) + ', '
                              + str(LH.entries) + ', '
                              + str(LH.base_domains) + ', '
                              + str(LH.google_base_domains) + ', '
                              + str(LH.base_domains - LH.google_base_domains) + '\n')
    #print(page_string)
    master_file.close()


master_address = 'https://chromium.googlesource.com/' \
                 'chromium/src/net/+log/master/http/transport_security_state_static.json'
old_address = 'https://chromium.googlesource.com/' \
              'chromium/src/net/+log/570f325aaed358dfc659ad8ca17824a1255f9bd6/base/transport_security_state_static.json'
#set below to true if you want to get the full list
#get_sts_json_files(master_address, '/http', False)
#get_sts_json_files(old_address, '/base', False)
parse_sts_json_files()
#get_single_json_file('https://chromium.googlesource.com/chromium/src/net/+/'
#                     'aef57cf56a178620558f3ce3be89d07c3d5713e9/http/', 'transport_security_state_static.certs')