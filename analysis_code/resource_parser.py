import sqlite3
import sys
from urlparse import urlparse
import lists_helper
import hsts_summary_maker

#File location stuff
database_path = "../test_crawl_db.sqlite"
#resource_path = "../logged_crawl.sqlite"
#get the preloaded domains from the list helper

LH = lists_helper.ListHelper()
preloaded_domains = LH.get_preloaded_list()
public_suffix_list = LH.get_public_suffix_list()
everywhere_list = LH.get_https_everywhere_list()
errors = {}
error_by_value = {}
listed_errors = {}


def check_scheme(scheme):
    if scheme == 'https' or scheme == 'http' or scheme == 'wss' or scheme == 'ws':
        return False
    else:
        return True


def handle_normal_traffic(resource_url, origin_url, window_url, inIframe):
    error_list = []
    if inIframe == 'True':
        #Handling iframe
        if origin_url.netloc is "":
            return error_list
            #error_list.append("Ignore")
        else:
            #Use Origin as Window Url
            if resource_url.scheme == "http" or resource_url.scheme == "ws":
                temp_list = handle_http_resource(resource_url, origin_url)
            else:
                temp_list = handle_https_resource(resource_url, origin_url)
            #Add IFrame to end of all errors for seperation
            for item in temp_list:
                error_list.append(item)
                #error_list.append(item + "Window")
    else:
        #Not in IFrame
        if resource_url.scheme == "http" or resource_url.scheme == "ws":
            error_list.extend(handle_http_resource(resource_url, window_url))
        else:
            error_list.extend(handle_https_resource(resource_url, window_url))
    return error_list


def handle_window_http(is_crawl_preloaded):
    #Window Scheme IS HTTP
    error_list = []
    if is_crawl_preloaded[0] == 'Domain':
        #Directly Listed
        if preloaded_domains[is_crawl_preloaded[1]]['pins']:
            error_list.append("PinnedSelfRedirectsToHTTP")
        else:
            error_list.append("PreloadedSelfRedirectsToHTTP")
    elif is_crawl_preloaded[0] == 'SubDomain':
        if preloaded_domains[is_crawl_preloaded[1]]['include_subdomains']:
            if preloaded_domains[is_crawl_preloaded[1]]['pins']:
                error_list.append("PinnedSelfRedirectsToHTTP")
            else:
                error_list.append("PreloadedSelfRedirectsToHTTP")
        else:
            #These are not HSTS domains
            error_list.append("BaseHSTSRedirectsToHTTP")
    else:
        error_list.append("BaseHSTSRedirectsToHTTP")
    return error_list


def handle_redirect(crawl_url, resource_url, origin_url, window_url, inIframe):
    error_list = []
    is_site_preloaded = LH.check_if_in_preloaded_list(window_url.netloc)
    if LH.check_common_domain(crawl_url.netloc, window_url.netloc):
        #These sites share a common base Domain
        if is_site_preloaded[0] == "Domain" or is_site_preloaded[0] == "SubDomain":
            #Handle just like normal HTTP/HTTPS:
            handle_normal_traffic(resource_url, origin_url, window_url, inIframe)
        elif is_site_preloaded[0] == "BaseDomain":
            error_list.append("PreloadedRedirectsOutOfSTS")
            if window_url.scheme == "http":
                error_list.append("PreloadedRedirectsToHTTP")
        else:
            print(LH.error + "In Handle Redirect check common")
            error_list.append("Error")
    else:
        #print(crawl_url.scheme, crawl_url.netloc, window_url.scheme, window_url.netloc)
        error_list.append("PreloadedRedirectsOutOfSTS")
        if window_url.scheme == "http":
            error_list.append("PreloadedRedirectsToHTTP")
    return error_list


def parser(crawl_url, resource_url, origin_url, window_url, inIframe):
    #This method handles all the actual parsing, either from the extension or crawl DB
    #ignore all request for about or chrome or data
    error_list = []
    if check_scheme(resource_url.scheme):
        #error_list.append("Ignore")
        return error_list
    #elif window_url.scheme == 'chrome' or window_url.scheme == 'about':
    elif check_scheme(window_url.scheme):
        #error_list.append("Ignore")
        return error_list
    #First, check if an HTTPS
    else:
        is_crawl_preloaded = LH.check_if_in_preloaded_list(crawl_url.netloc)
        if is_crawl_preloaded[0] == "Not in List":
            #Make sure crawl at least shares a base domain with a HSTS preloaded site
            error_list.append("CrawlNotPreloaded")
        else:
            if window_url.scheme == "http" or window_url.scheme == 'ws':
                #If Window is HTTP, does not matter what resource is
                if window_url.scheme == 'ws':
                    print("WS!", window_url.netloc, window_url.path, resource_url.netloc, resource_url.path)
                error_list.extend(handle_window_http(is_crawl_preloaded))
            else:
                #Window scheme is HTTPS
                if LH.check_common_domain(crawl_url.netloc, window_url.netloc):
                    #I handle IFrame in the handle_normal_Traffic
                    error_list.extend(handle_normal_traffic(resource_url, origin_url, window_url, inIframe))
                else:
                    #Since they do not share a base domain, some sort of redirect must take place.
                    error_list.extend(handle_redirect(crawl_url, resource_url, origin_url, window_url, inIframe))
    for item in error_list:
        if not errors.get(item, False):
            errors[item] = {}
            errors[item]['text'] = item
            errors[item]['list'] = []
    return error_list


def handle_http_resource(resource_url, window_url):
    error_list = []
    is_site_preloaded = LH.check_if_in_preloaded_list(window_url.netloc)
    is_resource_preloaded = LH.check_if_in_preloaded_list(resource_url.netloc)
    if is_resource_preloaded[0] == 'Domain' or (is_resource_preloaded[0] == 'SubDomain'
                                                and preloaded_domains[is_resource_preloaded[1]]['include_subdomains']):
        if preloaded_domains[is_resource_preloaded[1]]['mode']:
            error_list.append("HTTPSRefHSTSViaHTTP")
            if LH.check_common_domain(resource_url.netloc, window_url.netloc):
                error_list.append("HTTPSSelfRefHSTSViaHTTP")
            else:
                error_list.append("HTTPSRefHSTSExternalViaHTTP")
        if preloaded_domains[is_resource_preloaded[1]]['pins']:
            error_list.append("HTTPSRefPinnedViaHTTP")
            if LH.check_common_domain(resource_url.netloc, window_url.netloc):
                error_list.append("HTTPSSelfRefPinnedViaHTTP")
        if everywhere_list.get(LH.domain(is_resource_preloaded[1]), False):
            error_list.append("HTTPSRefEverywhereViaHTTP")
            if LH.check_common_domain(resource_url.netloc, window_url.netloc):
                error_list.append("HTTPSSelfRefEverywhereViaHTTP")
    #print(window_url.netloc, is_site_preloaded)
    if is_site_preloaded[0] == "Domain":
        #Check to see if the URL in the browser window is HTTPS:
        if window_url.scheme == "https":
            #Currently the window scheme is HTTPS while the resource is HTTP
            #Handle initial case first
            if preloaded_domains[is_site_preloaded[1]].get('pins', False):
                error_list.append("PinnedRefHTTP")
                if preloaded_domains[is_site_preloaded[1]]['mode']:
                    error_list.append("PinnedHSTSRefHTTP")
                else:
                    error_list.append("PreloadedRefHTTP")
            if LH.check_common_domain(resource_url.netloc, window_url.netloc):
                #now add check if also common domain
                if preloaded_domains[is_site_preloaded[1]]['pins']:
                    #The Crawl URL and Window URL share a similar base domain
                    error_list.append("PinnedSelfRefHTTP")
                    if preloaded_domains[is_site_preloaded[1]]['mode']:
                        error_list.append("PinnedHSTSSelfRefHTTP")
                else:
                    error_list.append("PreloadedSelfRefHTTP")
    elif is_site_preloaded[0] == "SubDomain":
        if preloaded_domains[is_site_preloaded[1]]['include_subdomains']:
            if preloaded_domains[is_site_preloaded[1]]['pins']:
                error_list.append("PinnedRefHTTP")
                if preloaded_domains[is_site_preloaded[1]]['mode']:
                    error_list.append("PinnedHSTSRefHTTP")
            else:
                error_list.append("PreloadedRefHTTP")
        else:
            if preloaded_domains[is_site_preloaded[1]]['pins']:
                error_list.append("PinnedWOSubRefHTTP")
                if preloaded_domains[is_site_preloaded[1]]['mode']:
                    error_list.append("PinnedHSTSWOSubRefHTTP")
            else:
                error_list.append("PreloadedWOSubRefHTTP")
        #Handle Additional errors if also common domain (Self)
        if LH.check_common_domain(resource_url.netloc, window_url.netloc):
            #The Crawl URL and Window URL share a similar base domain
            if preloaded_domains[is_site_preloaded[1]]['include_subdomains']:
                if preloaded_domains[is_site_preloaded[1]]['pins']:
                    error_list.append("PinnedSelfRefHTTP")
                    if preloaded_domains[is_site_preloaded[1]]['mode']:
                        error_list.append("PinnedHSTSSelfRefHTTP")
                else:
                    error_list.append("PreloadedSelfRefHTTP")
            else:
                #Not HSTS Do to Issue Without SUB
                if preloaded_domains[is_site_preloaded[1]]['pins']:
                    error_list.append("PinnedWOSubSelfRefHTTP")
                    if preloaded_domains[is_site_preloaded[1]]['mode']:
                        test = True
                        #error_list.append("PinnedHSTSWOSubSTSSelfRefHTTP")
                else:
                    error_list.append("PreloadedWOSubSelfRefHTTP")

    #elif is_site_preloaded[0] == "BaseDomain":
    #These are sites that base domains of more specific preloaded sites.
    #    error_list.append("HSTSBaseRefHTTP")
    #else:
    #    error_list.append("HTTPSRefHTTP")
    return error_list


def handle_https_pinned(is_resource_preloaded, is_site_preloaded):
    error_list = []
    if is_resource_preloaded[0] == 'Domain':
        if preloaded_domains[is_resource_preloaded[1]]['pins']:
            #Check for Pinned Ref Other Pinned
            if preloaded_domains[is_resource_preloaded[1]]['pins'] != preloaded_domains[is_site_preloaded[1]]['pins']:
                error_list.append("PinnedRefDifferentPin")
            else:
                error_list.append("PinnedRefPinned")
                if preloaded_domains[is_site_preloaded[1]]['mode']:
                    error_list.append("PinnedHSTSRefPinned")
        else:
            error_list.append("PinnedRefPreloaded")
    elif is_resource_preloaded[0] == 'SubDomain':
        if preloaded_domains[is_resource_preloaded[1]]['include_subdomains']:
            if preloaded_domains[is_resource_preloaded[1]]['pins']:
                error_list.append("PinnedRefPinned")
                if preloaded_domains[is_site_preloaded[1]]['mode']:
                    error_list.append("PinnedHSTSRefPinned")
                    if is_HSTS(is_resource_preloaded):
                        error_list.append("HSTSRefHSTS")
                        if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
                            error_list.append("HSTSSelfRefHSTS")
                        else:
                            error_list.append("HSTSRefHSTSExternal")
            else:
                error_list.append("PinnedRefPreloaded")
                error_list.append("PinnedRefHTTPS")
        else:
            error_list.append("PinnedRefWOSub")
            error_list.append("PinnedRefHTTPS")
            if preloaded_domains[is_site_preloaded[1]]['pins'] != 'google':
                error_list.append("PinnedNotGoogleRefHTTPS")
                error_list.append("PinnedNotGoogleRefWOSub")
            if preloaded_domains[is_site_preloaded[1]]['mode']:
                error_list.append("PinnedHSTSRefWOSub")
                error_list.append("PinnedHSTSRefHTTPS")
            if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
                error_list.append("PinnedSelfRefWOSub")
                error_list.append("PinnedSelfRefHTTPS")
                if preloaded_domains[is_site_preloaded[1]]['mode']:
                    error_list.append("PinnedHSTSSelfRefWOSub")
                    error_list.append("PinnedHSTSSelfRefHTTPS")
    elif is_resource_preloaded[0] == 'BaseDomain':
        error_list.append("PinnedRefBaseHSTS")
        error_list.append("PinnedRefHTTPS")
        if preloaded_domains[is_site_preloaded[1]]['pins'] != 'google':
            error_list.append("PinnedNotGoogleRefHTTPS")
            error_list.append("PinnedNotGoogleRefWOSub")
        if preloaded_domains[is_site_preloaded[1]]['mode']:
            error_list.append("PinnedHSTSRefBaseHSTS")
            error_list.append("PinnedHSTSRefHTTPS")
        if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
            error_list.append("PinnedSelfRefBaseHSTS")
            error_list.append("PinnedSelfRefHTTPS")
            if preloaded_domains[is_site_preloaded[1]]['mode']:
                error_list.append("PinnedHSTSSelfRefBaseHSTS")
                error_list.append("PinnedHSTSSelfRefHTTPS")
    else:
        error_list.append("PinnedRefHTTPS")
        if preloaded_domains[is_site_preloaded[1]]['pins'] != 'google':
            error_list.append("PinnedNotGoogleRefHTTPS")
        if preloaded_domains[is_site_preloaded[1]]['mode']:
            error_list.append("PinnedHSTSRefHTTPS")
        if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
            error_list.append("PinnedSelfRefHTTPS")
            if preloaded_domains[is_site_preloaded[1]]['mode']:
                error_list.append("PinnedHSTSSelfRefHTTPS")
    return error_list


def is_HSTS(is_x_preloaded):
    if is_x_preloaded[0] == 'Domain' or (is_x_preloaded[0] == 'SubDomain' and
                                        preloaded_domains[is_x_preloaded[1]]['include_subdomains']):
        if preloaded_domains[is_x_preloaded[1]]['mode']:
            return True
    return False


def handle_https_not_pinned(is_resource_preloaded, is_site_preloaded):
    error_list = []
    if is_resource_preloaded[0] == 'Domain':
        error_list.append("PreloadedRefPreloaded")
        if is_HSTS(is_resource_preloaded):
            error_list.append("HSTSRefHSTS")
        if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
            error_list.append("PreloadedSelfRefPreloaded")
    elif is_resource_preloaded[0] == 'SubDomain':
        if preloaded_domains[is_resource_preloaded[1]]['include_subdomains']:
            error_list.append("PreloadedRefPreloaded")
            if is_HSTS(is_resource_preloaded):
                error_list.append("HSTSRefHSTS")
                if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
                    error_list.append("HSTSSelfRefHSTS")
                else:
                    error_list.append("HSTSRefHSTSExternal")
            if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
                error_list.append("PreloadedSelfRefPreloaded")
        else:
            error_list.append("PreloadedRefWOSub")
            error_list.append("PreloadedRefHTTPS")
            if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
                error_list.append("PreloadedSelfRefWOSub")
                error_list.append("PreloadedSelfRefHTTPS")
    elif is_resource_preloaded[0] == 'BaseDomain':
        error_list.append("PreloadedRefBaseHSTS")
        error_list.append("PreloadedRefHTTPS")
        if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
            error_list.append("PreloadedSelfRefBaseHSTS")
            error_list.append("PreloadedSelfRefHTTPS")
    else:
        error_list.append("PreloadedRefHTTPS")
        if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
            error_list.append("PreloadedSelfRefHTTPS")

    return error_list


def handle_https_resource(resource_url, window_url):
    #The resource and window are both HTTPS.
    error_list = []
    is_site_preloaded = LH.check_if_in_preloaded_list(window_url.netloc)
    is_resource_preloaded = LH.check_if_in_preloaded_list(resource_url.netloc)
    if is_HSTS(is_resource_preloaded):
        error_list.append("HTTPSRefHSTS")
        if LH.check_common_domain(is_resource_preloaded[1], is_site_preloaded[1]):
            error_list.append("HTTPSSelfRefHSTS")
        else:
            error_list.append("HTTPSRefHSTSExternal")
    #Check if crawl URL in HSTS list (should always be)
    if is_site_preloaded[0] == 'Domain':
        #The window url is an exact match to preloaded site
        if preloaded_domains[is_site_preloaded[1]]['pins']:
            #Window URL is pinned. Check if resource_pinned
            error_list.extend(handle_https_pinned(is_resource_preloaded, is_site_preloaded))
        else:
            error_list.extend(handle_https_not_pinned(is_resource_preloaded, is_site_preloaded))
            #Window URL is not pinned
    elif is_site_preloaded[0] == 'SubDomain':
        if preloaded_domains[is_site_preloaded[1]]['include_subdomains']:
            if preloaded_domains[is_site_preloaded[1]]['pins']:
                error_list.extend(handle_https_pinned(is_resource_preloaded, is_site_preloaded))
            else:
                error_list.extend(handle_https_not_pinned(is_resource_preloaded, is_site_preloaded))
        else:
            #These are not HSTS sites
            error_list.append("BaseHSTSRefHTTPS")
    elif is_site_preloaded[0] == 'BaseDomain':
        error_list.append("BaseHSTSRefHTTPS")
    else:
        error_list.append("HTTPSRefHTTPS")
    return error_list


def delete_old_results():
    conn = sqlite3.connect(database_path)
    cur = conn.cursor()
    cur.executescript('DROP TABLE IF EXISTS HSTS_Pinning_Results_Categories;')
    cur.executescript('DROP TABLE IF EXISTS HSTS_Pinning_Results_List;')
    cur.execute("CREATE TABLE HSTS_Pinning_Results_Categories (name, count)")
    cur.execute("CREATE TABLE HSTS_Pinning_Results_List (error_type, crawl_url, resource_url, content_type, "
                "isInIFrame, window_url, request_origin, original_row)")
    conn.commit()


def update_results_summary():
    conn = sqlite3.connect(database_path)
    cur = conn.cursor()
    for error, value in sorted(errors.iteritems()):
        print(error, len(value['list']))
        cur.execute('INSERT INTO HSTS_Pinning_Results_Categories (name, count) VALUES (?,?)',
                    (error, len(value['list'])))
    conn.commit()
    conn.close()


def handle_extension_data(extension_data, cursor, database_string):
    for row in extension_data:
        crawl_url = urlparse(row[1])
        resource_url = urlparse(row[2])
        window_url = urlparse(row[5])
        origin_url = urlparse(row[6])
        error_list = parser(crawl_url, resource_url, origin_url, window_url, row[4])
        my_row = str(row[0]) + database_string
        for my_error in error_list:
            errors[my_error]['list'].append(my_row)
            cursor.execute("INSERT INTO HSTS_Pinning_Results_List (error_type, crawl_url, resource_url, content_type, "
                           "isInIFrame, window_url, request_origin, original_row) VALUES (?,?,?,?,?,?,?,?)",
                           (my_error, row[1], row[2], row[3], row[4], row[5], row[6], my_row))
        if row[0] % 5000 == 0:
            print('*'),
            sys.stdout.flush()


def handle_static_resources():
    #This method handles the static resources (those collected from OpenWPM objects)
    conn = sqlite3.connect(database_path)
    c = conn.execute('SELECT rowid, crawl_url, resource_url, content_type, isInIFrame,\
                    window_url, request_origin FROM Resource_Loads_Static')
    #row[0] = rowid, row[1] = crawl_url, row[2] = resource_url,
    #row[3] = content_type (image, a tag, etc), row[4] = IFrame test ('true' if iFrame),
    #row[5] = window url (whats in the tab), row[6] = origin

    extension_data = c.fetchall()
    print("Starting Static")
    handle_extension_data(extension_data, conn.cursor(), "S")
    print("\nFinished Static")
    conn.commit()
    conn.close()


def handle_dynamic_resources():
    #This method handles the dynamic resources
    conn = sqlite3.connect(database_path)
    c = conn.execute('SELECT rowid, crawl_url, resource_url, content_type, isInIFrame,\
                    window_url, request_origin FROM Resource_Loads_Dynamic')
    #row[0] = rowId, row[1] = crawl_url, row[2] = resource url, row[3] = content_type (Script, image,etc.)
    #row[4] = iFrame ('True' if in IFrame), row[5] = window url (whats in the tab), row[6] = origin
    #row[7] = node name,  row[8] = dtg
    extension_data = c.fetchall()
    print("Starting Dynamic")
    handle_extension_data(extension_data, conn.cursor(), "D")
    print('\nFinished Dynamic')
    conn.commit()
    conn.close()

delete_old_results()
handle_static_resources()
handle_dynamic_resources()
update_results_summary()
#summary_maker.make_summary(errors)



