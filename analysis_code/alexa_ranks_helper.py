import lists_helper
import sqlite3
import httplib
import pdb
from HTMLParser import HTMLParser
from operator import itemgetter

LH = lists_helper.ListHelper()
preloaded_list = LH.get_preloaded_list()
global_icon = False
rank = 0


#Class for use in parsing the Alexa HTML response
class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == "img":
            for attr in attrs:
                if attr[0] == 'title' and attr[1] == 'Global rank icon':
                    global global_icon
                    global_icon = True
                    #print(attr)

    def handle_data(self, data):
        global global_icon
        if global_icon:
            #print "Data     :", data
            global_icon = False
            global rank
            rank = data


#This method is used to pull the Alexa Ranking of a Site - used on the preloaded sites to see where they stand.
def get_preloaded_alexa_rank():
    alexa_file = open("resources/alexa_raw.txt", 'w')
    for domain, value in sorted(preloaded_list.iteritems()):
        global rank
        rank = 0
        alexa = "www.alexa.com"
        path = "/siteinfo/" + domain
        print("Getting Header for " + alexa + path)

        try:
            connection = httplib.HTTPConnection(alexa, timeout=5)
            connection.request("GET", path)
            res = connection.getresponse()
            results = res.status, res.reason
            print(results)
            parser = MyHTMLParser()
            parser.feed(res.read())
            if rank != 0:
                print("Rank = ", rank)
            else:
                print("Rank still 0")
            alexa_file.write(domain + ", " + str(rank) + '\n')

        except BaseException as reason:
            alexa_file.write(domain + ", error \n")
            print(reason)
    alexa_file.close()


#This sorts the already downloaded Alexa Ranks and outputs to a csv. The
def parse_alexa_by_base_domain():
    ranks = {}
    ranks_not_google = {}
    alexa_file = open("resources/alexa_raw.txt", 'r')
    for line in alexa_file:
        domain = LH.domain(line.split(',', 1)[0])
        if domain is None:
            domain = line.split(',', 1)[0]
        site_rank = line.split(',', 1)[1].strip().replace(",", "")
        if ranks.get(domain, False):
            if ranks[domain] != site_rank:
                print(domain, ranks[domain], site_rank)
        else:
            ranks[domain] = site_rank
            #If not a google site
            if preloaded_list[line.split(',', 1)[0]]['pins'] != 'google':
                ranks_not_google[domain] = site_rank
    alexa_base = open("resources/alexa_by_base_domain.csv", 'w')
    for domain in sorted(ranks.items(), key=itemgetter(0)):
        alexa_base.write(domain[0] + "," + domain[1] + '\n')
    alexa_base.close()
    alexa_base_not_google = open("resources/alexa_by_base_wo_google.csv", 'w')
    for domain in sorted(ranks_not_google.items(), key=itemgetter(0)):
        alexa_base_not_google.write(domain[0] + "," + domain[1] + '\n')
    alexa_base_not_google.close()


if __name__ == "__main__":
    #get_preloaded_alexa_rank()
    parse_alexa_by_base_domain()