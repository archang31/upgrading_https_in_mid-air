import cookielib
import urllib2
import lists_helper
import sqlite3

hsts_list = lists_helper.get_hsts_domains()
#hsts_list = ['www.google.com', 'keeperapp.com', 'usaa.com', 'twitter.com'
hsts_list = open('../test_sites.txt')
header_file = open('raw_headers.txt', 'w')
#header_file.write("Domain name, Strict TS (T/F), Max Age\n")
#cookie_file.write("Domain name, Name, Secure, Path, Domain, Includes Subs (T/F), ")


def initialize_cookie():
    c = {}
    c['secure'] = ''
    c['httponly'] = ''
    c['path'] = ''
    c['domain'] = ''
    c['expires'] = ''
    c['subdomains'] = ''
    return c

def my_cookie_parser(h):
    new_header = h.split('\r\n')
    new_cookie_list = []
    for line in new_header:
        if 'set-cookie' in line.lower():
            #cookie_String starts after Set-cookie:, the replace removes all spaces
            cookie_string = line[12:len(line)].replace(" ", "")
            #check if even a ';' in cookie_String
            my_cookie = initialize_cookie()
            if ';' in cookie_string:
                cookie = cookie_string.split(';', 1)
                name = cookie[0]
                cookie_string = cookie[1]
            else:
                name = cookie_string
            #my_cook[0] is name, my_cook[1] is the rest of the cookies
            name = name.split('=', 1)
            my_cookie['name'] = name[0]
            my_cookie['value'] = name[1]
            if ';' in cookie_string:
                attributes = cookie_string.split(';')
                for a in attributes:
                    if '=' in a:
                        temp = a.split('=', 1)
                        #if present, remove from dictionary before adding new
                        if my_cookie.get(temp[0].lower()):
                            my_cookie.pop(temp[0].lower())
                        my_cookie[temp[0].lower()] = temp[1]
                    else:
                        my_cookie[a.lower()] = 'True'
                #handle fields for consistency
                if my_cookie.get('domain'):
                    if my_cookie['domain'][0] == '.':
                        my_cookie.pop('subdomains')
                        my_cookie['subdomains'] = 'True'
            new_cookie_list.append(my_cookie)
    return new_cookie_list


def get_sts_headers_top_10000():
    conn = sqlite3.connect("resources/HSTS_cookies_db.sqlite")
    cur = conn.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS Headers (crawl_url, strict_TS, strict_subdomains, max_age, error)''')
    cur.execute('''CREATE TABLE IF NOT EXISTS Header_Strings (crawl_url, header)''')
    cur.execute('''CREATE TABLE IF NOT EXISTS Cookies \
    (crawl_url, cookie_name, cookie_value, secure, path, cookie_domain, includes_subdomains, expires)''')
    cur.execute('''CREATE TABLE IF NOT EXISTS My_Cookies \
    (crawl_url, cookie_name, cookie_value, secure, httponly, path, cookie_domain, includes_subdomains, expires)''')
    #cur.execute('''CREATE TABLE IF NOT EXISTS My_Cookie_Strings (crawl_url, cookie_string)''')
    cur.execute('''CREATE TABLE IF NOT EXISTS Header_Strings (crawl_url, header)''')
    for domain in hsts_list:
        crawl_url = domain #for testing
        #crawl_url = 'https://' + domain
        print("Getting Header for " + crawl_url)

        request = urllib2.Request(url=crawl_url)
        try:
            response = urllib2.urlopen(request, timeout=20)
            header = response.info()
            my_cookie_list = my_cookie_parser(str(header))
            cur.execute("INSERT INTO Header_Strings (crawl_url, header) VALUES (?,?)", (crawl_url, str(header)))
            if header.get('strict-transport-security', False):
                if ";" in header['strict-transport-security']:
                    strict = header['strict-transport-security'].split(";")
                    cur.execute("INSERT INTO Headers (crawl_url, strict_TS, strict_subdomains, max_age)"
                              " VALUES (?,?,?,?)", (crawl_url, 'True', strict[1], strict[0]))
                else:
                    cur.execute("INSERT INTO Headers (crawl_url, strict_TS, strict_subdomains, max_age)"
                              " VALUES (?,?,?,?)", (crawl_url, 'True', "", header['strict-transport-security']))
            else:
                cur.execute("INSERT INTO Headers (crawl_url, strict_TS, strict_subdomains, max_age)"
                          " VALUES (?,?,?,?)", (crawl_url, " ", " ", " "))
            cookie_list = cookielib.CookieJar()
            cookie_list.extract_cookies(response, request)
            for c in cookie_list:
                cur.execute("INSERT INTO Cookies (crawl_url, cookie_name, cookie_value, secure, path, "
                            "cookie_domain, includes_subdomains, expires) VALUES (?,?,?,?,?,?,?,?)",
                           (crawl_url, c.name, c.value, c.secure, c.path, c.domain_specified,
                            c.domain_initial_dot, c.expires))
            for c in my_cookie_list:
                cur.execute("INSERT INTO My_Cookies (crawl_url, cookie_name, cookie_value, secure, httponly, path, "
                            "cookie_domain, includes_subdomains, expires) VALUES (?,?,?,?,?,?,?,?, ?)",
                           (crawl_url, c['name'], c['value'], c['secure'], c['httponly'], c['path'],
                            c['domain'], c['subdomains'], c['expires']))
            conn.commit()

        except urllib2.URLError as reason:
            print("Could not access " + domain + " because " + str(reason))
            cur.execute("INSERT INTO Headers (crawl_url, error) VALUES (?,?)", (crawl_url, str(reason)))

        except:
            cur.execute("INSERT INTO Headers (crawl_url, error) VALUES (?,?)", (crawl_url, "Unknown error"))
            print("Unknown error for " + domain)

header_file.close()
#cookie_file.close()