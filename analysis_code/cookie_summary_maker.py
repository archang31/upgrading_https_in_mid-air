import lists_helper
import sqlite3
from urlparse import urlparse
import pdb

cookie_list = list()
LH = lists_helper.ListHelper()
preloaded_list = LH.get_preloaded_list()
pinned_domains = LH.pinned_list
hsts_domains = LH.hsts_list
pinned_list = LH.pinned_list
latex_file = open('../publication/resources/cookie_data.tex', 'w')


#Legacy Method - from my initial cookie crawl prior to using the database cookies
def input_my_cookies():
    conn = sqlite3.connect("resources/HSTS_cookies_db.sqlite")
    cur = conn.execute('SELECT rowid, crawl_url, cookie_name, cookie_value, secure, httponly, path, '
                       'cookie_domain, includes_subdomains, expires FROM My_Cookies')

    my_cookie_list = list()
    extension_data = cur.fetchall()
    for row in extension_data:
        c = dict()
        c['rowid'] = row[0]
        crawl_url = row[1]
        c['crawl_url'] = crawl_url[8:len(crawl_url)]
        c['name'] = row[2]
        c['value'] = row[3]
        c['secure'] = row[4]
        c['httponly'] = row[5]
        c['path'] = row[6]
        c['domain'] = row[7]
        c['subdomains'] = row[8]
        c['expires'] = row[9]
        my_cookie_list.append(c)
    return my_cookie_list


#Method for inputing cookies from profile - DO NOT USE due to issue with domains (does not
# differentiate between www.dropbox.com and dropbox.com for instance)
def input_crawl_profile_cookies(database_path):
    conn = sqlite3.connect(database_path)
    cur = conn.execute('SELECT rowid, page_url, name, value, isSecure, isHttpOnly, path, '
                       'domain, host, expiry FROM profile_cookies')

    my_cookie_list = list()
    extension_data = cur.fetchall()
    for row in extension_data:
        c = dict()
        c['rowid'] = row[0]
        crawl_url = row[1]
        c['crawl_url'] = crawl_url[8:len(crawl_url)]
        c['name'] = row[2]
        c['value'] = row[3]
        c['secure'] = row[4]
        c['httponly'] = row[5]
        c['path'] = row[6]
        c['domain'] = row[7]
        if row[8][0] == '.':
            c['subdomains'] = 'True'
        else:
            c['subdomains'] = 'False'
        c['expires'] = row[9]
        my_cookie_list.append(c)
    return my_cookie_list


#Method for inputing cookies from crawl database
def input_crawl_cookies(database_path):
    conn = sqlite3.connect(database_path)
    cur = conn.execute('SELECT rowid, top_url, name, value, isSecure, isHttpOnly,'
                       'referrer, domain, expiry, accessed, http_type FROM cookies')

    my_cookie_list = list()
    extension_data = cur.fetchall()
    for row in extension_data:
        c = dict()
        c['rowid'] = row[0]
        crawl_url = urlparse(row[1])
        c['crawl_url'] = crawl_url.netloc + crawl_url.path
        c['name'] = row[2]
        c['value'] = row[3]
        c['secure'] = row[4]
        c['httponly'] = row[5]
        c['path'] = row[6]
        if row[7] and row[7][0] == '.':
            c['subdomains'] = 'True'
            c['domain'] = row[7][1:]

        else:
            c['subdomains'] = 'False'
            c['domain'] = row[7]
        c['expires'] = row[8]
        c['accessed'] = row[9]
        c['httptype'] = row[10]
        my_cookie_list.append(c)
    return my_cookie_list


def move_crawl_cookies(database_path, cookie_list):
    conn = sqlite3.connect(database_path)
    cur = conn.cursor()
    for c in cookie_list:
        if c['subdomains']:
            c['domain'] = '.' + c['domain']
        cur.execute("INSERT INTO cookies (top_url, name, value, isSecure, isHttpOnly, referrer, domain, expiry,"
                    " crawl_id, accessed, http_type) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
                    (c['crawl_url'], c['name'], c['value'], c['secure'], c['httponly'],
                     c['path'], c['domain'], c['expires'], 9, c['accessed'], c['httptype']))
    conn.commit()


#basic attempt at determining if a tracking cookie
def is_tracking_cookie(name):
    if name.startswith("__utm") or name.startswith("heatmaps_g2g") \
            or name.startswith("_jsuid") or name.startswith("_first_pageview") \
            or name.startswith("SESSea0691c506da7") or name.startswith("__dc"):
        return True
    else:
        return False


def handle_cookie(my_list, cookie, crawl_netloc):
    if my_list.get(cookie['domain'], False):
        if not my_list[cookie['domain']].get(cookie['name'], False):
            my_list[cookie['domain']][cookie['name']] = cookie
    else:
        my_list[cookie['domain']] = {}
        my_list[cookie['domain']][cookie['name']] = cookie


def latex(command, command_text):
    if type(command_text) is int:
        output = "\\newcommand{\\" + str(command) + '}{' + str("{:,d}".format(command_text)) + '}\n'
    else:
        output = "\\newcommand{\\" + str(command) + '}{' + str(command_text) + '}\n'
    latex_file.write(output)


def comment(text):
    latex_file.write("%" + text + '\n')


def percent(num1, num2):
    return format(((float(num1) / float(num2)) * 100), '.1f')


def preloaded_cookie_test():
    my_cookie_list = input_crawl_cookies('../crawl_db.sqlite')
    domains_not_including_subs = {}
    printable_list = []
    printable_pinned_list = []
    printable_hsts_list = []
    for domain, values in preloaded_list.iteritems():
        if not values['include_subdomains']:
            domains_not_including_subs[domain] = domain
            if hsts_domains.get(domain, False):
                printable_hsts_list.append(domain)
            if not 'search.yahoo' in domain:
                printable_list.append(domain)
            if pinned_domains.get(domain, False):
                printable_pinned_list.append(domain)

    comment("Preloaded sites that have an includes_subdomains hole (ignoring search.yahoo):")
    comment(str(sorted(printable_list)) + '\n\n')

    comment("Pinned Sites that have an includes_subdomains hole:")
    comment(str(sorted(printable_pinned_list)) + '\n\n')

    test_a = {}
    test_a_not_tracking = {}
    test_a_not_google = {}
    test_b = {}
    test_b_not_tracking = {}
    test_b_not_google = {}
    for cookie in my_cookie_list:
        if cookie['domain']:
            resource_url = urlparse("https://" + cookie['crawl_url'])
            if domains_not_including_subs.get(cookie['domain'], False):
                #Test A
                #If site is pinned
                if pinned_domains.get(cookie['domain'], False):
                    #if cookie includes subdomains
                    #if cookie['subdomains'] == "True":
                    handle_cookie(test_a, cookie, resource_url.netloc)
                    if not is_tracking_cookie(cookie['name']):
                        handle_cookie(test_a_not_tracking, cookie, resource_url.netloc)
                    if preloaded_list[cookie['domain']]['pins'] != 'google':
                        handle_cookie(test_a_not_google, cookie, resource_url.netloc)

                #Test B:
                #If Cookie set for includes subdomains:
                #if cookie['subdomains'] == "True":
                    #If cookie not set to Secure
                #if cookie['secure'] == 0:
                if hsts_domains.get(cookie['domain'], False):
                    handle_cookie(test_b, cookie, resource_url.netloc)
                    if not is_tracking_cookie(cookie['name']):
                            handle_cookie(test_b_not_tracking, cookie, resource_url.netloc)
                    if pinned_domains.get(cookie['domain'], False):
                        if preloaded_list[cookie['domain']]['pins'] != 'google':
                            handle_cookie(test_b_not_google, cookie, resource_url.netloc)

    errors = [(test_a, "PinnedCookie"), (test_b, "HSTSCookie"),
              (test_a_not_google, "PinnedCookieNotGoogle"), (test_b_not_google, "HSTSCookieNotGoogle"),
              (test_a_not_tracking, "PinnedNotTrackingCookie"), (test_b_not_tracking, "HSTSNotTrackingCookie")]

    latex("HSTSDomainsWithHole", len(printable_hsts_list))
    latex("HSTSDomainsWithHolePercent", percent(len(printable_hsts_list), LH.hsts_base_domains))
    latex("PinnedDomainsWithHole", len(printable_pinned_list))
    latex("PinnedDomainsWithHolePercent", percent(len(printable_pinned_list), LH.pinned_base_domains))
    latex("PinnedDomainsWithLeakableCookie", len(test_a))
    latex("PinnedDomainsWithLeakableCookiePercent",percent(len(test_a), LH.pinned_base_domains))
    latex("HSTSDomainsWithLeakableCookie", len(test_b))
    latex("HSTSDomainsWithLeakableCookiePercent", percent(len(test_b), LH.hsts_base_domains))
    latex("PinnedDomainsWithLeakableCookieNotGoogle", len(test_a_not_google))
    latex("PinnedDomainsWithLeakableCookieNotGooglePercent",
          percent(len(test_a_not_google), LH.pinned_base_domains - LH.pinned_google_base_domains))
    latex("HSTSDomainsWithLeakableCookieNotGoogle", len(test_b_not_google))
    latex("HSTSDomainsWithLeakableCookieNotGooglePercent",
          percent(len(test_b_not_google), LH.hsts_base_domains - LH.pinned_google_base_domains))

    for error in errors:
        total_cookies = 0
        secure_cookies = 0
        not_secure_cookies = 0
        latex(error[1] + "Domains", len(error[0]))
        comment("Total Count, Count Secure/Not Secure at the end do to timing of counting\n")
        domain_list = []
        for domain, cookies in sorted(error[0].iteritems()):
            if not 'search.yahoo' in domain:
                if domain.replace(".", "").capitalize() != "Neg9org":
                    cookie_name_list = []
                    total_this_cookie = 0
                    secure_this_cookie = 0
                    not_secure_this_cookie = 0
                    eb = error[1] + domain.replace(".", "").replace("-", "").capitalize()
                    latex(eb, len(cookies))
                    latex(eb + "Hole", "*." + domains_not_including_subs[domain])
                    first = True
                    for cookie_name, cookie in cookies.iteritems():
                        if first:
                            dom_comment = "Example: " + str(cookie)
                            first = False
                        cookie_name_list.append(cookie_name)
                        total_this_cookie += 1
                        if cookie['secure']:
                            secure_this_cookie += 1
                        else:
                            not_secure_this_cookie += 1
                    domain_list.append(domain)
                    latex(eb + 'Secure', secure_this_cookie)
                    latex(eb + 'NotSecure', not_secure_this_cookie)
                    latex(eb + "Total", total_this_cookie)
                    total_cookies += total_this_cookie
                    secure_cookies += secure_this_cookie
                    not_secure_cookies += not_secure_this_cookie
                    comment("Name List:" + str(cookie_name_list))
                    comment(dom_comment + '\n')
                else:
                    print(domain)
        latex(error[1] + 'Secure', secure_cookies)
        latex(error[1] + 'NotSecure', not_secure_cookies)
        latex(error[1] + "Total", total_cookies)
        latex(error[1] + "PercentNotSecure", round((float(not_secure_cookies) / float(total_cookies) * 100), 1))
        comment("\n")

    #Turn Off Figure
    figure = True
    if figure:
        figure_file = open('../publication/resources/cookie_figure.tex', 'w')
        figure_file.write("%!TEX root =  ../main.tex\n")
        figure_file.write("\\begin{table}[t]\n  \centering\n")
        figure_file.write("  \caption{Leakable Pinned Cookies} \label{tab:PinnedCookies}\n")
        figure_file.write("  \\begin{tabular}{|| c | c | c | c ||}\n")
        figure_file.write("    Domain & Domain Hole & Insecure Cookies & Total Cookies\\\\\n    \hline\n")
        for domain, cookies in sorted(test_a.iteritems()):
            eb = "\PinnedCookie" + domain.replace(".", "").capitalize()
            figure_file.write("    " + domain + " & " + eb + "Hole & " + eb + "NotSecure & " + eb + "Total \\\\\n")
        figure_file.write("  \hline\n")
        figure_file.write(" \\multicolumn{2}{||c|}{\\textit{total}} & \\PinnedCookieNotSecure & \\PinnedCookieTotal \\\\\n")
        figure_file.write("  \end{tabular}\n\end{table}[t]")
        figure_file.close()
    #pdb.set_trace()
    latex_file.close()


def has_numbers(inputString):
    return any(char.isdigit() for char in inputString)


def dynamic_cookie_test(latex_subdomains):
    my_cookie_list = input_crawl_cookies('resources/top_dynamic_cookies.sqlite')
    global latex_file
    latex_file = open('../publication/resources/cookie_data_dynamic.tex', 'w')
    domains_not_including_subs = {}
    printable_list = []

    f = open('resources/top_dynamic_hsts_wosub_sites.txt', 'r')
    for line in f:
        domain = line.strip()
        domains_not_including_subs[domain] = domain
        printable_list.append(domain)

    comment("Dynamic sites that have an includes_subdomains hole (ignoring search.yahoo):")
    comment(str(sorted(printable_list)) + '\n\n')

    test_b = {}
    test_b_not_tracking = {}

    for cookie in my_cookie_list:
        if cookie['domain']:
            resource_url = urlparse("https://" + cookie['crawl_url'])
            if domains_not_including_subs.get(cookie['domain'], False):
                #Test B:
                #If Cookie set for includes subdomains:
                #if cookie['subdomains'] == "True":
                    #If cookie not set to Secure
                #if cookie['secure'] == 0:
                handle_cookie(test_b, cookie, resource_url.netloc)
                if not is_tracking_cookie(cookie['name']):
                        handle_cookie(test_b_not_tracking, cookie, resource_url.netloc)

    errors = [(test_b, "TopDynamicCookie")]
    latex("TopDynamicHSTSDomainsWithHole", len(domains_not_including_subs))
    latex("TopDynamicHSTSDomainsWithHolePercent", percent(len(domains_not_including_subs), 5142))
    latex("TopDynamicHSTSDomainsWithLeakableCookie", len(test_b))
#TODO: DO NO HARDCODE THIS NUMBER
    latex("TopDynamicHSTSDomainsWithLeakableCookiePercent", percent(len(test_b), 5142))

    for error in errors:
        total_cookies = 0
        secure_cookies = 0
        not_secure_cookies = 0
        latex(error[1] + "Domains", len(error[0]))
        comment("Total Count, Count Secure/Not Secure at the end do to timing of counting\n")
        domain_list = []
        for domain, cookies in sorted(error[0].iteritems()):
            if not 'search.yahoo' in domain:
                if not has_numbers(domain.replace(".", "").capitalize()):
                    cookie_name_list = []
                    total_this_cookie = 0
                    secure_this_cookie = 0
                    not_secure_this_cookie = 0
                    eb = error[1] + domain.replace(".", "").replace("-", "").capitalize()
                    first = True
                    for cookie_name, cookie in cookies.iteritems():
                        if first:
                            dom_comment = "Example: " + str(cookie)
                            first = False
                        cookie_name_list.append(cookie_name)
                        total_this_cookie += 1
                        if cookie['secure']:
                            secure_this_cookie += 1
                        else:
                            not_secure_this_cookie += 1
                    domain_list.append(domain)
                    total_cookies += total_this_cookie
                    secure_cookies += secure_this_cookie
                    not_secure_cookies += not_secure_this_cookie
                    if latex_subdomains:
                        latex(eb, len(cookies))
                        latex(eb + "Hole", "*." + domains_not_including_subs[domain])
                        latex(eb + 'Secure', secure_this_cookie)
                        latex(eb + 'NotSecure', not_secure_this_cookie)
                        latex(eb + "Total", total_this_cookie)
                        comment("Name List:" + str(cookie_name_list))
                        comment(dom_comment + '\n')
        latex(error[1] + 'Secure', secure_cookies)
        latex(error[1] + 'NotSecure', not_secure_cookies)
        latex(error[1] + "Total", total_cookies)
        latex(error[1] + "PercentNotSecure", round((float(not_secure_cookies) / float(total_cookies) * 100), 1))
        comment("\n")

    latex_file.close()

preloaded_cookie_test()
print("Done")
dynamic_cookie_test(latex_subdomains = False)