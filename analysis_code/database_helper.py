import sqlite3
import sys
from urlparse import urlparse
import lists_helper
import hsts_summary_maker


#This is a file to helper in the moving, consolidating, etc. of the various databases
#happens a lot with crawls that databased get split

#File location stuff
database_path = "../crawl_db.sqlite"
resource_path = "../logged_crawl.sqlite"


def move_static_db():
    old_conn = sqlite3.connect('../crawl_db2.sqlite')
    c = old_conn.execute('SELECT crawl_url, resource_url, content_type, isInIFrame,\
                    window_url, request_origin FROM Resource_Loads_Static')
    conn = sqlite3.connect('../crawl_db.sqlite')
    cur = conn.cursor()
    data = c.fetchall()
    for row in data:
        cur.execute("INSERT INTO Resource_Loads_Static(crawl_url, resource_url, content_type, isInIFrame,\
                    window_url, request_origin) VALUES (?,?,?,?,?,?)",
                    (row[0], row[1], row[2], row[3], row[4], row[5]))
    conn.commit()
    print("Done Move Static")


def move_dynamic_db():
    old_conn = sqlite3.connect('../Requests2.sqlite')
    c = old_conn.execute('SELECT crawl_url, resource_url, content_type, isInIFrame,\
                    window_url, request_origin, node_name FROM Resource_Loads_Static')
    conn = sqlite3.connect('../crawl_db.sqlite')
    cur = conn.cursor()
    data = c.fetchall()
    for row in data:
        cur.execute("INSERT INTO Resource_Loads_Dynamic(crawl_url, resource_url, content_type, isInIFrame,\
                    window_url, request_origin, node_name) VALUES (?,?,?,?,?,?,?)",
                    (row[0], row[1], row[2], row[3], row[4], row[5], row[6]))
    conn.commit()
    print("Done Move Dynamic")


def move_cookies():
    old_conn = sqlite3.connect('../crawl_db2.sqlite')
    c = old_conn.execute('SELECT top_url, name, value, isSecure, isHttpOnly, referrer, domain,'
                         ' expiry, referrer, http_type, crawl_id, accessed FROM cookies')
    conn = sqlite3.connect('../crawl_db.sqlite')
    cur = conn.cursor()
    data = c.fetchall()
    for row in data:
        cur.execute("INSERT INTO cookies(top_url, name, value, isSecure, isHttpOnly, referrer, domain,"
                    " expiry, referrer, http_type, crawl_id, accessed) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                    (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11]))
    conn.commit()
    print("Done Move Cookies")


def move_profile_cookies():
    old_conn = sqlite3.connect('../crawl_db2.sqlite')
    c = old_conn.execute('SELECT page_url, name, value, isSecure, isHttpOnly, path, '
                         'domain, host, expiry, accessed, creationTime, crawl_id FROM profile_cookies')
    conn = sqlite3.connect('../crawl_db.sqlite')
    cur = conn.cursor()
    data = c.fetchall()
    for row in data:
        cur.execute("INSERT INTO profile_cookies(page_url, name, value, isSecure, isHttpOnly, path, "
                    "domain, host, expiry, accessed, creationTime, crawl_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                    (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11]))
    conn.commit()
    print("Done Move Profile Cookies")

#move_static_db()
#move_dynamic_db()
#move_cookies()
#move_profile_cookies()